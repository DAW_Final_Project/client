# => Build container
FROM node:14.17.0-alpine as builder
WORKDIR /app
COPY . .
RUN rm -Rf node_modules
RUN npm install 
RUN npm run build
# => Run container
FROM nginx:alpine
# Nginx config
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/default.conf /etc/nginx/conf.d
# Static build
COPY --from=builder /app/build /usr/share/nginx/html/
# Default port exposure
EXPOSE 80
# Copy .env.template for parse env vars into container
WORKDIR /usr/share/nginx/html
# Add bash
RUN apk add --no-cache bash
# SAVE build date for maintenance operations
RUN date > /build-date.txt
# Start Nginx server after parse env vars 
CMD ["/bin/sh",  "-c",  "exec nginx -g 'daemon off;'"]