// ActionTypes
import {
  // Get Operations List
  GET_OPERATIONS_LIST,
  GET_OPERATIONS_LIST_SUCCESS,
  GET_OPERATIONS_LIST_ERROR,
  // Get Operation Data
  GET_OPERATION_DATA,
  GET_OPERATION_DATA_SUCCESS,
  GET_OPERATION_DATA_ERROR,
  // Add Operation
  ADD_OPERATION,
  ADD_OPERATION_SUCCESS,
  ADD_OPERATION_ERROR,
  // Modify Operation Data
  MODIFY_OPERATION_DATA,
  MODIFY_OPERATION_DATA_SUCCESS,
  MODIFY_OPERATION_DATA_ERROR,
  // Delete Operation
  DELETE_OPERATION,
  DELETE_OPERATION_SUCCESS,
  DELETE_OPERATION_ERROR,
  // Specific Reset
  RESET_OPERATIONS,
  // General Reset
  RESET,
} from "./actionTypes";

const initialState = {
  operationsError: "",
  operationsnSuccess: "",
  operationsLoading: false,
  operationsList: [],
  operationsListTotal: 0,
  operationData: {},
};

const operations = (state = initialState, action) => {
  switch (action.type) {
    // Get Operations List
    case GET_OPERATIONS_LIST:
      state = {
        ...state,
        operationsLoading: true,
        operationsSuccess: "",
        operationsError: "",
      };
      break;
    case GET_OPERATIONS_LIST_SUCCESS:
      state = {
        ...state,
        operationsList: action.payload.list,
        operationsListTotal: action.payload.total,
        operationsSuccess: "MsgOk[Operations List Received]",
        operationsError: "",
        operationsLoading: false,
      };
      break;
    case GET_OPERATIONS_LIST_ERROR:
      state = {
        ...state,
        operationsList: [],
        operationsListTotal: 0,
        operationsError: action.payload,
        operationsSuccess: "",
        operationsLoading: false,
      };
      break;

    // Get Operation Data
    case GET_OPERATION_DATA:
      state = {
        ...state,
        operationsLoading: true,
        operationsSuccess: "",
        operationsError: "",
      };
      break;
    case GET_OPERATION_DATA_SUCCESS:
      state = {
        ...state,
        operationData: action.payload,
        operationsSuccess: "MsgOk[Operation Data Received]",
        operationsError: "",
        operationsLoading: false,
      };
      break;
    case GET_OPERATION_DATA_ERROR:
      state = {
        ...state,
        operationData: {},
        operationsError: action.payload,
        operationsSuccess: "",
        operationsLoading: false,
      };
      break;

    // Add Operation
    case ADD_OPERATION:
      state = {
        ...state,
        operationsLoading: true,
        operationsSuccess: "",
        operationsError: "",
      };
      break;
    case ADD_OPERATION_SUCCESS:
      state = {
        ...state,
        operationsSuccess: action.payload,
        operationsError: "",
        operationsLoading: false,
      };
      break;
    case ADD_OPERATION_ERROR:
      state = {
        ...state,
        operationsError: action.payload,
        operationsSuccess: "",
        operationsLoading: false,
      };
      break;

    // Modify Operation Data
    case MODIFY_OPERATION_DATA:
      state = {
        ...state,
        operationsLoading: true,
        operationsSuccess: "",
        operationsError: "",
      };
      break;
    case MODIFY_OPERATION_DATA_SUCCESS:
      state = {
        ...state,
        operationsSuccess: action.payload,
        operationsError: "",
        operationsLoading: false,
      };
      break;
    case MODIFY_OPERATION_DATA_ERROR:
      state = {
        ...state,
        operationsError: action.payload,
        operationsSuccess: "",
        operationsLoading: false,
      };
      break;

    // Delete Operation
    case DELETE_OPERATION:
      state = {
        ...state,
        operationsLoading: true,
        operationsSuccess: "",
        operationsError: "",
      };
      break;
    case DELETE_OPERATION_SUCCESS:
      state = {
        ...state,
        operationsSuccess: action.payload,
        operationsError: "",
        operationsLoading: false,
      };
      break;
    case DELETE_OPERATION_ERROR:
      state = {
        ...state,
        operationsError: action.payload,
        operationsSuccess: "",
        operationsLoading: false,
      };
      break;

    // Reset especifico
    case RESET_OPERATIONS:
      state = initialState;
      break;

    // Reset general
    case RESET:
      state = initialState;
      break;

    default:
      state = { ...state };
      break;
  }

  return state;
};

export default operations;
