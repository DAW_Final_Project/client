import { takeEvery, put, call } from "redux-saga/effects";

// ActionTypes
import {
  // Get Operations List
  GET_OPERATIONS_LIST,
  // Get Operation Data
  GET_OPERATION_DATA,
  // Add Operation
  ADD_OPERATION,
  // Modify Operation Data
  MODIFY_OPERATION_DATA,
  // Delete Operation
  DELETE_OPERATION,
} from "./actionTypes";

// Actions
import {
  // Get Operations List
  getOperationsListSuccess,
  getOperationsListError,
  // Get Operation Data
  getOperationDataSuccess,
  getOperationDataError,
  // Add Operation
  addOperationSuccess,
  addOperationError,
  // Modify Operation Data
  modifyOperationDataSuccess,
  modifyOperationDataError,
  // Delete Operation
  deleteOperationSuccess,
  deleteOperationError,
} from "./actions";

// Api functions
import * as api from "api/operations/operations.service";

/* --- FUNCTIONS --- */
// Get Operations List
function* getOperationsList({ params: { product_id, sort, order, limit, skip, navigate } }) {
  try {
    const response = yield call(api.getOperationsList, product_id, sort, order, limit, skip);
    if (response.status === 200) {
      yield put(getOperationsListSuccess(response.data));
    } else {
      yield put(getOperationsListError("MsgErr[Get Operations List Failed]"));
    }
  } catch (error) {
    yield put(getOperationsListError(error));
  }
}

// Get Operation Data
function* getOperationData({ params: { operation_id, navigate } }) {
  try {
    const response = yield call(api.getOperationData, operation_id);
    if (response.status === 200) {
      yield put(getOperationDataSuccess(response.data));
    } else {
      yield put(getOperationDataError("MsgErr[Get Operation Data Failed]"));
    }
  } catch (error) {
    yield put(getOperationDataError(error));
  }
}

// Add Operation
function* addOperation({ params: { data, navigate } }) {
  try {
    const response = yield call(api.addOperation, data);
    if (response.status === 200) {
      yield put(addOperationSuccess("MsgOk[Operation Added]"));
    } else {
      yield put(addOperationError("MsgErr[Get Operation Data Failed]"));
    }
  } catch (error) {
    yield put(addOperationError(error));
  }
}
// Modify Operation Data
function* modifyOperationData({ params: { operation_id, data, navigate } }) {
  try {
    const response = yield call(api.modifyOperationData, operation_id, data);
    if (response.status === 200) {
      yield put(modifyOperationDataSuccess("MsgOk[Operation Data Modified]"));
    } else {
      yield put(modifyOperationDataError("MsgErr[Get Operation Data Failed]"));
    }
  } catch (error) {
    yield put(modifyOperationDataError(error));
  }
}
// Delete Operation
function* deleteOperation({ params: { operation_id, navigate } }) {
  try {
    const response = yield call(api.deleteOperation, operation_id);
    if (response.status === 200) {
      yield put(deleteOperationSuccess("MsgOk[Operation Data Modified]"));
    } else {
      yield put(deleteOperationError("MsgErr[Get Operation Data Failed]"));
    }
  } catch (error) {
    yield put(deleteOperationError(error));
  }
}

function* OperationsSaga() {
  yield takeEvery(GET_OPERATIONS_LIST, getOperationsList);
  yield takeEvery(GET_OPERATION_DATA, getOperationData);
  yield takeEvery(ADD_OPERATION, addOperation);
  yield takeEvery(MODIFY_OPERATION_DATA, modifyOperationData);
  yield takeEvery(DELETE_OPERATION, deleteOperation);
}

export default OperationsSaga;