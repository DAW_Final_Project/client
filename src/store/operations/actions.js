// ActionTypes
import {
  // Get Operations List
  GET_OPERATIONS_LIST,
  GET_OPERATIONS_LIST_SUCCESS,
  GET_OPERATIONS_LIST_ERROR,
  // Get Operation Data
  GET_OPERATION_DATA,
  GET_OPERATION_DATA_SUCCESS,
  GET_OPERATION_DATA_ERROR,
  // Add Operation
  ADD_OPERATION,
  ADD_OPERATION_SUCCESS,
  ADD_OPERATION_ERROR,
  // Modify Operation Data
  MODIFY_OPERATION_DATA,
  MODIFY_OPERATION_DATA_SUCCESS,
  MODIFY_OPERATION_DATA_ERROR,
  // Delete Operation
  DELETE_OPERATION,
  DELETE_OPERATION_SUCCESS,
  DELETE_OPERATION_ERROR,
} from "./actionTypes";

// Get Operations List
export const getOperationsList = (product_id, sort, order, limit, skip, navigate) => ({
  type: GET_OPERATIONS_LIST,
  params: { product_id, sort, order, limit, skip, navigate },
});

export const getOperationsListSuccess = (obj) => ({
  type: GET_OPERATIONS_LIST_SUCCESS,
  payload: obj,
});

export const getOperationsListError = (error) => ({
  type: GET_OPERATIONS_LIST_ERROR,
  payload: error,
});

// Get Operation Data
export const getOperationData = (operation_id, navigate) => ({
  type: GET_OPERATION_DATA,
  params: { operation_id, navigate },
});

export const getOperationDataSuccess = (obj) => ({
  type: GET_OPERATION_DATA_SUCCESS,
  payload: obj,
});

export const getOperationDataError = (error) => ({
  type: GET_OPERATION_DATA_ERROR,
  payload: error,
});

// Add Operation
export const addOperation = (data, navigate) => ({
  type: ADD_OPERATION,
  params: { data, navigate },
});

export const addOperationSuccess = (obj) => ({
  type: ADD_OPERATION_SUCCESS,
  payload: obj,
});

export const addOperationError = (error) => ({
  type: ADD_OPERATION_ERROR,
  payload: error,
});

// Modify Operation Data
export const modifyOperationData = (operation_id, data, navigate) => ({
  type: MODIFY_OPERATION_DATA,
  params: { operation_id, data, navigate },
});

export const modifyOperationDataSuccess = (obj) => ({
  type: MODIFY_OPERATION_DATA_SUCCESS,
  payload: obj,
});

export const modifyOperationDataError = (error) => ({
  type: MODIFY_OPERATION_DATA_ERROR,
  payload: error,
});

// Delete Operation
export const deleteOperation = (operation_id, navigate) => ({
  type: DELETE_OPERATION,
  params: { operation_id, navigate },
});

export const deleteOperationSuccess = (obj) => ({
  type: DELETE_OPERATION_SUCCESS,
  payload: obj,
});

export const deleteOperationError = (error) => ({
  type: DELETE_OPERATION_ERROR,
  payload: error,
});
