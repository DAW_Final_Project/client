import { takeEvery, put, call } from "redux-saga/effects";

// ActionTypes
import {
  // Get Status List
  GET_STATUS_LIST,
  // Get Status Data
  GET_STATUS_DATA,
  // Add Status
  ADD_STATUS,
  // Modify Status Data
  MODIFY_STATUS_DATA,
  // Delete Status
  DELETE_STATUS,
} from "./actionTypes";

// Actions
import {
  // Get Status List
  getStatusListSuccess,
  getStatusListError,
  // Get Status Data
  getStatusDataSuccess,
  getStatusDataError,
  // Add Status
  addStatusuccess,
  addStatusError,
  // Modify Status Data
  modifyStatusDataSuccess,
  modifyStatusDataError,
  // Delete Status
  deleteStatusuccess,
  deleteStatusError,
} from "./actions";

// Api functions
import * as api from "api/status/status.service";

/* --- FUNCTIONS --- */
// Get Status List
function* getStatusList({ params: { sort, order, limit, skip, navigate } }) {
  try {
    const response = yield call(api.getStatusList, sort, order, limit, skip);
    if (response.status === 200) {
      yield put(getStatusListSuccess(response.data));
    } else {
      yield put(getStatusListError("MsgErr[Get Status List Failed]"));
    }
  } catch (error) {
    yield put(getStatusListError(error));
  }
}

// Get Status Data
function* getStatusData({ params: { status_id, navigate } }) {
  try {
    const response = yield call(api.getStatusData, status_id);
    if (response.status === 200) {
      yield put(getStatusDataSuccess(response.data[0]));
    } else {
      yield put(getStatusDataError("MsgErr[Get Status Data Failed]"));
    }
  } catch (error) {
    yield put(getStatusDataError(error));
  }
}

// Add Status
function* addStatus({ params: { data, navigate } }) {
  try {
    const response = yield call(api.addStatus, data);
    if (response.status === 200) {
      yield put(addStatusuccess("MsgOk[Status Added]"));
    } else {
      yield put(addStatusError("MsgErr[Get Status Data Failed]"));
    }
  } catch (error) {
    yield put(addStatusError(error));
  }
}
// Modify Status Data
function* modifyStatusData({ params: { status_id, data, navigate } }) {
  try {
    const response = yield call(api.modifyStatusData, status_id, data);
    if (response.status === 200) {
      yield put(modifyStatusDataSuccess("MsgOk[Status Data Modified]"));
    } else {
      yield put(modifyStatusDataError("MsgErr[Get Status Data Failed]"));
    }
  } catch (error) {
    yield put(modifyStatusDataError(error));
  }
}
// Delete Status
function* deleteStatus({ params: { status_id, navigate } }) {
  try {
    const response = yield call(api.deleteStatus, status_id);
    if (response.status === 200) {
      yield put(deleteStatusuccess("MsgOk[Status Data Modified]"));
    } else {
      yield put(deleteStatusError("MsgErr[Get Status Data Failed]"));
    }
  } catch (error) {
    yield put(deleteStatusError(error));
  }
}

function* StatusSaga() {
  yield takeEvery(GET_STATUS_LIST, getStatusList);
  yield takeEvery(GET_STATUS_DATA, getStatusData);
  yield takeEvery(ADD_STATUS, addStatus);
  yield takeEvery(MODIFY_STATUS_DATA, modifyStatusData);
  yield takeEvery(DELETE_STATUS, deleteStatus);
}

export default StatusSaga;