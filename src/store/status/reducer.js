// ActionTypes
import {
  // Get Status List
  GET_STATUS_LIST,
  GET_STATUS_LIST_SUCCESS,
  GET_STATUS_LIST_ERROR,
  // Get Status Data
  GET_STATUS_DATA,
  GET_STATUS_DATA_SUCCESS,
  GET_STATUS_DATA_ERROR,
  // Add Status
  ADD_STATUS,
  ADD_STATUS_SUCCESS,
  ADD_STATUS_ERROR,
  // Modify Status Data
  MODIFY_STATUS_DATA,
  MODIFY_STATUS_DATA_SUCCESS,
  MODIFY_STATUS_DATA_ERROR,
  // Delete Status
  DELETE_STATUS,
  DELETE_STATUS_SUCCESS,
  DELETE_STATUS_ERROR,
  // Specific Reset
  RESET_STATUS,
  // General Reset
  RESET,
} from "./actionTypes";

const initialState = {
  statusError: "",
  statusnSuccess: "",
  statusLoading: false,
  statusList: [],
  statusListTotal: 0,
  statusData: {},
};

const status = (state = initialState, action) => {
  switch (action.type) {
    // Get Status List
    case GET_STATUS_LIST:
      state = {
        ...state,
        statusLoading: true,
        statusSuccess: "",
        statusError: "",
      };
      break;
    case GET_STATUS_LIST_SUCCESS:
      state = {
        ...state,
        statusList: action.payload.list,
        statusListTotal: action.payload.total,
        statusSuccess: "MsgOk[Status List Received]",
        statusError: "",
        statusLoading: false,
      };
      break;
    case GET_STATUS_LIST_ERROR:
      state = {
        ...state,
        statusList: [],
        statusListTotal: 0,
        statusError: action.payload,
        statusSuccess: "",
        statusLoading: false,
      };
      break;

    // Get Status Data
    case GET_STATUS_DATA:
      state = {
        ...state,
        statusLoading: true,
        statusSuccess: "",
        statusError: "",
      };
      break;
    case GET_STATUS_DATA_SUCCESS:
      state = {
        ...state,
        statusData: action.payload,
        statusSuccess: "MsgOk[Status Data Received]",
        statusError: "",
        statusLoading: false,
      };
      break;
    case GET_STATUS_DATA_ERROR:
      state = {
        ...state,
        statusData: {},
        statusError: action.payload,
        statusSuccess: "",
        statusLoading: false,
      };
      break;

    // Add Status
    case ADD_STATUS:
      state = {
        ...state,
        statusLoading: true,
        statusSuccess: "",
        statusError: "",
      };
      break;
    case ADD_STATUS_SUCCESS:
      state = {
        ...state,
        statusSuccess: action.payload,
        statusError: "",
        statusLoading: false,
      };
      break;
    case ADD_STATUS_ERROR:
      state = {
        ...state,
        statusError: action.payload,
        statusSuccess: "",
        statusLoading: false,
      };
      break;

    // Modify Status Data
    case MODIFY_STATUS_DATA:
      state = {
        ...state,
        statusLoading: true,
        statusSuccess: "",
        statusError: "",
      };
      break;
    case MODIFY_STATUS_DATA_SUCCESS:
      state = {
        ...state,
        statusSuccess: action.payload,
        statusError: "",
        statusLoading: false,
      };
      break;
    case MODIFY_STATUS_DATA_ERROR:
      state = {
        ...state,
        statusError: action.payload,
        statusSuccess: "",
        statusLoading: false,
      };
      break;

    // Delete Status
    case DELETE_STATUS:
      state = {
        ...state,
        statusLoading: true,
        statusSuccess: "",
        statusError: "",
      };
      break;
    case DELETE_STATUS_SUCCESS:
      state = {
        ...state,
        statusSuccess: action.payload,
        statusError: "",
        statusLoading: false,
      };
      break;
    case DELETE_STATUS_ERROR:
      state = {
        ...state,
        statusError: action.payload,
        statusSuccess: "",
        statusLoading: false,
      };
      break;

    // Reset especifico
    case RESET_STATUS:
      state = initialState;
      break;

    // Reset general
    case RESET:
      state = initialState;
      break;

    default:
      state = { ...state };
      break;
  }

  return state;
};

export default status;
