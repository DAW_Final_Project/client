// ActionTypes
import {
  // Get Status List
  GET_STATUS_LIST,
  GET_STATUS_LIST_SUCCESS,
  GET_STATUS_LIST_ERROR,
  // Get Status Data
  GET_STATUS_DATA,
  GET_STATUS_DATA_SUCCESS,
  GET_STATUS_DATA_ERROR,
  // Add Status
  ADD_STATUS,
  ADD_STATUS_SUCCESS,
  ADD_STATUS_ERROR,
  // Modify Status Data
  MODIFY_STATUS_DATA,
  MODIFY_STATUS_DATA_SUCCESS,
  MODIFY_STATUS_DATA_ERROR,
  // Delete Status
  DELETE_STATUS,
  DELETE_STATUS_SUCCESS,
  DELETE_STATUS_ERROR,
} from "./actionTypes";

// Get Status List
export const getStatusList = (sort, order, limit, skip, navigate) => ({
  type: GET_STATUS_LIST,
  params: { sort, order, limit, skip, navigate },
});

export const getStatusListSuccess = (obj) => ({
  type: GET_STATUS_LIST_SUCCESS,
  payload: obj,
});

export const getStatusListError = (error) => ({
  type: GET_STATUS_LIST_ERROR,
  payload: error,
});

// Get Status Data
export const getStatusData = (status_id, navigate) => ({
  type: GET_STATUS_DATA,
  params: { status_id, navigate },
});

export const getStatusDataSuccess = (obj) => ({
  type: GET_STATUS_DATA_SUCCESS,
  payload: obj,
});

export const getStatusDataError = (error) => ({
  type: GET_STATUS_DATA_ERROR,
  payload: error,
});

// Add Status
export const addStatus = (data, navigate) => ({
  type: ADD_STATUS,
  params: { data, navigate },
});

export const addStatusuccess = (obj) => ({
  type: ADD_STATUS_SUCCESS,
  payload: obj,
});

export const addStatusError = (error) => ({
  type: ADD_STATUS_ERROR,
  payload: error,
});

// Modify Status Data
export const modifyStatusData = (status_id, data, navigate) => ({
  type: MODIFY_STATUS_DATA,
  params: { status_id, data, navigate },
});

export const modifyStatusDataSuccess = (obj) => ({
  type: MODIFY_STATUS_DATA_SUCCESS,
  payload: obj,
});

export const modifyStatusDataError = (error) => ({
  type: MODIFY_STATUS_DATA_ERROR,
  payload: error,
});

// Delete Status
export const deleteStatus = (status_id, navigate) => ({
  type: DELETE_STATUS,
  params: { status_id, navigate },
});

export const deleteStatusuccess = (obj) => ({
  type: DELETE_STATUS_SUCCESS,
  payload: obj,
});

export const deleteStatusError = (error) => ({
  type: DELETE_STATUS_ERROR,
  payload: error,
});
