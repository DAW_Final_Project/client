import { takeEvery, put, call } from "redux-saga/effects";

// ActionTypes
import {
  // Get Stores List For Login
  GET_STORES_LIST_FOR_LOGIN,
  // Get Stores List
  GET_STORES_LIST,
  // Get Store Data
  GET_STORE_DATA,
  // Add Store
  ADD_STORE,
  // Modify Store Data
  MODIFY_STORE_DATA,
  // Delete Store
  DELETE_STORE,
} from "./actionTypes";

// Actions
import {
  // Get Stores List For Login
  getStoresListForLoginSuccess,
  getStoresListForLoginError,
  // Get Stores List
  getStoresListSuccess,
  getStoresListError,
  // Get Store Data
  getStoreDataSuccess,
  getStoreDataError,
  // Add Store
  addStoreSuccess,
  addStoreError,
  // Modify Store Data
  modifyStoreDataSuccess,
  modifyStoreDataError,
  // Delete Store
  deleteStoreSuccess,
  deleteStoreError,
} from "./actions";

// Api functions
import * as api from "api/stores/stores.service";

/* --- FUNCTIONS --- */
// Get Stores List
function* getStoresListForLogin({ params: { sort, order, limit, skip } }) {
  try {
    const response = yield call(api.getStoresListForLogin, sort, order, limit, skip);
    if (response.status === 200) {
      yield put(getStoresListForLoginSuccess(response.data));
    } else {
      yield put(getStoresListForLoginError("MsgErr[Get Stores List Failed]"));
    }
  } catch (error) {
    yield put(getStoresListForLoginError(error));
  }
}

function* getStoresList({ params: { sort, order, limit, skip, navigate } }) {
  try {
    const response = yield call(api.getStoresList, sort, order, limit, skip);
    if (response.status === 200) {
      yield put(getStoresListSuccess(response.data));
    } else {
      yield put(getStoresListError("MsgErr[Get Stores List Failed]"));
    }
  } catch (error) {
    yield put(getStoresListError(error));
  }
}

// Get Store Data
function* getStoreData({ params: { store_id, navigate } }) {
  try {
    const response = yield call(api.getStoreData, store_id);
    if (response.status === 200) {
      yield put(getStoreDataSuccess(response.data));
    } else {
      yield put(getStoreDataError("MsgErr[Get Store Data Failed]"));
    }
  } catch (error) {
    yield put(getStoreDataError(error));
  }
}

// Add Store
function* addStore({ params: { data, navigate } }) {
  try {
    const response = yield call(api.addStore, data);
    if (response.status === 200) {
      yield put(addStoreSuccess("MsgOk[Store Added]"));
    } else {
      yield put(addStoreError("MsgErr[Get Store Data Failed]"));
    }
  } catch (error) {
    yield put(addStoreError(error));
  }
}
// Modify Store Data
function* modifyStoreData({ params: { store_id, data, navigate } }) {
  try {
    const response = yield call(api.modifyStoreData, store_id, data);
    if (response.status === 200) {
      yield put(modifyStoreDataSuccess("MsgOk[Store Data Modified]"));
    } else {
      yield put(modifyStoreDataError("MsgErr[Get Store Data Failed]"));
    }
  } catch (error) {
    yield put(modifyStoreDataError(error));
  }
}
// Delete Store
function* deleteStore({ params: { store_id, navigate } }) {
  try {
    const response = yield call(api.deleteStore, store_id);
    if (response.status === 200) {
      yield put(deleteStoreSuccess("MsgOk[Store Data Modified]"));
    } else {
      yield put(deleteStoreError("MsgErr[Get Store Data Failed]"));
    }
  } catch (error) {
    yield put(deleteStoreError(error));
  }
}

function* StoresSaga() {
  yield takeEvery(GET_STORES_LIST_FOR_LOGIN, getStoresListForLogin);
  yield takeEvery(GET_STORES_LIST, getStoresList);
  yield takeEvery(GET_STORE_DATA, getStoreData);
  yield takeEvery(ADD_STORE, addStore);
  yield takeEvery(MODIFY_STORE_DATA, modifyStoreData);
  yield takeEvery(DELETE_STORE, deleteStore);
}

export default StoresSaga;