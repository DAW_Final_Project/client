// ActionTypes
import {
  // Get Stores List For Login
  GET_STORES_LIST_FOR_LOGIN,
  GET_STORES_LIST_FOR_LOGIN_SUCCESS,
  GET_STORES_LIST_FOR_LOGIN_ERROR,
  // Get Stores List
  GET_STORES_LIST,
  GET_STORES_LIST_SUCCESS,
  GET_STORES_LIST_ERROR,
  // Get Store Data
  GET_STORE_DATA,
  GET_STORE_DATA_SUCCESS,
  GET_STORE_DATA_ERROR,
  // Add Store
  ADD_STORE,
  ADD_STORE_SUCCESS,
  ADD_STORE_ERROR,
  // Modify Store Data
  MODIFY_STORE_DATA,
  MODIFY_STORE_DATA_SUCCESS,
  MODIFY_STORE_DATA_ERROR,
  // Delete Store
  DELETE_STORE,
  DELETE_STORE_SUCCESS,
  DELETE_STORE_ERROR,
} from "./actionTypes";

// Get Stores List For Login
export const getStoresListForLogin = (sort, order, limit, skip) => ({
  type: GET_STORES_LIST_FOR_LOGIN,
  params: { sort, order, limit, skip },
});

export const getStoresListForLoginSuccess = (obj) => ({
  type: GET_STORES_LIST_FOR_LOGIN_SUCCESS,
  payload: obj,
});

export const getStoresListForLoginError = (error) => ({
  type: GET_STORES_LIST_FOR_LOGIN_ERROR,
  payload: error,
});

// Get Stores List
export const getStoresList = (sort, order, limit, skip, navigate) => ({
  type: GET_STORES_LIST,
  params: { sort, order, limit, skip, navigate },
});

export const getStoresListSuccess = (obj) => ({
  type: GET_STORES_LIST_SUCCESS,
  payload: obj,
});

export const getStoresListError = (error) => ({
  type: GET_STORES_LIST_ERROR,
  payload: error,
});

// Get Store Data
export const getStoreData = (store_id, navigate) => ({
  type: GET_STORE_DATA,
  params: { store_id, navigate },
});

export const getStoreDataSuccess = (obj) => ({
  type: GET_STORE_DATA_SUCCESS,
  payload: obj,
});

export const getStoreDataError = (error) => ({
  type: GET_STORE_DATA_ERROR,
  payload: error,
});

// Add Store
export const addStore = (data, navigate) => ({
  type: ADD_STORE,
  params: { data, navigate },
});

export const addStoreSuccess = (obj) => ({
  type: ADD_STORE_SUCCESS,
  payload: obj,
});

export const addStoreError = (error) => ({
  type: ADD_STORE_ERROR,
  payload: error,
});

// Modify Store Data
export const modifyStoreData = (store_id, data, navigate) => ({
  type: MODIFY_STORE_DATA,
  params: { store_id, data, navigate },
});

export const modifyStoreDataSuccess = (obj) => ({
  type: MODIFY_STORE_DATA_SUCCESS,
  payload: obj,
});

export const modifyStoreDataError = (error) => ({
  type: MODIFY_STORE_DATA_ERROR,
  payload: error,
});

// Delete Store
export const deleteStore = (store_id, navigate) => ({
  type: DELETE_STORE,
  params: { store_id, navigate },
});

export const deleteStoreSuccess = (obj) => ({
  type: DELETE_STORE_SUCCESS,
  payload: obj,
});

export const deleteStoreError = (error) => ({
  type: DELETE_STORE_ERROR,
  payload: error,
});
