// ActionTypes
import {
  // Get Stores List For Login
  GET_STORES_LIST_FOR_LOGIN,
  GET_STORES_LIST_FOR_LOGIN_SUCCESS,
  GET_STORES_LIST_FOR_LOGIN_ERROR,
  // Get Stores List
  GET_STORES_LIST,
  GET_STORES_LIST_SUCCESS,
  GET_STORES_LIST_ERROR,
  // Get Store Data
  GET_STORE_DATA,
  GET_STORE_DATA_SUCCESS,
  GET_STORE_DATA_ERROR,
  // Add Store
  ADD_STORE,
  ADD_STORE_SUCCESS,
  ADD_STORE_ERROR,
  // Modify Store Data
  MODIFY_STORE_DATA,
  MODIFY_STORE_DATA_SUCCESS,
  MODIFY_STORE_DATA_ERROR,
  // Delete Store
  DELETE_STORE,
  DELETE_STORE_SUCCESS,
  DELETE_STORE_ERROR,
  // Specific Reset
  RESET_STORES,
  // General Reset
  RESET,
} from "./actionTypes";

const initialState = {
  storesError: "",
  storesnSuccess: "",
  storesLoading: false,
  storesList: [],
  storesListTotal: 0,
  storeData: {},
};

const stores = (state = initialState, action) => {
  switch (action.type) {
    // Get Stores List For Login
    case GET_STORES_LIST_FOR_LOGIN:
      state = {
        ...state,
        storesLoading: true,
        storesSuccess: "",
        storesError: "",
      };
      break;
    case GET_STORES_LIST_FOR_LOGIN_SUCCESS:
      state = {
        ...state,
        storesList: action.payload,
        storesSuccess: "MsgOk[Stores List Received]",
        storesError: "",
        storesLoading: false,
      };
      break;
    case GET_STORES_LIST_FOR_LOGIN_ERROR:
      state = {
        ...state,
        storesList: [],
        storesListTotal: 0,
        storesError: action.payload,
        storesSuccess: "",
        storesLoading: false,
      };
      break;

    // Get Stores List
    case GET_STORES_LIST:
      state = {
        ...state,
        storesLoading: true,
        storesSuccess: "",
        storesError: "",
      };
      break;
    case GET_STORES_LIST_SUCCESS:
      state = {
        ...state,
        storesList: action.payload.list,
        storesListTotal: action.payload.total,
        storesSuccess: "MsgOk[Stores List Received]",
        storesError: "",
        storesLoading: false,
      };
      break;
    case GET_STORES_LIST_ERROR:
      state = {
        ...state,
        storesList: [],
        storesListTotal: 0,
        storesError: action.payload,
        storesSuccess: "",
        storesLoading: false,
      };
      break;

    // Get Store Data
    case GET_STORE_DATA:
      state = {
        ...state,
        storesLoading: true,
        storesSuccess: "",
        storesError: "",
      };
      break;
    case GET_STORE_DATA_SUCCESS:
      state = {
        ...state,
        storeData: action.payload,
        storesSuccess: "MsgOk[Store Data Received]",
        storesError: "",
        storesLoading: false,
      };
      break;
    case GET_STORE_DATA_ERROR:
      state = {
        ...state,
        storeData: {},
        storesError: action.payload,
        storesSuccess: "",
        storesLoading: false,
      };
      break;

    // Add Store
    case ADD_STORE:
      state = {
        ...state,
        storesLoading: true,
        storesSuccess: "",
        storesError: "",
      };
      break;
    case ADD_STORE_SUCCESS:
      state = {
        ...state,
        storesSuccess: action.payload,
        storesError: "",
        storesLoading: false,
      };
      break;
    case ADD_STORE_ERROR:
      state = {
        ...state,
        storesError: action.payload,
        storesSuccess: "",
        storesLoading: false,
      };
      break;

    // Modify Store Data
    case MODIFY_STORE_DATA:
      state = {
        ...state,
        storesLoading: true,
        storesSuccess: "",
        storesError: "",
      };
      break;
    case MODIFY_STORE_DATA_SUCCESS:
      state = {
        ...state,
        storesSuccess: action.payload,
        storesError: "",
        storesLoading: false,
      };
      break;
    case MODIFY_STORE_DATA_ERROR:
      state = {
        ...state,
        storesError: action.payload,
        storesSuccess: "",
        storesLoading: false,
      };
      break;

    // Delete Store
    case DELETE_STORE:
      state = {
        ...state,
        storesLoading: true,
        storesSuccess: "",
        storesError: "",
      };
      break;
    case DELETE_STORE_SUCCESS:
      state = {
        ...state,
        storesSuccess: action.payload,
        storesError: "",
        storesLoading: false,
      };
      break;
    case DELETE_STORE_ERROR:
      state = {
        ...state,
        storesError: action.payload,
        storesSuccess: "",
        storesLoading: false,
      };
      break;

    // Reset especifico
    case RESET_STORES:
      state = initialState;
      break;

    // Reset general
    case RESET:
      state = initialState;
      break;

    default:
      state = { ...state };
      break;
  }

  return state;
};

export default stores;
