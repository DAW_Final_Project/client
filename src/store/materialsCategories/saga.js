import { takeEvery, put, call } from "redux-saga/effects";

// ActionTypes
import {
  // Get MaterialsCategories List
  GET_MATERIALSCATEGORIES_LIST,
  // Get MaterialsCategory Data
  GET_MATERIALSCATEGORY_DATA,
  // Add MaterialsCategory
  ADD_MATERIALSCATEGORY,
  // Modify MaterialsCategory Data
  MODIFY_MATERIALSCATEGORY_DATA,
  // Delete MaterialsCategory
  DELETE_MATERIALSCATEGORY,
} from "./actionTypes";

// Actions
import {
  // Get MaterialsCategories List
  getMaterialsCategoriesListSuccess,
  getMaterialsCategoriesListError,
  // Get MaterialsCategory Data
  getMaterialsCategoryDataSuccess,
  getMaterialsCategoryDataError,
  // Add MaterialsCategory
  addMaterialsCategoriesuccess,
  addMaterialsCategoryError,
  // Modify MaterialsCategory Data
  modifyMaterialsCategoryDataSuccess,
  modifyMaterialsCategoryDataError,
  // Delete MaterialsCategory
  deleteMaterialsCategoriesuccess,
  deleteMaterialsCategoryError,
} from "./actions";

// Api functions
import * as api from "api/materialscategories/materialscategories.service";

/* --- FUNCTIONS --- */
// Get MaterialsCategories List
function* getMaterialsCategoriesList({ params: { sort, order, limit, skip, navigate } }) {
  try {
    const response = yield call(api.getMaterialsCategoriesList, sort, order, limit, skip);
    if (response.status === 200) {
      yield put(getMaterialsCategoriesListSuccess(response.data));
    } else {
      yield put(getMaterialsCategoriesListError("MsgErr[Get MaterialsCategories List Failed]"));
    }
  } catch (error) {
    yield put(getMaterialsCategoriesListError(error));
  }
}

// Get MaterialsCategory Data
function* getMaterialsCategoryData({ params: { materialscategory_id, navigate } }) {
  try {
    const response = yield call(api.getMaterialsCategoryData, materialscategory_id);
    if (response.status === 200) {
      yield put(getMaterialsCategoryDataSuccess(response.data[0]));
    } else {
      yield put(getMaterialsCategoryDataError("MsgErr[Get MaterialsCategory Data Failed]"));
    }
  } catch (error) {
    yield put(getMaterialsCategoryDataError(error));
  }
}

// Add MaterialsCategory
function* addMaterialsCategory({ params: { data, navigate } }) {
  try {
    const response = yield call(api.addMaterialsCategory, data);
    if (response.status === 200) {
      yield put(addMaterialsCategoriesuccess("MsgOk[MaterialsCategory Added]"));
    } else {
      yield put(addMaterialsCategoryError("MsgErr[Get MaterialsCategory Data Failed]"));
    }
  } catch (error) {
    yield put(addMaterialsCategoryError(error));
  }
}
// Modify MaterialsCategory Data
function* modifyMaterialsCategoryData({ params: { materialscategory_id, data, navigate } }) {
  try {
    const response = yield call(api.modifyMaterialsCategoryData, materialscategory_id, data);
    if (response.status === 200) {
      yield put(modifyMaterialsCategoryDataSuccess("MsgOk[MaterialsCategory Data Modified]"));
    } else {
      yield put(modifyMaterialsCategoryDataError("MsgErr[Get MaterialsCategory Data Failed]"));
    }
  } catch (error) {
    yield put(modifyMaterialsCategoryDataError(error));
  }
}
// Delete MaterialsCategory
function* deleteMaterialsCategory({ params: { materialscategory_id, navigate } }) {
  try {
    const response = yield call(api.deleteMaterialsCategory, materialscategory_id);
    if (response.status === 200) {
      yield put(deleteMaterialsCategoriesuccess("MsgOk[MaterialsCategory Data Modified]"));
    } else {
      yield put(deleteMaterialsCategoryError("MsgErr[Get MaterialsCategory Data Failed]"));
    }
  } catch (error) {
    yield put(deleteMaterialsCategoryError(error));
  }
}

function* MaterialsCategoriesSaga() {
  yield takeEvery(GET_MATERIALSCATEGORIES_LIST, getMaterialsCategoriesList);
  yield takeEvery(GET_MATERIALSCATEGORY_DATA, getMaterialsCategoryData);
  yield takeEvery(ADD_MATERIALSCATEGORY, addMaterialsCategory);
  yield takeEvery(MODIFY_MATERIALSCATEGORY_DATA, modifyMaterialsCategoryData);
  yield takeEvery(DELETE_MATERIALSCATEGORY, deleteMaterialsCategory);
}

export default MaterialsCategoriesSaga;