// Get MaterialsCategories List
export const GET_MATERIALSCATEGORIES_LIST = "GET_MATERIALSCATEGORIES_LIST"
export const GET_MATERIALSCATEGORIES_LIST_SUCCESS = "GET_MATERIALSCATEGORIES_LIST_SUCCESS"
export const GET_MATERIALSCATEGORIES_LIST_ERROR = "GET_MATERIALSCATEGORIES_LIST_ERROR"

// Get MaterialsCategory Data
export const GET_MATERIALSCATEGORY_DATA = "GET_MATERIALSCATEGORY_DATA"
export const GET_MATERIALSCATEGORY_DATA_SUCCESS = "GET_MATERIALSCATEGORY_DATA_SUCCESS"
export const GET_MATERIALSCATEGORY_DATA_ERROR = "GET_MATERIALSCATEGORY_DATA_ERROR"

// Add MaterialsCategory
export const ADD_MATERIALSCATEGORY = "ADD_MATERIALSCATEGORY"
export const ADD_MATERIALSCATEGORY_SUCCESS = "ADD_MATERIALSCATEGORY_SUCCESS"
export const ADD_MATERIALSCATEGORY_ERROR = "ADD_MATERIALSCATEGORY_ERROR"

// Modify MaterialsCategory Data
export const MODIFY_MATERIALSCATEGORY_DATA = "MODIFY_MATERIALSCATEGORY_DATA"
export const MODIFY_MATERIALSCATEGORY_DATA_SUCCESS = "MODIFY_MATERIALSCATEGORY_DATA_SUCCESS"
export const MODIFY_MATERIALSCATEGORY_DATA_ERROR = "MODIFY_MATERIALSCATEGORY_DATA_ERROR"

// Delete MaterialsCategory
export const DELETE_MATERIALSCATEGORY = "DELETE_MATERIALSCATEGORY"
export const DELETE_MATERIALSCATEGORY_SUCCESS = "DELETE_MATERIALSCATEGORY_SUCCESS"
export const DELETE_MATERIALSCATEGORY_ERROR = "DELETE_MATERIALSCATEGORY_ERROR"

// Specifict Reset
export const RESET_MATERIALSCATEGORIES = "RESET_MATERIALSCATEGORIES"

// General Reset
export const RESET = "RESET"
