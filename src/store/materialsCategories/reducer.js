// ActionTypes
import {
  // Get MaterialsCategories List
  GET_MATERIALSCATEGORIES_LIST,
  GET_MATERIALSCATEGORIES_LIST_SUCCESS,
  GET_MATERIALSCATEGORIES_LIST_ERROR,
  // Get MaterialsCategory Data
  GET_MATERIALSCATEGORY_DATA,
  GET_MATERIALSCATEGORY_DATA_SUCCESS,
  GET_MATERIALSCATEGORY_DATA_ERROR,
  // Add MaterialsCategory
  ADD_MATERIALSCATEGORY,
  ADD_MATERIALSCATEGORY_SUCCESS,
  ADD_MATERIALSCATEGORY_ERROR,
  // Modify MaterialsCategory Data
  MODIFY_MATERIALSCATEGORY_DATA,
  MODIFY_MATERIALSCATEGORY_DATA_SUCCESS,
  MODIFY_MATERIALSCATEGORY_DATA_ERROR,
  // Delete MaterialsCategory
  DELETE_MATERIALSCATEGORY,
  DELETE_MATERIALSCATEGORY_SUCCESS,
  DELETE_MATERIALSCATEGORY_ERROR,
  // Specific Reset
  RESET_MATERIALSCATEGORIES,
  // General Reset
  RESET,
} from "./actionTypes";

const initialState = {
  materialscategoriesError: "",
  materialscategoriesnSuccess: "",
  materialscategoriesLoading: false,
  materialscategoriesList: [],
  materialscategoriesListTotal: 0,
  materialscategoryData: {},
};

const materialscategories = (state = initialState, action) => {
  switch (action.type) {
    // Get MaterialsCategories List
    case GET_MATERIALSCATEGORIES_LIST:
      state = {
        ...state,
        materialscategoriesLoading: true,
        materialscategoriesSuccess: "",
        materialscategoriesError: "",
      };
      break;
    case GET_MATERIALSCATEGORIES_LIST_SUCCESS:
      state = {
        ...state,
        materialscategoriesList: action.payload.list,
        materialscategoriesListTotal: action.payload.total,
        materialscategoriesSuccess: "MsgOk[MaterialsCategories List Received]",
        materialscategoriesError: "",
        materialscategoriesLoading: false,
      };
      break;
    case GET_MATERIALSCATEGORIES_LIST_ERROR:
      state = {
        ...state,
        materialscategoriesList: [],
        materialscategoriesListTotal: 0,
        materialscategoriesError: action.payload,
        materialscategoriesSuccess: "",
        materialscategoriesLoading: false,
      };
      break;

    // Get MaterialsCategory Data
    case GET_MATERIALSCATEGORY_DATA:
      state = {
        ...state,
        materialscategoriesLoading: true,
        materialscategoriesSuccess: "",
        materialscategoriesError: "",
      };
      break;
    case GET_MATERIALSCATEGORY_DATA_SUCCESS:
      state = {
        ...state,
        materialscategoryData: action.payload,
        materialscategoriesSuccess: "MsgOk[MaterialsCategory Data Received]",
        materialscategoriesError: "",
        materialscategoriesLoading: false,
      };
      break;
    case GET_MATERIALSCATEGORY_DATA_ERROR:
      state = {
        ...state,
        materialscategoryData: {},
        materialscategoriesError: action.payload,
        materialscategoriesSuccess: "",
        materialscategoriesLoading: false,
      };
      break;

    // Add MaterialsCategory
    case ADD_MATERIALSCATEGORY:
      state = {
        ...state,
        materialscategoriesLoading: true,
        materialscategoriesSuccess: "",
        materialscategoriesError: "",
      };
      break;
    case ADD_MATERIALSCATEGORY_SUCCESS:
      state = {
        ...state,
        materialscategoriesSuccess: action.payload,
        materialscategoriesError: "",
        materialscategoriesLoading: false,
      };
      break;
    case ADD_MATERIALSCATEGORY_ERROR:
      state = {
        ...state,
        materialscategoriesError: action.payload,
        materialscategoriesSuccess: "",
        materialscategoriesLoading: false,
      };
      break;

    // Modify MaterialsCategory Data
    case MODIFY_MATERIALSCATEGORY_DATA:
      state = {
        ...state,
        materialscategoriesLoading: true,
        materialscategoriesSuccess: "",
        materialscategoriesError: "",
      };
      break;
    case MODIFY_MATERIALSCATEGORY_DATA_SUCCESS:
      state = {
        ...state,
        materialscategoriesSuccess: action.payload,
        materialscategoriesError: "",
        materialscategoriesLoading: false,
      };
      break;
    case MODIFY_MATERIALSCATEGORY_DATA_ERROR:
      state = {
        ...state,
        materialscategoriesError: action.payload,
        materialscategoriesSuccess: "",
        materialscategoriesLoading: false,
      };
      break;

    // Delete MaterialsCategory
    case DELETE_MATERIALSCATEGORY:
      state = {
        ...state,
        materialscategoriesLoading: true,
        materialscategoriesSuccess: "",
        materialscategoriesError: "",
      };
      break;
    case DELETE_MATERIALSCATEGORY_SUCCESS:
      state = {
        ...state,
        materialscategoriesSuccess: action.payload,
        materialscategoriesError: "",
        materialscategoriesLoading: false,
      };
      break;
    case DELETE_MATERIALSCATEGORY_ERROR:
      state = {
        ...state,
        materialscategoriesError: action.payload,
        materialscategoriesSuccess: "",
        materialscategoriesLoading: false,
      };
      break;

    // Reset especifico
    case RESET_MATERIALSCATEGORIES:
      state = initialState;
      break;

    // Reset general
    case RESET:
      state = initialState;
      break;

    default:
      state = { ...state };
      break;
  }

  return state;
};

export default materialscategories;
