// ActionTypes
import {
  // Get MaterialsCategories List
  GET_MATERIALSCATEGORIES_LIST,
  GET_MATERIALSCATEGORIES_LIST_SUCCESS,
  GET_MATERIALSCATEGORIES_LIST_ERROR,
  // Get MaterialsCategory Data
  GET_MATERIALSCATEGORY_DATA,
  GET_MATERIALSCATEGORY_DATA_SUCCESS,
  GET_MATERIALSCATEGORY_DATA_ERROR,
  // Add MaterialsCategory
  ADD_MATERIALSCATEGORY,
  ADD_MATERIALSCATEGORY_SUCCESS,
  ADD_MATERIALSCATEGORY_ERROR,
  // Modify MaterialsCategory Data
  MODIFY_MATERIALSCATEGORY_DATA,
  MODIFY_MATERIALSCATEGORY_DATA_SUCCESS,
  MODIFY_MATERIALSCATEGORY_DATA_ERROR,
  // Delete MaterialsCategory
  DELETE_MATERIALSCATEGORY,
  DELETE_MATERIALSCATEGORY_SUCCESS,
  DELETE_MATERIALSCATEGORY_ERROR,
} from "./actionTypes";

// Get MaterialsCategories List
export const getMaterialsCategoriesList = (sort, order, limit, skip, navigate) => ({
  type: GET_MATERIALSCATEGORIES_LIST,
  params: { sort, order, limit, skip, navigate },
});

export const getMaterialsCategoriesListSuccess = (obj) => ({
  type: GET_MATERIALSCATEGORIES_LIST_SUCCESS,
  payload: obj,
});

export const getMaterialsCategoriesListError = (error) => ({
  type: GET_MATERIALSCATEGORIES_LIST_ERROR,
  payload: error,
});

// Get MaterialsCategory Data
export const getMaterialsCategoryData = (materialscategory_id, navigate) => ({
  type: GET_MATERIALSCATEGORY_DATA,
  params: { materialscategory_id, navigate },
});

export const getMaterialsCategoryDataSuccess = (obj) => ({
  type: GET_MATERIALSCATEGORY_DATA_SUCCESS,
  payload: obj,
});

export const getMaterialsCategoryDataError = (error) => ({
  type: GET_MATERIALSCATEGORY_DATA_ERROR,
  payload: error,
});

// Add MaterialsCategory
export const addMaterialsCategory = (data, navigate) => ({
  type: ADD_MATERIALSCATEGORY,
  params: { data, navigate },
});

export const addMaterialsCategoriesuccess = (obj) => ({
  type: ADD_MATERIALSCATEGORY_SUCCESS,
  payload: obj,
});

export const addMaterialsCategoryError = (error) => ({
  type: ADD_MATERIALSCATEGORY_ERROR,
  payload: error,
});

// Modify MaterialsCategory Data
export const modifyMaterialsCategoryData = (materialscategory_id, data, navigate) => ({
  type: MODIFY_MATERIALSCATEGORY_DATA,
  params: { materialscategory_id, data, navigate },
});

export const modifyMaterialsCategoryDataSuccess = (obj) => ({
  type: MODIFY_MATERIALSCATEGORY_DATA_SUCCESS,
  payload: obj,
});

export const modifyMaterialsCategoryDataError = (error) => ({
  type: MODIFY_MATERIALSCATEGORY_DATA_ERROR,
  payload: error,
});

// Delete MaterialsCategory
export const deleteMaterialsCategory = (materialscategory_id, navigate) => ({
  type: DELETE_MATERIALSCATEGORY,
  params: { materialscategory_id, navigate },
});

export const deleteMaterialsCategoriesuccess = (obj) => ({
  type: DELETE_MATERIALSCATEGORY_SUCCESS,
  payload: obj,
});

export const deleteMaterialsCategoryError = (error) => ({
  type: DELETE_MATERIALSCATEGORY_ERROR,
  payload: error,
});
