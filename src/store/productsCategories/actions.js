// ActionTypes
import {
  // Get ProductsCategories List
  GET_PRODUCTSCATEGORIES_LIST,
  GET_PRODUCTSCATEGORIES_LIST_SUCCESS,
  GET_PRODUCTSCATEGORIES_LIST_ERROR,
  // Get ProductsCategory Data
  GET_PRODUCTSCATEGORY_DATA,
  GET_PRODUCTSCATEGORY_DATA_SUCCESS,
  GET_PRODUCTSCATEGORY_DATA_ERROR,
  // Add ProductsCategory
  ADD_PRODUCTSCATEGORY,
  ADD_PRODUCTSCATEGORY_SUCCESS,
  ADD_PRODUCTSCATEGORY_ERROR,
  // Modify ProductsCategory Data
  MODIFY_PRODUCTSCATEGORY_DATA,
  MODIFY_PRODUCTSCATEGORY_DATA_SUCCESS,
  MODIFY_PRODUCTSCATEGORY_DATA_ERROR,
  // Delete ProductsCategory
  DELETE_PRODUCTSCATEGORY,
  DELETE_PRODUCTSCATEGORY_SUCCESS,
  DELETE_PRODUCTSCATEGORY_ERROR,
} from "./actionTypes";

// Get ProductsCategories List
export const getProductsCategoriesList = (sort, order, limit, skip, navigate) => ({
  type: GET_PRODUCTSCATEGORIES_LIST,
  params: { sort, order, limit, skip, navigate },
});

export const getProductsCategoriesListSuccess = (obj) => ({
  type: GET_PRODUCTSCATEGORIES_LIST_SUCCESS,
  payload: obj,
});

export const getProductsCategoriesListError = (error) => ({
  type: GET_PRODUCTSCATEGORIES_LIST_ERROR,
  payload: error,
});

// Get ProductsCategory Data
export const getProductsCategoryData = (productscategory_id, navigate) => ({
  type: GET_PRODUCTSCATEGORY_DATA,
  params: { productscategory_id, navigate },
});

export const getProductsCategoryDataSuccess = (obj) => ({
  type: GET_PRODUCTSCATEGORY_DATA_SUCCESS,
  payload: obj,
});

export const getProductsCategoryDataError = (error) => ({
  type: GET_PRODUCTSCATEGORY_DATA_ERROR,
  payload: error,
});

// Add ProductsCategory
export const addProductsCategory = (data, navigate) => ({
  type: ADD_PRODUCTSCATEGORY,
  params: { data, navigate },
});

export const addProductsCategoriesuccess = (obj) => ({
  type: ADD_PRODUCTSCATEGORY_SUCCESS,
  payload: obj,
});

export const addProductsCategoryError = (error) => ({
  type: ADD_PRODUCTSCATEGORY_ERROR,
  payload: error,
});

// Modify ProductsCategory Data
export const modifyProductsCategoryData = (productscategory_id, data, navigate) => ({
  type: MODIFY_PRODUCTSCATEGORY_DATA,
  params: { productscategory_id, data, navigate },
});

export const modifyProductsCategoryDataSuccess = (obj) => ({
  type: MODIFY_PRODUCTSCATEGORY_DATA_SUCCESS,
  payload: obj,
});

export const modifyProductsCategoryDataError = (error) => ({
  type: MODIFY_PRODUCTSCATEGORY_DATA_ERROR,
  payload: error,
});

// Delete ProductsCategory
export const deleteProductsCategory = (productscategory_id, navigate) => ({
  type: DELETE_PRODUCTSCATEGORY,
  params: { productscategory_id, navigate },
});

export const deleteProductsCategoriesuccess = (obj) => ({
  type: DELETE_PRODUCTSCATEGORY_SUCCESS,
  payload: obj,
});

export const deleteProductsCategoryError = (error) => ({
  type: DELETE_PRODUCTSCATEGORY_ERROR,
  payload: error,
});
