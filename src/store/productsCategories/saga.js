import { takeEvery, put, call } from "redux-saga/effects";

// ActionTypes
import {
  // Get ProductsCategories List
  GET_PRODUCTSCATEGORIES_LIST,
  // Get ProductsCategory Data
  GET_PRODUCTSCATEGORY_DATA,
  // Add ProductsCategory
  ADD_PRODUCTSCATEGORY,
  // Modify ProductsCategory Data
  MODIFY_PRODUCTSCATEGORY_DATA,
  // Delete ProductsCategory
  DELETE_PRODUCTSCATEGORY,
} from "./actionTypes";

// Actions
import {
  // Get ProductsCategories List
  getProductsCategoriesListSuccess,
  getProductsCategoriesListError,
  // Get ProductsCategory Data
  getProductsCategoryDataSuccess,
  getProductsCategoryDataError,
  // Add ProductsCategory
  addProductsCategoriesuccess,
  addProductsCategoryError,
  // Modify ProductsCategory Data
  modifyProductsCategoryDataSuccess,
  modifyProductsCategoryDataError,
  // Delete ProductsCategory
  deleteProductsCategoriesuccess,
  deleteProductsCategoryError,
} from "./actions";

// Api functions
import * as api from "api/productscategories/productscategories.service";

/* --- FUNCTIONS --- */
// Get ProductsCategories List
function* getProductsCategoriesList({ params: { sort, order, limit, skip, navigate } }) {
  try {
    const response = yield call(api.getProductsCategoriesList, sort, order, limit, skip);
    if (response.status === 200) {
      yield put(getProductsCategoriesListSuccess(response.data));
    } else {
      yield put(getProductsCategoriesListError("MsgErr[Get ProductsCategories List Failed]"));
    }
  } catch (error) {
    yield put(getProductsCategoriesListError(error));
  }
}

// Get ProductsCategory Data
function* getProductsCategoryData({ params: { productscategory_id, navigate } }) {
  try {
    const response = yield call(api.getProductsCategoryData, productscategory_id);
    if (response.status === 200) {
      yield put(getProductsCategoryDataSuccess(response.data[0]));
    } else {
      yield put(getProductsCategoryDataError("MsgErr[Get ProductsCategory Data Failed]"));
    }
  } catch (error) {
    yield put(getProductsCategoryDataError(error));
  }
}

// Add ProductsCategory
function* addProductsCategory({ params: { data, navigate } }) {
  try {
    const response = yield call(api.addProductsCategory, data);
    if (response.status === 200) {
      yield put(addProductsCategoriesuccess("MsgOk[ProductsCategory Added]"));
    } else {
      yield put(addProductsCategoryError("MsgErr[Get ProductsCategory Data Failed]"));
    }
  } catch (error) {
    yield put(addProductsCategoryError(error));
  }
}
// Modify ProductsCategory Data
function* modifyProductsCategoryData({ params: { productscategory_id, data, navigate } }) {
  try {
    const response = yield call(api.modifyProductsCategoryData, productscategory_id, data);
    if (response.status === 200) {
      yield put(modifyProductsCategoryDataSuccess("MsgOk[ProductsCategory Data Modified]"));
    } else {
      yield put(modifyProductsCategoryDataError("MsgErr[Get ProductsCategory Data Failed]"));
    }
  } catch (error) {
    yield put(modifyProductsCategoryDataError(error));
  }
}
// Delete ProductsCategory
function* deleteProductsCategory({ params: { productscategory_id, navigate } }) {
  try {
    const response = yield call(api.deleteProductsCategory, productscategory_id);
    if (response.status === 200) {
      yield put(deleteProductsCategoriesuccess("MsgOk[ProductsCategory Data Modified]"));
    } else {
      yield put(deleteProductsCategoryError("MsgErr[Get ProductsCategory Data Failed]"));
    }
  } catch (error) {
    yield put(deleteProductsCategoryError(error));
  }
}

function* ProductsCategoriesSaga() {
  yield takeEvery(GET_PRODUCTSCATEGORIES_LIST, getProductsCategoriesList);
  yield takeEvery(GET_PRODUCTSCATEGORY_DATA, getProductsCategoryData);
  yield takeEvery(ADD_PRODUCTSCATEGORY, addProductsCategory);
  yield takeEvery(MODIFY_PRODUCTSCATEGORY_DATA, modifyProductsCategoryData);
  yield takeEvery(DELETE_PRODUCTSCATEGORY, deleteProductsCategory);
}

export default ProductsCategoriesSaga;