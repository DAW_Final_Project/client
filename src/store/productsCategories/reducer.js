// ActionTypes
import {
  // Get ProductsCategories List
  GET_PRODUCTSCATEGORIES_LIST,
  GET_PRODUCTSCATEGORIES_LIST_SUCCESS,
  GET_PRODUCTSCATEGORIES_LIST_ERROR,
  // Get ProductsCategory Data
  GET_PRODUCTSCATEGORY_DATA,
  GET_PRODUCTSCATEGORY_DATA_SUCCESS,
  GET_PRODUCTSCATEGORY_DATA_ERROR,
  // Add ProductsCategory
  ADD_PRODUCTSCATEGORY,
  ADD_PRODUCTSCATEGORY_SUCCESS,
  ADD_PRODUCTSCATEGORY_ERROR,
  // Modify ProductsCategory Data
  MODIFY_PRODUCTSCATEGORY_DATA,
  MODIFY_PRODUCTSCATEGORY_DATA_SUCCESS,
  MODIFY_PRODUCTSCATEGORY_DATA_ERROR,
  // Delete ProductsCategory
  DELETE_PRODUCTSCATEGORY,
  DELETE_PRODUCTSCATEGORY_SUCCESS,
  DELETE_PRODUCTSCATEGORY_ERROR,
  // Specific Reset
  RESET_PRODUCTSCATEGORIES,
  // General Reset
  RESET,
} from "./actionTypes";

const initialState = {
  productscategoriesError: "",
  productscategoriesnSuccess: "",
  productscategoriesLoading: false,
  productscategoriesList: [],
  productscategoriesListTotal: 0,
  productscategoryData: {},
};

const productscategories = (state = initialState, action) => {
  switch (action.type) {
    // Get ProductsCategories List
    case GET_PRODUCTSCATEGORIES_LIST:
      state = {
        ...state,
        productscategoriesLoading: true,
        productscategoriesSuccess: "",
        productscategoriesError: "",
      };
      break;
    case GET_PRODUCTSCATEGORIES_LIST_SUCCESS:
      state = {
        ...state,
        productscategoriesList: action.payload.list,
        productscategoriesListTotal: action.payload.total,
        productscategoriesSuccess: "MsgOk[ProductsCategories List Received]",
        productscategoriesError: "",
        productscategoriesLoading: false,
      };
      break;
    case GET_PRODUCTSCATEGORIES_LIST_ERROR:
      state = {
        ...state,
        productscategoriesList: [],
        productscategoriesListTotal: 0,
        productscategoriesError: action.payload,
        productscategoriesSuccess: "",
        productscategoriesLoading: false,
      };
      break;

    // Get ProductsCategory Data
    case GET_PRODUCTSCATEGORY_DATA:
      state = {
        ...state,
        productscategoriesLoading: true,
        productscategoriesSuccess: "",
        productscategoriesError: "",
      };
      break;
    case GET_PRODUCTSCATEGORY_DATA_SUCCESS:
      state = {
        ...state,
        productscategoryData: action.payload,
        productscategoriesSuccess: "MsgOk[ProductsCategory Data Received]",
        productscategoriesError: "",
        productscategoriesLoading: false,
      };
      break;
    case GET_PRODUCTSCATEGORY_DATA_ERROR:
      state = {
        ...state,
        productscategoryData: {},
        productscategoriesError: action.payload,
        productscategoriesSuccess: "",
        productscategoriesLoading: false,
      };
      break;

    // Add ProductsCategory
    case ADD_PRODUCTSCATEGORY:
      state = {
        ...state,
        productscategoriesLoading: true,
        productscategoriesSuccess: "",
        productscategoriesError: "",
      };
      break;
    case ADD_PRODUCTSCATEGORY_SUCCESS:
      state = {
        ...state,
        productscategoriesSuccess: action.payload,
        productscategoriesError: "",
        productscategoriesLoading: false,
      };
      break;
    case ADD_PRODUCTSCATEGORY_ERROR:
      state = {
        ...state,
        productscategoriesError: action.payload,
        productscategoriesSuccess: "",
        productscategoriesLoading: false,
      };
      break;

    // Modify ProductsCategory Data
    case MODIFY_PRODUCTSCATEGORY_DATA:
      state = {
        ...state,
        productscategoriesLoading: true,
        productscategoriesSuccess: "",
        productscategoriesError: "",
      };
      break;
    case MODIFY_PRODUCTSCATEGORY_DATA_SUCCESS:
      state = {
        ...state,
        productscategoriesSuccess: action.payload,
        productscategoriesError: "",
        productscategoriesLoading: false,
      };
      break;
    case MODIFY_PRODUCTSCATEGORY_DATA_ERROR:
      state = {
        ...state,
        productscategoriesError: action.payload,
        productscategoriesSuccess: "",
        productscategoriesLoading: false,
      };
      break;

    // Delete ProductsCategory
    case DELETE_PRODUCTSCATEGORY:
      state = {
        ...state,
        productscategoriesLoading: true,
        productscategoriesSuccess: "",
        productscategoriesError: "",
      };
      break;
    case DELETE_PRODUCTSCATEGORY_SUCCESS:
      state = {
        ...state,
        productscategoriesSuccess: action.payload,
        productscategoriesError: "",
        productscategoriesLoading: false,
      };
      break;
    case DELETE_PRODUCTSCATEGORY_ERROR:
      state = {
        ...state,
        productscategoriesError: action.payload,
        productscategoriesSuccess: "",
        productscategoriesLoading: false,
      };
      break;

    // Reset especifico
    case RESET_PRODUCTSCATEGORIES:
      state = initialState;
      break;

    // Reset general
    case RESET:
      state = initialState;
      break;

    default:
      state = { ...state };
      break;
  }

  return state;
};

export default productscategories;
