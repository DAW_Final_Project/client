import { all } from "redux-saga/effects";

// Access
import AccessSaga from "./access/saga"
// Authentication
import AuthenticationSaga from "./authentication/saga"
// Clients
import ClientsSaga from "./clients/saga"
// Materials
import MaterialsSaga from "./materials/saga"
// MaterialsCategories
import MaterialsCategoriesSaga from "./materialsCategories/saga"
// Operations
import OperationsSaga from "./operations/saga"
// Products
import ProductsSaga from "./products/saga"
// ProductsCategories
import ProductsCategoriesSaga from "./productsCategories/saga"
// Roles
import RolesSaga from "./roles/saga"
// Services
import ServicesSaga from "./services/saga"
// Status
import StatusSaga from "./status/saga"
// Stores
import StoresSaga from "./stores/saga"
// Trademarks
import TrademarksSaga from "./trademarks/saga"
// Users
import UsersSaga from "./users/saga"

export default function* rootSaga() {
  yield all([
    AccessSaga(),
    AuthenticationSaga(),
    ClientsSaga(),
    MaterialsSaga(),
    MaterialsCategoriesSaga(),
    OperationsSaga(),
    ProductsSaga(),
    ProductsCategoriesSaga(),
    RolesSaga(),
    ServicesSaga(),
    StatusSaga(),
    StoresSaga(),
    TrademarksSaga(),
    UsersSaga(),
  ]);
}
