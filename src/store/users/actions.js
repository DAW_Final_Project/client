// ActionTypes
import {
  // Get Users List
  GET_USERS_LIST,
  GET_USERS_LIST_SUCCESS,
  GET_USERS_LIST_ERROR,
  // Get User Data
  GET_USER_DATA,
  GET_USER_DATA_SUCCESS,
  GET_USER_DATA_ERROR,
} from "./actionTypes";

 // Get Users List
 export const getUsersList = (sort, order, limit, skip, navigate) => ({
  type: GET_USERS_LIST,
  params: { sort, order, limit, skip, navigate },
});

export const getUsersListSuccess = (obj) => ({
  type: GET_USERS_LIST_SUCCESS,
  payload: obj,
});

export const getUsersListError = (error) => ({
  type: GET_USERS_LIST_ERROR,
  payload: error,
});

// Get User Data
export const getUserData = (user_id, navigate) => ({
 type: GET_USER_DATA,
 params: { user_id, navigate },
});

export const getUserDataSuccess = (obj) => ({
 type: GET_USER_DATA_SUCCESS,
 payload: obj,
});

export const getUserDataError = (error) => ({
 type: GET_USER_DATA_ERROR,
 payload: error,
});
