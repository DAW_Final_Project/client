import { takeEvery, put, call } from "redux-saga/effects";

// ActionTypes
import {
  // Get Users List
  GET_USERS_LIST,
  // Get User Data
  GET_USER_DATA,
} from "./actionTypes";

// Actions
import {
  // Get Users List
  getUsersListSuccess,
  getUsersListError,
  // Get User Data
  getUserDataSuccess,
  getUserDataError,
} from "./actions";

// Api functions
import * as api from "api/users/users.service";

/* --- FUNCTIONS --- */
// Get Users List
function* getUsersList({ params: { sort, order, limit, skip, navigate } }) {
  try {
    const response = yield call(api.getUsersList, sort, order, limit, skip);
    if (response.status === 200) {
      yield put(getUsersListSuccess(response.data));
    } else {
      yield put(getUsersListError("MsgErr[Get Users List Failed]"));
    }
  } catch (error) {
    yield put(getUsersListError(error));
  }
}

// Get User Data
function* getUserData({ params: { user_id, navigate } }) {
  try {
    const response = yield call(api.getUserData, user_id);
    if (response.status === 200) {
      yield put(getUserDataSuccess(response.data[0]));
    } else {
      yield put(getUserDataError("MsgErr[Get User Data Failed]"));
    }
  } catch (error) {
    yield put(getUserDataError(error));
  }
}

function* UsersSaga() {
  yield takeEvery(GET_USERS_LIST, getUsersList);
  yield takeEvery(GET_USER_DATA, getUserData);
}

export default UsersSaga;
