// ActionTypes
import {
  // Get Users List
  GET_USERS_LIST,
  GET_USERS_LIST_SUCCESS,
  GET_USERS_LIST_ERROR,
  // Get User Data
  GET_USER_DATA,
  GET_USER_DATA_SUCCESS,
  GET_USER_DATA_ERROR,
  // Specific Reset
  RESET_USERS,
  // General Reset
  RESET,
} from "./actionTypes";

const initialState = {
  usersError: "",
  usersnSuccess: "",
  usersLoading: false,
  usersList: [],
  usersListTotal: 0,
  userData: {},
};

const users = (state = initialState, action) => {
  switch (action.type) {
    // Get Users List
    case GET_USERS_LIST:
      state = {
        ...state,
        usersLoading: true,
        usersSuccess: "",
        usersError: "",
      };
      break;
    case GET_USERS_LIST_SUCCESS:
      state = {
        ...state,
        usersList: action.payload.list,
        usersListTotal: action.payload.total,
        usersSuccess: "MsgOk[Users List Received]",
        usersError: "",
        usersLoading: false,
      };
      break;
    case GET_USERS_LIST_ERROR:
      state = {
        ...state,
        usersList: [],
        usersListTotal: 0,
        usersError: action.payload,
        usersSuccess: "",
        usersLoading: false,
      };
      break;

    // Get User Data
    case GET_USER_DATA:
      state = {
        ...state,
        usersLoading: true,
        usersSuccess: "",
        usersError: "",
      };
      break;
    case GET_USER_DATA_SUCCESS:
      state = {
        ...state,
        userData: action.payload,
        usersSuccess: "MsgOk[User Data Received]",
        usersError: "",
        usersLoading: false,
      };
      break;
    case GET_USER_DATA_ERROR:
      state = {
        ...state,
        userData: {},
        usersError: action.payload,
        usersSuccess: "",
        usersLoading: false,
      };
      break;

    // Reset especifico
    case RESET_USERS:
      state = initialState;
      break;

    // Reset general
    case RESET:
      state = initialState;
      break;

    default:
      state = { ...state };
      break;
  }

  return state;
};

export default users;
