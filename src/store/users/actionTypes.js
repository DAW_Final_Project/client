// Get Users List
export const GET_USERS_LIST = "GET_USERS_LIST"
export const GET_USERS_LIST_SUCCESS = "GET_USERS_LIST_SUCCESS"
export const GET_USERS_LIST_ERROR = "GET_USERS_LIST_ERROR"

// Get User Data
export const GET_USER_DATA = "GET_USER_DATA"
export const GET_USER_DATA_SUCCESS = "GET_USER_DATA_SUCCESS"
export const GET_USER_DATA_ERROR = "GET_USER_DATA_ERROR"

// Specifict Reset
export const RESET_USERS = "RESET_USERS"

// General Reset
export const RESET = "RESET"
