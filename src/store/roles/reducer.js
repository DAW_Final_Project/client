// ActionTypes
import {
  // Get Roles List
  GET_ROLES_LIST,
  GET_ROLES_LIST_SUCCESS,
  GET_ROLES_LIST_ERROR,
  // Get Role Data
  GET_ROLE_DATA,
  GET_ROLE_DATA_SUCCESS,
  GET_ROLE_DATA_ERROR,
  // Add Role
  ADD_ROLE,
  ADD_ROLE_SUCCESS,
  ADD_ROLE_ERROR,
  // Modify Role Data
  MODIFY_ROLE_DATA,
  MODIFY_ROLE_DATA_SUCCESS,
  MODIFY_ROLE_DATA_ERROR,
  // Delete Role
  DELETE_ROLE,
  DELETE_ROLE_SUCCESS,
  DELETE_ROLE_ERROR,
  // Specific Reset
  RESET_ROLES,
  // General Reset
  RESET,
} from "./actionTypes";

const initialState = {
  rolesError: "",
  rolesnSuccess: "",
  rolesLoading: false,
  rolesList: [],
  rolesListTotal: 0,
  roleData: {},
};

const roles = (state = initialState, action) => {
  switch (action.type) {
    // Get Roles List
    case GET_ROLES_LIST:
      state = {
        ...state,
        rolesLoading: true,
        rolesSuccess: "",
        rolesError: "",
      };
      break;
    case GET_ROLES_LIST_SUCCESS:
      state = {
        ...state,
        rolesList: action.payload.list,
        rolesListTotal: action.payload.total,
        rolesSuccess: "MsgOk[Roles List Received]",
        rolesError: "",
        rolesLoading: false,
      };
      break;
    case GET_ROLES_LIST_ERROR:
      state = {
        ...state,
        rolesList: [],
        rolesListTotal: 0,
        rolesError: action.payload,
        rolesSuccess: "",
        rolesLoading: false,
      };
      break;

    // Get Role Data
    case GET_ROLE_DATA:
      state = {
        ...state,
        rolesLoading: true,
        rolesSuccess: "",
        rolesError: "",
      };
      break;
    case GET_ROLE_DATA_SUCCESS:
      state = {
        ...state,
        roleData: action.payload,
        rolesSuccess: "MsgOk[Role Data Received]",
        rolesError: "",
        rolesLoading: false,
      };
      break;
    case GET_ROLE_DATA_ERROR:
      state = {
        ...state,
        roleData: {},
        rolesError: action.payload,
        rolesSuccess: "",
        rolesLoading: false,
      };
      break;

    // Add Role
    case ADD_ROLE:
      state = {
        ...state,
        rolesLoading: true,
        rolesSuccess: "",
        rolesError: "",
      };
      break;
    case ADD_ROLE_SUCCESS:
      state = {
        ...state,
        rolesSuccess: action.payload,
        rolesError: "",
        rolesLoading: false,
      };
      break;
    case ADD_ROLE_ERROR:
      state = {
        ...state,
        rolesError: action.payload,
        rolesSuccess: "",
        rolesLoading: false,
      };
      break;

    // Modify Role Data
    case MODIFY_ROLE_DATA:
      state = {
        ...state,
        rolesLoading: true,
        rolesSuccess: "",
        rolesError: "",
      };
      break;
    case MODIFY_ROLE_DATA_SUCCESS:
      state = {
        ...state,
        rolesSuccess: action.payload,
        rolesError: "",
        rolesLoading: false,
      };
      break;
    case MODIFY_ROLE_DATA_ERROR:
      state = {
        ...state,
        rolesError: action.payload,
        rolesSuccess: "",
        rolesLoading: false,
      };
      break;

    // Delete Role
    case DELETE_ROLE:
      state = {
        ...state,
        rolesLoading: true,
        rolesSuccess: "",
        rolesError: "",
      };
      break;
    case DELETE_ROLE_SUCCESS:
      state = {
        ...state,
        rolesSuccess: action.payload,
        rolesError: "",
        rolesLoading: false,
      };
      break;
    case DELETE_ROLE_ERROR:
      state = {
        ...state,
        rolesError: action.payload,
        rolesSuccess: "",
        rolesLoading: false,
      };
      break;

    // Reset especifico
    case RESET_ROLES:
      state = initialState;
      break;

    // Reset general
    case RESET:
      state = initialState;
      break;

    default:
      state = { ...state };
      break;
  }

  return state;
};

export default roles;
