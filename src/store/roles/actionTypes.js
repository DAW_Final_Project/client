// Get Roles List
export const GET_ROLES_LIST = "GET_ROLES_LIST"
export const GET_ROLES_LIST_SUCCESS = "GET_ROLES_LIST_SUCCESS"
export const GET_ROLES_LIST_ERROR = "GET_ROLES_LIST_ERROR"

// Get Role Data
export const GET_ROLE_DATA = "GET_ROLE_DATA"
export const GET_ROLE_DATA_SUCCESS = "GET_ROLE_DATA_SUCCESS"
export const GET_ROLE_DATA_ERROR = "GET_ROLE_DATA_ERROR"

// Add Role
export const ADD_ROLE = "ADD_ROLE"
export const ADD_ROLE_SUCCESS = "ADD_ROLE_SUCCESS"
export const ADD_ROLE_ERROR = "ADD_ROLE_ERROR"

// Modify Role Data
export const MODIFY_ROLE_DATA = "MODIFY_ROLE_DATA"
export const MODIFY_ROLE_DATA_SUCCESS = "MODIFY_ROLE_DATA_SUCCESS"
export const MODIFY_ROLE_DATA_ERROR = "MODIFY_ROLE_DATA_ERROR"

// Delete Role
export const DELETE_ROLE = "DELETE_ROLE"
export const DELETE_ROLE_SUCCESS = "DELETE_ROLE_SUCCESS"
export const DELETE_ROLE_ERROR = "DELETE_ROLE_ERROR"

// Specifict Reset
export const RESET_ROLES = "RESET_ROLES"

// General Reset
export const RESET = "RESET"
