import { takeEvery, put, call } from "redux-saga/effects";

// ActionTypes
import {
  // Get Roles List
  GET_ROLES_LIST,
  // Get Role Data
  GET_ROLE_DATA,
  // Add Role
  ADD_ROLE,
  // Modify Role Data
  MODIFY_ROLE_DATA,
  // Delete Role
  DELETE_ROLE,
} from "./actionTypes";

// Actions
import {
  // Get Roles List
  getRolesListSuccess,
  getRolesListError,
  // Get Role Data
  getRoleDataSuccess,
  getRoleDataError,
  // Add Role
  addRoleSuccess,
  addRoleError,
  // Modify Role Data
  modifyRoleDataSuccess,
  modifyRoleDataError,
  // Delete Role
  deleteRoleSuccess,
  deleteRoleError,
} from "./actions";

// Api functions
import * as api from "api/roles/roles.service";

/* --- FUNCTIONS --- */
// Get Roles List
function* getRolesList({ params: { sort, order, limit, skip, navigate } }) {
  try {
    const response = yield call(api.getRolesList, sort, order, limit, skip);
    if (response.status === 200) {
      yield put(getRolesListSuccess(response.data));
    } else {
      yield put(getRolesListError("MsgErr[Get Roles List Failed]"));
    }
  } catch (error) {
    yield put(getRolesListError(error));
  }
}

// Get Role Data
function* getRoleData({ params: { role_id, navigate } }) {
  try {
    const response = yield call(api.getRoleData, role_id);
    if (response.status === 200) {
      yield put(getRoleDataSuccess(response.data[0]));
    } else {
      yield put(getRoleDataError("MsgErr[Get Role Data Failed]"));
    }
  } catch (error) {
    yield put(getRoleDataError(error));
  }
}

// Add Role
function* addRole({ params: { data, navigate } }) {
  try {
    const response = yield call(api.addRole, data);
    if (response.status === 200) {
      yield put(addRoleSuccess("MsgOk[Role Added]"));
    } else {
      yield put(addRoleError("MsgErr[Get Role Data Failed]"));
    }
  } catch (error) {
    yield put(addRoleError(error));
  }
}
// Modify Role Data
function* modifyRoleData({ params: { role_id, data, navigate } }) {
  try {
    const response = yield call(api.modifyRoleData, role_id, data);
    if (response.status === 200) {
      yield put(modifyRoleDataSuccess("MsgOk[Role Data Modified]"));
    } else {
      yield put(modifyRoleDataError("MsgErr[Get Role Data Failed]"));
    }
  } catch (error) {
    yield put(modifyRoleDataError(error));
  }
}
// Delete Role
function* deleteRole({ params: { role_id, navigate } }) {
  try {
    const response = yield call(api.deleteRole, role_id);
    if (response.status === 200) {
      yield put(deleteRoleSuccess("MsgOk[Role Data Modified]"));
    } else {
      yield put(deleteRoleError("MsgErr[Get Role Data Failed]"));
    }
  } catch (error) {
    yield put(deleteRoleError(error));
  }
}

function* RolesSaga() {
  yield takeEvery(GET_ROLES_LIST, getRolesList);
  yield takeEvery(GET_ROLE_DATA, getRoleData);
  yield takeEvery(ADD_ROLE, addRole);
  yield takeEvery(MODIFY_ROLE_DATA, modifyRoleData);
  yield takeEvery(DELETE_ROLE, deleteRole);
}

export default RolesSaga;