// ActionTypes
import {
  // Get Roles List
  GET_ROLES_LIST,
  GET_ROLES_LIST_SUCCESS,
  GET_ROLES_LIST_ERROR,
  // Get Role Data
  GET_ROLE_DATA,
  GET_ROLE_DATA_SUCCESS,
  GET_ROLE_DATA_ERROR,
  // Add Role
  ADD_ROLE,
  ADD_ROLE_SUCCESS,
  ADD_ROLE_ERROR,
  // Modify Role Data
  MODIFY_ROLE_DATA,
  MODIFY_ROLE_DATA_SUCCESS,
  MODIFY_ROLE_DATA_ERROR,
  // Delete Role
  DELETE_ROLE,
  DELETE_ROLE_SUCCESS,
  DELETE_ROLE_ERROR,
} from "./actionTypes";

// Get Roles List
export const getRolesList = (sort, order, limit, skip, navigate) => ({
  type: GET_ROLES_LIST,
  params: { sort, order, limit, skip, navigate },
});

export const getRolesListSuccess = (obj) => ({
  type: GET_ROLES_LIST_SUCCESS,
  payload: obj,
});

export const getRolesListError = (error) => ({
  type: GET_ROLES_LIST_ERROR,
  payload: error,
});

// Get Role Data
export const getRoleData = (role_id, navigate) => ({
  type: GET_ROLE_DATA,
  params: { role_id, navigate },
});

export const getRoleDataSuccess = (obj) => ({
  type: GET_ROLE_DATA_SUCCESS,
  payload: obj,
});

export const getRoleDataError = (error) => ({
  type: GET_ROLE_DATA_ERROR,
  payload: error,
});

// Add Role
export const addRole = (data, navigate) => ({
  type: ADD_ROLE,
  params: { data, navigate },
});

export const addRoleSuccess = (obj) => ({
  type: ADD_ROLE_SUCCESS,
  payload: obj,
});

export const addRoleError = (error) => ({
  type: ADD_ROLE_ERROR,
  payload: error,
});

// Modify Role Data
export const modifyRoleData = (role_id, data, navigate) => ({
  type: MODIFY_ROLE_DATA,
  params: { role_id, data, navigate },
});

export const modifyRoleDataSuccess = (obj) => ({
  type: MODIFY_ROLE_DATA_SUCCESS,
  payload: obj,
});

export const modifyRoleDataError = (error) => ({
  type: MODIFY_ROLE_DATA_ERROR,
  payload: error,
});

// Delete Role
export const deleteRole = (role_id, navigate) => ({
  type: DELETE_ROLE,
  params: { role_id, navigate },
});

export const deleteRoleSuccess = (obj) => ({
  type: DELETE_ROLE_SUCCESS,
  payload: obj,
});

export const deleteRoleError = (error) => ({
  type: DELETE_ROLE_ERROR,
  payload: error,
});
