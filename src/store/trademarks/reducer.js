// ActionTypes
import {
  // Get Trademarks List
  GET_TRADEMARKS_LIST,
  GET_TRADEMARKS_LIST_SUCCESS,
  GET_TRADEMARKS_LIST_ERROR,
  // Get Trademark Data
  GET_TRADEMARK_DATA,
  GET_TRADEMARK_DATA_SUCCESS,
  GET_TRADEMARK_DATA_ERROR,
  // Add Trademark
  ADD_TRADEMARK,
  ADD_TRADEMARK_SUCCESS,
  ADD_TRADEMARK_ERROR,
  // Modify Trademark Data
  MODIFY_TRADEMARK_DATA,
  MODIFY_TRADEMARK_DATA_SUCCESS,
  MODIFY_TRADEMARK_DATA_ERROR,
  // Delete Trademark
  DELETE_TRADEMARK,
  DELETE_TRADEMARK_SUCCESS,
  DELETE_TRADEMARK_ERROR,
  // Specific Reset
  RESET_TRADEMARKS,
  // General Reset
  RESET,
} from "./actionTypes";

const initialState = {
  trademarksError: "",
  trademarksnSuccess: "",
  trademarksLoading: false,
  trademarksList: [],
  trademarksListTotal: 0,
  trademarkData: {},
};

const trademarks = (state = initialState, action) => {
  switch (action.type) {
    // Get Trademarks List
    case GET_TRADEMARKS_LIST:
      state = {
        ...state,
        trademarksLoading: true,
        trademarksSuccess: "",
        trademarksError: "",
      };
      break;
    case GET_TRADEMARKS_LIST_SUCCESS:
      state = {
        ...state,
        trademarksList: action.payload.list,
        trademarksListTotal: action.payload.total,
        trademarksSuccess: "MsgOk[Trademarks List Received]",
        trademarksError: "",
        trademarksLoading: false,
      };
      break;
    case GET_TRADEMARKS_LIST_ERROR:
      state = {
        ...state,
        trademarksList: [],
        trademarksListTotal: 0,
        trademarksError: action.payload,
        trademarksSuccess: "",
        trademarksLoading: false,
      };
      break;

    // Get Trademark Data
    case GET_TRADEMARK_DATA:
      state = {
        ...state,
        trademarksLoading: true,
        trademarksSuccess: "",
        trademarksError: "",
      };
      break;
    case GET_TRADEMARK_DATA_SUCCESS:
      state = {
        ...state,
        trademarkData: action.payload,
        trademarksSuccess: "MsgOk[Trademark Data Received]",
        trademarksError: "",
        trademarksLoading: false,
      };
      break;
    case GET_TRADEMARK_DATA_ERROR:
      state = {
        ...state,
        trademarkData: {},
        trademarksError: action.payload,
        trademarksSuccess: "",
        trademarksLoading: false,
      };
      break;

    // Add Trademark
    case ADD_TRADEMARK:
      state = {
        ...state,
        trademarksLoading: true,
        trademarksSuccess: "",
        trademarksError: "",
      };
      break;
    case ADD_TRADEMARK_SUCCESS:
      state = {
        ...state,
        trademarksSuccess: action.payload,
        trademarksError: "",
        trademarksLoading: false,
      };
      break;
    case ADD_TRADEMARK_ERROR:
      state = {
        ...state,
        trademarksError: action.payload,
        trademarksSuccess: "",
        trademarksLoading: false,
      };
      break;

    // Modify Trademark Data
    case MODIFY_TRADEMARK_DATA:
      state = {
        ...state,
        trademarksLoading: true,
        trademarksSuccess: "",
        trademarksError: "",
      };
      break;
    case MODIFY_TRADEMARK_DATA_SUCCESS:
      state = {
        ...state,
        trademarksSuccess: action.payload,
        trademarksError: "",
        trademarksLoading: false,
      };
      break;
    case MODIFY_TRADEMARK_DATA_ERROR:
      state = {
        ...state,
        trademarksError: action.payload,
        trademarksSuccess: "",
        trademarksLoading: false,
      };
      break;

    // Delete Trademark
    case DELETE_TRADEMARK:
      state = {
        ...state,
        trademarksLoading: true,
        trademarksSuccess: "",
        trademarksError: "",
      };
      break;
    case DELETE_TRADEMARK_SUCCESS:
      state = {
        ...state,
        trademarksSuccess: action.payload,
        trademarksError: "",
        trademarksLoading: false,
      };
      break;
    case DELETE_TRADEMARK_ERROR:
      state = {
        ...state,
        trademarksError: action.payload,
        trademarksSuccess: "",
        trademarksLoading: false,
      };
      break;

    // Reset especifico
    case RESET_TRADEMARKS:
      state = initialState;
      break;

    // Reset general
    case RESET:
      state = initialState;
      break;

    default:
      state = { ...state };
      break;
  }

  return state;
};

export default trademarks;
