import { takeEvery, put, call } from "redux-saga/effects";

// ActionTypes
import {
  // Get Trademarks List
  GET_TRADEMARKS_LIST,
  // Get Trademark Data
  GET_TRADEMARK_DATA,
  // Add Trademark
  ADD_TRADEMARK,
  // Modify Trademark Data
  MODIFY_TRADEMARK_DATA,
  // Delete Trademark
  DELETE_TRADEMARK,
} from "./actionTypes";

// Actions
import {
  // Get Trademarks List
  getTrademarksListSuccess,
  getTrademarksListError,
  // Get Trademark Data
  getTrademarkDataSuccess,
  getTrademarkDataError,
  // Add Trademark
  addTrademarkSuccess,
  addTrademarkError,
  // Modify Trademark Data
  modifyTrademarkDataSuccess,
  modifyTrademarkDataError,
  // Delete Trademark
  deleteTrademarkSuccess,
  deleteTrademarkError,
} from "./actions";

// Api functions
import * as api from "api/productstrademarks/productstrademarks.service";

/* --- FUNCTIONS --- */
// Get Trademarks List
function* getTrademarksList({ params: { sort, order, limit, skip, navigate } }) {
  try {
    const response = yield call(api.getTrademarksList, sort, order, limit, skip);
    if (response.status === 200) {
      yield put(getTrademarksListSuccess(response.data));
    } else {
      yield put(getTrademarksListError("MsgErr[Get Trademarks List Failed]"));
    }
  } catch (error) {
    yield put(getTrademarksListError(error));
  }
}

// Get Trademark Data
function* getTrademarkData({ params: { trademark_id, navigate } }) {
  try {
    const response = yield call(api.getTrademarkData, trademark_id);
    if (response.status === 200) {
      yield put(getTrademarkDataSuccess(response.data));
    } else {
      yield put(getTrademarkDataError("MsgErr[Get Trademark Data Failed]"));
    }
  } catch (error) {
    yield put(getTrademarkDataError(error));
  }
}

// Add Trademark
function* addTrademark({ params: { data, navigate } }) {
  try {
    const response = yield call(api.addTrademark, data);
    if (response.status === 200) {
      yield put(addTrademarkSuccess("MsgOk[Trademark Added]"));
    } else {
      yield put(addTrademarkError("MsgErr[Get Trademark Data Failed]"));
    }
  } catch (error) {
    yield put(addTrademarkError(error));
  }
}
// Modify Trademark Data
function* modifyTrademarkData({ params: { trademark_id, data, navigate } }) {
  try {
    const response = yield call(api.modifyTrademarkData, trademark_id, data);
    if (response.status === 200) {
      yield put(modifyTrademarkDataSuccess("MsgOk[Trademark Data Modified]"));
    } else {
      yield put(modifyTrademarkDataError("MsgErr[Get Trademark Data Failed]"));
    }
  } catch (error) {
    yield put(modifyTrademarkDataError(error));
  }
}
// Delete Trademark
function* deleteTrademark({ params: { trademark_id, navigate } }) {
  try {
    const response = yield call(api.deleteTrademark, trademark_id);
    if (response.status === 200) {
      yield put(deleteTrademarkSuccess("MsgOk[Trademark Data Modified]"));
    } else {
      yield put(deleteTrademarkError("MsgErr[Get Trademark Data Failed]"));
    }
  } catch (error) {
    yield put(deleteTrademarkError(error));
  }
}

function* TrademarksSaga() {
  yield takeEvery(GET_TRADEMARKS_LIST, getTrademarksList);
  yield takeEvery(GET_TRADEMARK_DATA, getTrademarkData);
  yield takeEvery(ADD_TRADEMARK, addTrademark);
  yield takeEvery(MODIFY_TRADEMARK_DATA, modifyTrademarkData);
  yield takeEvery(DELETE_TRADEMARK, deleteTrademark);
}

export default TrademarksSaga;