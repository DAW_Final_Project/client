// ActionTypes
import {
  // Get Trademarks List
  GET_TRADEMARKS_LIST,
  GET_TRADEMARKS_LIST_SUCCESS,
  GET_TRADEMARKS_LIST_ERROR,
  // Get Trademark Data
  GET_TRADEMARK_DATA,
  GET_TRADEMARK_DATA_SUCCESS,
  GET_TRADEMARK_DATA_ERROR,
  // Add Trademark
  ADD_TRADEMARK,
  ADD_TRADEMARK_SUCCESS,
  ADD_TRADEMARK_ERROR,
  // Modify Trademark Data
  MODIFY_TRADEMARK_DATA,
  MODIFY_TRADEMARK_DATA_SUCCESS,
  MODIFY_TRADEMARK_DATA_ERROR,
  // Delete Trademark
  DELETE_TRADEMARK,
  DELETE_TRADEMARK_SUCCESS,
  DELETE_TRADEMARK_ERROR,
} from "./actionTypes";

// Get Trademarks List
export const getTrademarksList = (sort, order, limit, skip, navigate) => ({
  type: GET_TRADEMARKS_LIST,
  params: { sort, order, limit, skip, navigate },
});

export const getTrademarksListSuccess = (obj) => ({
  type: GET_TRADEMARKS_LIST_SUCCESS,
  payload: obj,
});

export const getTrademarksListError = (error) => ({
  type: GET_TRADEMARKS_LIST_ERROR,
  payload: error,
});

// Get Trademark Data
export const getTrademarkData = (trademark_id, navigate) => ({
  type: GET_TRADEMARK_DATA,
  params: { trademark_id, navigate },
});

export const getTrademarkDataSuccess = (obj) => ({
  type: GET_TRADEMARK_DATA_SUCCESS,
  payload: obj,
});

export const getTrademarkDataError = (error) => ({
  type: GET_TRADEMARK_DATA_ERROR,
  payload: error,
});

// Add Trademark
export const addTrademark = (data, navigate) => ({
  type: ADD_TRADEMARK,
  params: { data, navigate },
});

export const addTrademarkSuccess = (obj) => ({
  type: ADD_TRADEMARK_SUCCESS,
  payload: obj,
});

export const addTrademarkError = (error) => ({
  type: ADD_TRADEMARK_ERROR,
  payload: error,
});

// Modify Trademark Data
export const modifyTrademarkData = (trademark_id, data, navigate) => ({
  type: MODIFY_TRADEMARK_DATA,
  params: { trademark_id, data, navigate },
});

export const modifyTrademarkDataSuccess = (obj) => ({
  type: MODIFY_TRADEMARK_DATA_SUCCESS,
  payload: obj,
});

export const modifyTrademarkDataError = (error) => ({
  type: MODIFY_TRADEMARK_DATA_ERROR,
  payload: error,
});

// Delete Trademark
export const deleteTrademark = (trademark_id, navigate) => ({
  type: DELETE_TRADEMARK,
  params: { trademark_id, navigate },
});

export const deleteTrademarkSuccess = (obj) => ({
  type: DELETE_TRADEMARK_SUCCESS,
  payload: obj,
});

export const deleteTrademarkError = (error) => ({
  type: DELETE_TRADEMARK_ERROR,
  payload: error,
});
