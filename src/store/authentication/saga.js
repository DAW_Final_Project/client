import { takeEvery, put, call } from "redux-saga/effects";

// ActionTypes
import {
  // Login
  LOGIN,
} from "./actionTypes";

// Actions
import {
  // Login
  loginSuccess,
  loginError,
} from "./actions";

// Api functions
import * as api from "api/authentication/authetication.service";

//Authentication
import * as authentication from "../../helpers/localstorage/authentication";

/* --- FUNCTIONS --- */
// Login
function* login({ params: { data, navigate } }) {
  try {
    const response = yield call(api.login, data);
    if (response.status === 200) {
      let user = {
        user: response.data.user,
        id: response.data.id,
        role: response.data.role,
        store: data.store
      };
      let token = response.data.token;

      authentication.assignLoggedInUser(JSON.stringify(user), token);
      switch (response.data.role) {
        case 1:
          navigate("/admindashboard");
          break;
        case 2:
          navigate("/clients");
          break;
        case 3:
          navigate("/techdashboard");
          break;
      }
      yield put(loginSuccess("MsgOk[Access Granted]"));
    } else {
      yield put(loginError("MsgErr[Access Denied]"));
    }
  } catch (error) {
    yield put(loginError(error));
  }
}

function* AutheticationSaga() {
  yield takeEvery(LOGIN, login);
}

export default AutheticationSaga;
