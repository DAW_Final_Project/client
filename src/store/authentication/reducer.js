// ActionTypes
import {
  // Login
  LOGIN,
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  // Reset especifico
  RESET_AUTHENTICATION,
  // Reset general
  RESET,
} from "./actionTypes";

const initialState = {
  authenticationError: "",
  authenticationnSuccess: "",
  authenticationLoading: false,
};

const authentication = (state = initialState, action) => {
  switch (action.type) {
    // Login
    case LOGIN:
      state = {
        ...state,
        authenticationLoading: true,
        authenticationSuccess: "",
        authenticationError: "",
      };
      break;
    case LOGIN_SUCCESS:
      state = {
        ...state,
        authenticationSuccess: action.payload,
        authenticationError: "",
        authenticationLoading: false,
      };
      break;
    case LOGIN_ERROR:
      state = {
        ...state,
        authenticationError: action.payload,
        authenticationSuccess: "",
        authenticationLoading: false,
      };
      break;

    // Reset especifico
    case RESET_AUTHENTICATION:
      state = initialState;
      break;

    // Reset general
    case RESET:
      state = initialState;
      break;

    default:
      state = { ...state };
      break;
  }

  return state;
};

export default authentication;
