// ActionTypes
import {
  // Login
  LOGIN,
  LOGIN_SUCCESS,
  LOGIN_ERROR,
} from "./actionTypes";

// Login
export const login = (data, navigate) => ({
  type: LOGIN,
  params: { data, navigate },
});

export const loginSuccess = (obj) => ({
  type: LOGIN_SUCCESS,
  payload: obj,
});

export const loginError = (error) => ({
  type: LOGIN_ERROR,
  payload: error,
});