// Login
export const LOGIN = "LOGIN"
export const LOGIN_SUCCESS = "LOGIN_SUCCESS"
export const LOGIN_ERROR = "LOGIN_ERROR"

// Reset especifico
export const RESET_AUTHENTICATION = "RESET_AUTHENTICATION"

// Reset general
export const RESET = "RESET"
