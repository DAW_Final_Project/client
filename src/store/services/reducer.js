// ActionTypes
import {
  // Get Services List
  GET_SERVICES_LIST,
  GET_SERVICES_LIST_SUCCESS,
  GET_SERVICES_LIST_ERROR,
  // Get Service Data
  GET_SERVICE_DATA,
  GET_SERVICE_DATA_SUCCESS,
  GET_SERVICE_DATA_ERROR,
  // Add Service
  ADD_SERVICE,
  ADD_SERVICE_SUCCESS,
  ADD_SERVICE_ERROR,
  // Modify Service Data
  MODIFY_SERVICE_DATA,
  MODIFY_SERVICE_DATA_SUCCESS,
  MODIFY_SERVICE_DATA_ERROR,
  // Delete Service
  DELETE_SERVICE,
  DELETE_SERVICE_SUCCESS,
  DELETE_SERVICE_ERROR,
  // Specific Reset
  RESET_SERVICES,
  // General Reset
  RESET,
} from "./actionTypes";

const initialState = {
  servicesError: "",
  servicesnSuccess: "",
  servicesLoading: false,
  servicesList: [],
  servicesListTotal: 0,
  serviceData: {},
};

const services = (state = initialState, action) => {
  switch (action.type) {
    // Get Services List
    case GET_SERVICES_LIST:
      state = {
        ...state,
        servicesLoading: true,
        servicesSuccess: "",
        servicesError: "",
      };
      break;
    case GET_SERVICES_LIST_SUCCESS:
      state = {
        ...state,
        servicesList: action.payload.list,
        servicesListTotal: action.payload.total,
        servicesSuccess: "MsgOk[Services List Received]",
        servicesError: "",
        servicesLoading: false,
      };
      break;
    case GET_SERVICES_LIST_ERROR:
      state = {
        ...state,
        servicesList: [],
        servicesListTotal: 0,
        servicesError: action.payload,
        servicesSuccess: "",
        servicesLoading: false,
      };
      break;

    // Get Service Data
    case GET_SERVICE_DATA:
      state = {
        ...state,
        servicesLoading: true,
        servicesSuccess: "",
        servicesError: "",
      };
      break;
    case GET_SERVICE_DATA_SUCCESS:
      state = {
        ...state,
        serviceData: action.payload,
        servicesSuccess: "MsgOk[Service Data Received]",
        servicesError: "",
        servicesLoading: false,
      };
      break;
    case GET_SERVICE_DATA_ERROR:
      state = {
        ...state,
        serviceData: {},
        servicesError: action.payload,
        servicesSuccess: "",
        servicesLoading: false,
      };
      break;

    // Add Service
    case ADD_SERVICE:
      state = {
        ...state,
        servicesLoading: true,
        servicesSuccess: "",
        servicesError: "",
      };
      break;
    case ADD_SERVICE_SUCCESS:
      state = {
        ...state,
        servicesSuccess: action.payload,
        servicesError: "",
        servicesLoading: false,
      };
      break;
    case ADD_SERVICE_ERROR:
      state = {
        ...state,
        servicesError: action.payload,
        servicesSuccess: "",
        servicesLoading: false,
      };
      break;

    // Modify Service Data
    case MODIFY_SERVICE_DATA:
      state = {
        ...state,
        servicesLoading: true,
        servicesSuccess: "",
        servicesError: "",
      };
      break;
    case MODIFY_SERVICE_DATA_SUCCESS:
      state = {
        ...state,
        servicesSuccess: action.payload,
        servicesError: "",
        servicesLoading: false,
      };
      break;
    case MODIFY_SERVICE_DATA_ERROR:
      state = {
        ...state,
        servicesError: action.payload,
        servicesSuccess: "",
        servicesLoading: false,
      };
      break;

    // Delete Service
    case DELETE_SERVICE:
      state = {
        ...state,
        servicesLoading: true,
        servicesSuccess: "",
        servicesError: "",
      };
      break;
    case DELETE_SERVICE_SUCCESS:
      state = {
        ...state,
        servicesSuccess: action.payload,
        servicesError: "",
        servicesLoading: false,
      };
      break;
    case DELETE_SERVICE_ERROR:
      state = {
        ...state,
        servicesError: action.payload,
        servicesSuccess: "",
        servicesLoading: false,
      };
      break;

    // Reset especifico
    case RESET_SERVICES:
      state = initialState;
      break;

    // Reset general
    case RESET:
      state = initialState;
      break;

    default:
      state = { ...state };
      break;
  }

  return state;
};

export default services;
