import { takeEvery, put, call } from "redux-saga/effects";

// ActionTypes
import {
  // Get Services List
  GET_SERVICES_LIST,
  // Get Service Data
  GET_SERVICE_DATA,
  // Add Service
  ADD_SERVICE,
  // Modify Service Data
  MODIFY_SERVICE_DATA,
  // Delete Service
  DELETE_SERVICE,
} from "./actionTypes";

// Actions
import {
  // Get Services List
  getServicesListSuccess,
  getServicesListError,
  // Get Service Data
  getServiceDataSuccess,
  getServiceDataError,
  // Add Service
  addServiceSuccess,
  addServiceError,
  // Modify Service Data
  modifyServiceDataSuccess,
  modifyServiceDataError,
  // Delete Service
  deleteServiceSuccess,
  deleteServiceError,
} from "./actions";

// Api functions
import * as api from "api/services/services.service";

/* --- FUNCTIONS --- */
// Get Services List
function* getServicesList({ params: { client_id, sort, order, limit, skip, navigate } }) {
  try {
    const response = yield call(api.getServicesList, client_id, sort, order, limit, skip);
    if (response.status === 200) {
      yield put(getServicesListSuccess(response.data));
    } else {
      yield put(getServicesListError("MsgErr[Get Services List Failed]"));
    }
  } catch (error) {
    yield put(getServicesListError(error));
  }
}

// Get Service Data
function* getServiceData({ params: { service_id, navigate } }) {
  try {
    const response = yield call(api.getServiceData, service_id);
    if (response.status === 200) {
      yield put(getServiceDataSuccess(response.data));
    } else {
      yield put(getServiceDataError("MsgErr[Get Service Data Failed]"));
    }
  } catch (error) {
    yield put(getServiceDataError(error));
  }
}

// Add Service
function* addService({ params: { data, navigate } }) {
  try {
    const response = yield call(api.addService, data);
    if (response.status === 200) {
      yield put(addServiceSuccess("MsgOk[Service Added]"));
    } else {
      yield put(addServiceError("MsgErr[Get Service Data Failed]"));
    }
  } catch (error) {
    yield put(addServiceError(error));
  }
}
// Modify Service Data
function* modifyServiceData({ params: { service_id, data, navigate } }) {
  try {
    const response = yield call(api.modifyServiceData, service_id, data);
    if (response.status === 200) {
      yield put(modifyServiceDataSuccess("MsgOk[Service Data Modified]"));
    } else {
      yield put(modifyServiceDataError("MsgErr[Get Service Data Failed]"));
    }
  } catch (error) {
    yield put(modifyServiceDataError(error));
  }
}
// Delete Service
function* deleteService({ params: { service_id, navigate } }) {
  try {
    const response = yield call(api.deleteService, service_id);
    if (response.status === 200) {
      yield put(deleteServiceSuccess("MsgOk[Service Data Modified]"));
    } else {
      yield put(deleteServiceError("MsgErr[Get Service Data Failed]"));
    }
  } catch (error) {
    yield put(deleteServiceError(error));
  }
}

function* ServicesSaga() {
  yield takeEvery(GET_SERVICES_LIST, getServicesList);
  yield takeEvery(GET_SERVICE_DATA, getServiceData);
  yield takeEvery(ADD_SERVICE, addService);
  yield takeEvery(MODIFY_SERVICE_DATA, modifyServiceData);
  yield takeEvery(DELETE_SERVICE, deleteService);
}

export default ServicesSaga;