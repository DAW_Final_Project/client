// ActionTypes
import {
  // Get Services List
  GET_SERVICES_LIST,
  GET_SERVICES_LIST_SUCCESS,
  GET_SERVICES_LIST_ERROR,
  // Get Service Data
  GET_SERVICE_DATA,
  GET_SERVICE_DATA_SUCCESS,
  GET_SERVICE_DATA_ERROR,
  // Add Service
  ADD_SERVICE,
  ADD_SERVICE_SUCCESS,
  ADD_SERVICE_ERROR,
  // Modify Service Data
  MODIFY_SERVICE_DATA,
  MODIFY_SERVICE_DATA_SUCCESS,
  MODIFY_SERVICE_DATA_ERROR,
  // Delete Service
  DELETE_SERVICE,
  DELETE_SERVICE_SUCCESS,
  DELETE_SERVICE_ERROR,
} from "./actionTypes";

// Get Services List
export const getServicesList = (client_id, sort, order, limit, skip, navigate) => ({
  type: GET_SERVICES_LIST,
  params: { client_id, sort, order, limit, skip, navigate },
});

export const getServicesListSuccess = (obj) => ({
  type: GET_SERVICES_LIST_SUCCESS,
  payload: obj,
});

export const getServicesListError = (error) => ({
  type: GET_SERVICES_LIST_ERROR,
  payload: error,
});

// Get Service Data
export const getServiceData = (service_id, navigate) => ({
  type: GET_SERVICE_DATA,
  params: { service_id, navigate },
});

export const getServiceDataSuccess = (obj) => ({
  type: GET_SERVICE_DATA_SUCCESS,
  payload: obj,
});

export const getServiceDataError = (error) => ({
  type: GET_SERVICE_DATA_ERROR,
  payload: error,
});

// Add Service
export const addService = (data, navigate) => ({
  type: ADD_SERVICE,
  params: { data, navigate },
});

export const addServiceSuccess = (obj) => ({
  type: ADD_SERVICE_SUCCESS,
  payload: obj,
});

export const addServiceError = (error) => ({
  type: ADD_SERVICE_ERROR,
  payload: error,
});

// Modify Service Data
export const modifyServiceData = (service_id, data, navigate) => ({
  type: MODIFY_SERVICE_DATA,
  params: { service_id, data, navigate },
});

export const modifyServiceDataSuccess = (obj) => ({
  type: MODIFY_SERVICE_DATA_SUCCESS,
  payload: obj,
});

export const modifyServiceDataError = (error) => ({
  type: MODIFY_SERVICE_DATA_ERROR,
  payload: error,
});

// Delete Service
export const deleteService = (service_id, navigate) => ({
  type: DELETE_SERVICE,
  params: { service_id, navigate },
});

export const deleteServiceSuccess = (obj) => ({
  type: DELETE_SERVICE_SUCCESS,
  payload: obj,
});

export const deleteServiceError = (error) => ({
  type: DELETE_SERVICE_ERROR,
  payload: error,
});
