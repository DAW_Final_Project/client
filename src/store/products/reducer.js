// ActionTypes
import {
  // Get Products List
  GET_PRODUCTS_LIST,
  GET_PRODUCTS_LIST_SUCCESS,
  GET_PRODUCTS_LIST_ERROR,
  // Get Product Data
  GET_PRODUCT_DATA,
  GET_PRODUCT_DATA_SUCCESS,
  GET_PRODUCT_DATA_ERROR,
  // Add Product
  ADD_PRODUCT,
  ADD_PRODUCT_SUCCESS,
  ADD_PRODUCT_ERROR,
  // Modify Product Data
  MODIFY_PRODUCT_DATA,
  MODIFY_PRODUCT_DATA_SUCCESS,
  MODIFY_PRODUCT_DATA_ERROR,
  // Delete Product
  DELETE_PRODUCT,
  DELETE_PRODUCT_SUCCESS,
  DELETE_PRODUCT_ERROR,
  // Specific Reset
  RESET_PRODUCTS,
  // General Reset
  RESET,
} from "./actionTypes";

const initialState = {
  productsError: "",
  productsnSuccess: "",
  productsLoading: false,
  productsList: [],
  productsListTotal: 0,
  productData: {},
};

const products = (state = initialState, action) => {
  switch (action.type) {
    // Get Products List
    case GET_PRODUCTS_LIST:
      state = {
        ...state,
        productsLoading: true,
        productsSuccess: "",
        productsError: "",
      };
      break;
    case GET_PRODUCTS_LIST_SUCCESS:
      state = {
        ...state,
        productsList: action.payload.list,
        productsListTotal: action.payload.total,
        productsSuccess: "MsgOk[Products List Received]",
        productsError: "",
        productsLoading: false,
      };
      break;
    case GET_PRODUCTS_LIST_ERROR:
      state = {
        ...state,
        productsList: [],
        productsListTotal: 0,
        productsError: action.payload,
        productsSuccess: "",
        productsLoading: false,
      };
      break;

    // Get Product Data
    case GET_PRODUCT_DATA:
      state = {
        ...state,
        productsLoading: true,
        productsSuccess: "",
        productsError: "",
      };
      break;
    case GET_PRODUCT_DATA_SUCCESS:
      state = {
        ...state,
        productData: action.payload,
        productsSuccess: "MsgOk[Product Data Received]",
        productsError: "",
        productsLoading: false,
      };
      break;
    case GET_PRODUCT_DATA_ERROR:
      state = {
        ...state,
        productData: {},
        productsError: action.payload,
        productsSuccess: "",
        productsLoading: false,
      };
      break;

    // Add Product
    case ADD_PRODUCT:
      state = {
        ...state,
        productsLoading: true,
        productsSuccess: "",
        productsError: "",
      };
      break;
    case ADD_PRODUCT_SUCCESS:
      state = {
        ...state,
        productsSuccess: action.payload,
        productsError: "",
        productsLoading: false,
      };
      break;
    case ADD_PRODUCT_ERROR:
      state = {
        ...state,
        productsError: action.payload,
        productsSuccess: "",
        productsLoading: false,
      };
      break;

    // Modify Product Data
    case MODIFY_PRODUCT_DATA:
      state = {
        ...state,
        productsLoading: true,
        productsSuccess: "",
        productsError: "",
      };
      break;
    case MODIFY_PRODUCT_DATA_SUCCESS:
      state = {
        ...state,
        productsSuccess: action.payload,
        productsError: "",
        productsLoading: false,
      };
      break;
    case MODIFY_PRODUCT_DATA_ERROR:
      state = {
        ...state,
        productsError: action.payload,
        productsSuccess: "",
        productsLoading: false,
      };
      break;

    // Delete Product
    case DELETE_PRODUCT:
      state = {
        ...state,
        productsLoading: true,
        productsSuccess: "",
        productsError: "",
      };
      break;
    case DELETE_PRODUCT_SUCCESS:
      state = {
        ...state,
        productsSuccess: action.payload,
        productsError: "",
        productsLoading: false,
      };
      break;
    case DELETE_PRODUCT_ERROR:
      state = {
        ...state,
        productsError: action.payload,
        productsSuccess: "",
        productsLoading: false,
      };
      break;

    // Reset especifico
    case RESET_PRODUCTS:
      state = initialState;
      break;

    // Reset general
    case RESET:
      state = initialState;
      break;

    default:
      state = { ...state };
      break;
  }

  return state;
};

export default products;
