import { takeEvery, put, call } from "redux-saga/effects";

// ActionTypes
import {
  // Get Products List
  GET_PRODUCTS_LIST,
  // Get Product Data
  GET_PRODUCT_DATA,
  // Add Product
  ADD_PRODUCT,
  // Modify Product Data
  MODIFY_PRODUCT_DATA,
  // Delete Product
  DELETE_PRODUCT,
} from "./actionTypes";

// Actions
import {
  // Get Products List
  getProductsListSuccess,
  getProductsListError,
  // Get Product Data
  getProductDataSuccess,
  getProductDataError,
  // Add Product
  addProductSuccess,
  addProductError,
  // Modify Product Data
  modifyProductDataSuccess,
  modifyProductDataError,
  // Delete Product
  deleteProductSuccess,
  deleteProductError,
} from "./actions";

// Api functions
import * as api from "api/products/products.service";

/* --- FUNCTIONS --- */
// Get Products List
function* getProductsList({ params: { category_id, trademark_id, sort, order, limit, skip, navigate } }) {
  try {
    const response = yield call(api.getProductsList, category_id, trademark_id, sort, order, limit, skip);
    if (response.status === 200) {
      yield put(getProductsListSuccess(response.data));
    } else {
      yield put(getProductsListError("MsgErr[Get Products List Failed]"));
    }
  } catch (error) {
    yield put(getProductsListError(error));
  }
}

// Get Product Data
function* getProductData({ params: { product_id, navigate } }) {
  try {
    const response = yield call(api.getProductData, product_id);
    if (response.status === 200) {
      yield put(getProductDataSuccess(response.data));
    } else {
      yield put(getProductDataError("MsgErr[Get Product Data Failed]"));
    }
  } catch (error) {
    yield put(getProductDataError(error));
  }
}

// Add Product
function* addProduct({ params: { data, navigate } }) {
  try {
    const response = yield call(api.addProduct, data);
    if (response.status === 200) {
      yield put(addProductSuccess("MsgOk[Product Added]"));
    } else {
      yield put(addProductError("MsgErr[Get Product Data Failed]"));
    }
  } catch (error) {
    yield put(addProductError(error));
  }
}
// Modify Product Data
function* modifyProductData({ params: { product_id, data, navigate } }) {
  try {
    const response = yield call(api.modifyProductData, product_id, data);
    if (response.status === 200) {
      yield put(modifyProductDataSuccess("MsgOk[Product Data Modified]"));
    } else {
      yield put(modifyProductDataError("MsgErr[Get Product Data Failed]"));
    }
  } catch (error) {
    yield put(modifyProductDataError(error));
  }
}
// Delete Product
function* deleteProduct({ params: { product_id, navigate } }) {
  try {
    const response = yield call(api.deleteProduct, product_id);
    if (response.status === 200) {
      yield put(deleteProductSuccess("MsgOk[Product Data Modified]"));
    } else {
      yield put(deleteProductError("MsgErr[Get Product Data Failed]"));
    }
  } catch (error) {
    yield put(deleteProductError(error));
  }
}

function* ProductsSaga() {
  yield takeEvery(GET_PRODUCTS_LIST, getProductsList);
  yield takeEvery(GET_PRODUCT_DATA, getProductData);
  yield takeEvery(ADD_PRODUCT, addProduct);
  yield takeEvery(MODIFY_PRODUCT_DATA, modifyProductData);
  yield takeEvery(DELETE_PRODUCT, deleteProduct);
}

export default ProductsSaga;