// ActionTypes
import {
  // Get Products List
  GET_PRODUCTS_LIST,
  GET_PRODUCTS_LIST_SUCCESS,
  GET_PRODUCTS_LIST_ERROR,
  // Get Product Data
  GET_PRODUCT_DATA,
  GET_PRODUCT_DATA_SUCCESS,
  GET_PRODUCT_DATA_ERROR,
  // Add Product
  ADD_PRODUCT,
  ADD_PRODUCT_SUCCESS,
  ADD_PRODUCT_ERROR,
  // Modify Product Data
  MODIFY_PRODUCT_DATA,
  MODIFY_PRODUCT_DATA_SUCCESS,
  MODIFY_PRODUCT_DATA_ERROR,
  // Delete Product
  DELETE_PRODUCT,
  DELETE_PRODUCT_SUCCESS,
  DELETE_PRODUCT_ERROR,
} from "./actionTypes";

// Get Products List
export const getProductsList = (category_id, trademark_id, sort, order, limit, skip, navigate) => ({
  type: GET_PRODUCTS_LIST,
  params: { category_id, trademark_id, sort, order, limit, skip, navigate },
});

export const getProductsListSuccess = (obj) => ({
  type: GET_PRODUCTS_LIST_SUCCESS,
  payload: obj,
});

export const getProductsListError = (error) => ({
  type: GET_PRODUCTS_LIST_ERROR,
  payload: error,
});

// Get Product Data
export const getProductData = (product_id, navigate) => ({
  type: GET_PRODUCT_DATA,
  params: { product_id, navigate },
});

export const getProductDataSuccess = (obj) => ({
  type: GET_PRODUCT_DATA_SUCCESS,
  payload: obj,
});

export const getProductDataError = (error) => ({
  type: GET_PRODUCT_DATA_ERROR,
  payload: error,
});

// Add Product
export const addProduct = (data, navigate) => ({
  type: ADD_PRODUCT,
  params: { data, navigate },
});

export const addProductSuccess = (obj) => ({
  type: ADD_PRODUCT_SUCCESS,
  payload: obj,
});

export const addProductError = (error) => ({
  type: ADD_PRODUCT_ERROR,
  payload: error,
});

// Modify Product Data
export const modifyProductData = (product_id, data, navigate) => ({
  type: MODIFY_PRODUCT_DATA,
  params: { product_id, data, navigate },
});

export const modifyProductDataSuccess = (obj) => ({
  type: MODIFY_PRODUCT_DATA_SUCCESS,
  payload: obj,
});

export const modifyProductDataError = (error) => ({
  type: MODIFY_PRODUCT_DATA_ERROR,
  payload: error,
});

// Delete Product
export const deleteProduct = (product_id, navigate) => ({
  type: DELETE_PRODUCT,
  params: { product_id, navigate },
});

export const deleteProductSuccess = (obj) => ({
  type: DELETE_PRODUCT_SUCCESS,
  payload: obj,
});

export const deleteProductError = (error) => ({
  type: DELETE_PRODUCT_ERROR,
  payload: error,
});
