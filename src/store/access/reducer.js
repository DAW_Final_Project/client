// ActionTypes
import {
  // Get Access List
  GET_ACCESS_LIST,
  GET_ACCESS_LIST_SUCCESS,
  GET_ACCESS_LIST_ERROR,
  // Get Access Data
  GET_ACCESS_DATA,
  GET_ACCESS_DATA_SUCCESS,
  GET_ACCESS_DATA_ERROR,
  // Add Access
  ADD_ACCESS,
  ADD_ACCESS_SUCCESS,
  ADD_ACCESS_ERROR,
  // Modify Access Data
  MODIFY_ACCESS_DATA,
  MODIFY_ACCESS_DATA_SUCCESS,
  MODIFY_ACCESS_DATA_ERROR,
  // Delete Access
  DELETE_ACCESS,
  DELETE_ACCESS_SUCCESS,
  DELETE_ACCESS_ERROR,
  // Specific Reset
  RESET_ACCESS,
  // General Reset
  RESET,
} from "./actionTypes";

const initialState = {
  accessError: "",
  accessnSuccess: "",
  accessLoading: false,
  accessList: [],
  accessListTotal: 0,
  accessData: {},
};

const access = (state = initialState, action) => {
  switch (action.type) {
    // Get Access List
    case GET_ACCESS_LIST:
      state = {
        ...state,
        accessLoading: true,
        accessSuccess: "",
        accessError: "",
      };
      break;
    case GET_ACCESS_LIST_SUCCESS:
      state = {
        ...state,
        accessList: action.payload.list,
        accessListTotal: action.payload.total,
        accessSuccess: "MsgOk[Access List Received]",
        accessError: "",
        accessLoading: false,
      };
      break;
    case GET_ACCESS_LIST_ERROR:
      state = {
        ...state,
        accessList: [],
        accessListTotal: 0,
        accessError: action.payload,
        accessSuccess: "",
        accessLoading: false,
      };
      break;

    // Get Access Data
    case GET_ACCESS_DATA:
      state = {
        ...state,
        accessLoading: true,
        accessSuccess: "",
        accessError: "",
      };
      break;
    case GET_ACCESS_DATA_SUCCESS:
      state = {
        ...state,
        accessData: action.payload,
        accessSuccess: "MsgOk[Access Data Received]",
        accessError: "",
        accessLoading: false,
      };
      break;
    case GET_ACCESS_DATA_ERROR:
      state = {
        ...state,
        accessData: {},
        accessError: action.payload,
        accessSuccess: "",
        accessLoading: false,
      };
      break;

    // Add Access
    case ADD_ACCESS:
      state = {
        ...state,
        accessLoading: true,
        accessSuccess: "",
        accessError: "",
      };
      break;
    case ADD_ACCESS_SUCCESS:
      state = {
        ...state,
        accessSuccess: action.payload,
        accessError: "",
        accessLoading: false,
      };
      break;
    case ADD_ACCESS_ERROR:
      state = {
        ...state,
        accessError: action.payload,
        accessSuccess: "",
        accessLoading: false,
      };
      break;

    // Modify Access Data
    case MODIFY_ACCESS_DATA:
      state = {
        ...state,
        accessLoading: true,
        accessSuccess: "",
        accessError: "",
      };
      break;
    case MODIFY_ACCESS_DATA_SUCCESS:
      state = {
        ...state,
        accessSuccess: action.payload,
        accessError: "",
        accessLoading: false,
      };
      break;
    case MODIFY_ACCESS_DATA_ERROR:
      state = {
        ...state,
        accessError: action.payload,
        accessSuccess: "",
        accessLoading: false,
      };
      break;

    // Delete Access
    case DELETE_ACCESS:
      state = {
        ...state,
        accessLoading: true,
        accessSuccess: "",
        accessError: "",
      };
      break;
    case DELETE_ACCESS_SUCCESS:
      state = {
        ...state,
        accessSuccess: action.payload,
        accessError: "",
        accessLoading: false,
      };
      break;
    case DELETE_ACCESS_ERROR:
      state = {
        ...state,
        accessError: action.payload,
        accessSuccess: "",
        accessLoading: false,
      };
      break;

    // Reset especifico
    case RESET_ACCESS:
      state = initialState;
      break;

    // Reset general
    case RESET:
      state = initialState;
      break;

    default:
      state = { ...state };
      break;
  }

  return state;
};

export default access;
