// ActionTypes
import {
  // Get Access List
  GET_ACCESS_LIST,
  GET_ACCESS_LIST_SUCCESS,
  GET_ACCESS_LIST_ERROR,
  // Get Access Data
  GET_ACCESS_DATA,
  GET_ACCESS_DATA_SUCCESS,
  GET_ACCESS_DATA_ERROR,
  // Add Access
  ADD_ACCESS,
  ADD_ACCESS_SUCCESS,
  ADD_ACCESS_ERROR,
  // Modify Access Data
  MODIFY_ACCESS_DATA,
  MODIFY_ACCESS_DATA_SUCCESS,
  MODIFY_ACCESS_DATA_ERROR,
  // Delete Access
  DELETE_ACCESS,
  DELETE_ACCESS_SUCCESS,
  DELETE_ACCESS_ERROR,
} from "./actionTypes";

// Get Access List
export const getAccessList = (sort, order, limit, skip, navigate) => ({
  type: GET_ACCESS_LIST,
  params: { sort, order, limit, skip, navigate },
});

export const getAccessListSuccess = (obj) => ({
  type: GET_ACCESS_LIST_SUCCESS,
  payload: obj,
});

export const getAccessListError = (error) => ({
  type: GET_ACCESS_LIST_ERROR,
  payload: error,
});

// Get Access Data
export const getAccessData = (access_id, navigate) => ({
  type: GET_ACCESS_DATA,
  params: { access_id, navigate },
});

export const getAccessDataSuccess = (obj) => ({
  type: GET_ACCESS_DATA_SUCCESS,
  payload: obj,
});

export const getAccessDataError = (error) => ({
  type: GET_ACCESS_DATA_ERROR,
  payload: error,
});

// Add Access
export const addAccess = (data, navigate) => ({
  type: ADD_ACCESS,
  params: { data, navigate },
});

export const addAccessuccess = (obj) => ({
  type: ADD_ACCESS_SUCCESS,
  payload: obj,
});

export const addAccessError = (error) => ({
  type: ADD_ACCESS_ERROR,
  payload: error,
});

// Modify Access Data
export const modifyAccessData = (access_id, data, navigate) => ({
  type: MODIFY_ACCESS_DATA,
  params: { access_id, data, navigate },
});

export const modifyAccessDataSuccess = (obj) => ({
  type: MODIFY_ACCESS_DATA_SUCCESS,
  payload: obj,
});

export const modifyAccessDataError = (error) => ({
  type: MODIFY_ACCESS_DATA_ERROR,
  payload: error,
});

// Delete Access
export const deleteAccess = (access_id, navigate) => ({
  type: DELETE_ACCESS,
  params: { access_id, navigate },
});

export const deleteAccessuccess = (obj) => ({
  type: DELETE_ACCESS_SUCCESS,
  payload: obj,
});

export const deleteAccessError = (error) => ({
  type: DELETE_ACCESS_ERROR,
  payload: error,
});
