import { takeEvery, put, call } from "redux-saga/effects";

// ActionTypes
import {
  // Get Access List
  GET_ACCESS_LIST,
  // Get Access Data
  GET_ACCESS_DATA,
  // Add Access
  ADD_ACCESS,
  // Modify Access Data
  MODIFY_ACCESS_DATA,
  // Delete Access
  DELETE_ACCESS,
} from "./actionTypes";

// Actions
import {
  // Get Access List
  getAccessListSuccess,
  getAccessListError,
  // Get Access Data
  getAccessDataSuccess,
  getAccessDataError,
  // Add Access
  addAccessuccess,
  addAccessError,
  // Modify Access Data
  modifyAccessDataSuccess,
  modifyAccessDataError,
  // Delete Access
  deleteAccessuccess,
  deleteAccessError,
} from "./actions";

// Api functions
import * as api from "api/access/access.service";

/* --- FUNCTIONS --- */
// Get Access List
function* getAccessList({ params: { sort, order, limit, skip, navigate } }) {
  try {
    const response = yield call(api.getAccessList, sort, order, limit, skip);
    if (response.status === 200) {
      yield put(getAccessListSuccess(response.data));
    } else {
      yield put(getAccessListError("MsgErr[Get Access List Failed]"));
    }
  } catch (error) {
    yield put(getAccessListError(error));
  }
}

// Get Access Data
function* getAccessData({ params: { access_id, navigate } }) {
  try {
    const response = yield call(api.getAccessData, access_id);
    if (response.status === 200) {
      yield put(getAccessDataSuccess(response.data[0]));
    } else {
      yield put(getAccessDataError("MsgErr[Get Access Data Failed]"));
    }
  } catch (error) {
    yield put(getAccessDataError(error));
  }
}

// Add Access
function* addAccess({ params: { data, navigate } }) {
  try {
    const response = yield call(api.addAccess, data);
    if (response.status === 200) {
      yield put(addAccessuccess("MsgOk[Access Added]"));
    } else {
      yield put(addAccessError("MsgErr[Get Access Data Failed]"));
    }
  } catch (error) {
    yield put(addAccessError(error));
  }
}
// Modify Access Data
function* modifyAccessData({ params: { access_id, data, navigate } }) {
  try {
    const response = yield call(api.modifyAccessData, access_id, data);
    if (response.status === 200) {
      yield put(modifyAccessDataSuccess("MsgOk[Access Data Modified]"));
    } else {
      yield put(modifyAccessDataError("MsgErr[Get Access Data Failed]"));
    }
  } catch (error) {
    yield put(modifyAccessDataError(error));
  }
}
// Delete Access
function* deleteAccess({ params: { access_id, navigate } }) {
  try {
    const response = yield call(api.deleteAccess, access_id);
    if (response.status === 200) {
      yield put(deleteAccessuccess("MsgOk[Access Data Modified]"));
    } else {
      yield put(deleteAccessError("MsgErr[Get Access Data Failed]"));
    }
  } catch (error) {
    yield put(deleteAccessError(error));
  }
}

function* AccessSaga() {
  yield takeEvery(GET_ACCESS_LIST, getAccessList);
  yield takeEvery(GET_ACCESS_DATA, getAccessData);
  yield takeEvery(ADD_ACCESS, addAccess);
  yield takeEvery(MODIFY_ACCESS_DATA, modifyAccessData);
  yield takeEvery(DELETE_ACCESS, deleteAccess);
}

export default AccessSaga;