import { combineReducers } from "redux"

// Access
import Access from "./access/reducer"
// Authentication
import Authentication from "./authentication/reducer"
// Clients
import Clients from "./clients/reducer"
// Materials
import Materials from "./materials/reducer"
// MaterialsCategories
import MaterialsCategories from "./materialsCategories/reducer"
// Operations
import Operations from "./operations/reducer"
// Products
import Products from "./products/reducer"
// ProductsCategories
import ProductsCategories from "./productsCategories/reducer"
// Roles
import Roles from "./roles/reducer"
// Services
import Services from "./services/reducer"
// Status
import Status from "./status/reducer"
// Stores
import Stores from "./stores/reducer"
// Trademarks
import Trademarks from "./trademarks/reducer"
// Users
import Users from "./users/reducer"


const appReducer = combineReducers({
  Access,
  Authentication,
  Clients,
  Materials,
  MaterialsCategories,
  Operations,
  Products,
  ProductsCategories,
  Roles,
  Services,
  Status,
  Stores,
  Trademarks,
  Users,
})

const rootReducer = (state, action) => {
  if (action.type === "LOGOUT_USER") {
    state = undefined
  }
  return appReducer(state, action)
}

export default rootReducer
