// ActionTypes
import {
  // Get Clients List
  GET_CLIENTS_LIST,
  GET_CLIENTS_LIST_SUCCESS,
  GET_CLIENTS_LIST_ERROR,
  // Get Client Data
  GET_CLIENT_DATA,
  GET_CLIENT_DATA_SUCCESS,
  GET_CLIENT_DATA_ERROR,
  // Add Client
  ADD_CLIENT,
  ADD_CLIENT_SUCCESS,
  ADD_CLIENT_ERROR,
  // Modify Client Data
  MODIFY_CLIENT_DATA,
  MODIFY_CLIENT_DATA_SUCCESS,
  MODIFY_CLIENT_DATA_ERROR,
  // Delete Client
  DELETE_CLIENT,
  DELETE_CLIENT_SUCCESS,
  DELETE_CLIENT_ERROR,
  // Specific Reset
  RESET_CLIENTS,
  // General Reset
  RESET,
} from "./actionTypes";

const initialState = {
  clientsError: "",
  clientsnSuccess: "",
  clientsLoading: false,
  clientsList: [],
  clientsListTotal: 0,
  clientData: {},
};

const clients = (state = initialState, action) => {
  switch (action.type) {
    // Get Clients List
    case GET_CLIENTS_LIST:
      state = {
        ...state,
        clientsLoading: true,
        clientsSuccess: "",
        clientsError: "",
      };
      break;
    case GET_CLIENTS_LIST_SUCCESS:
      state = {
        ...state,
        clientsList: action.payload.list,
        clientsListTotal: action.payload.total,
        clientsSuccess: "MsgOk[Clients List Received]",
        clientsError: "",
        clientsLoading: false,
      };
      break;
    case GET_CLIENTS_LIST_ERROR:
      state = {
        ...state,
        clientsList: [],
        clientsListTotal: 0,
        clientsError: action.payload,
        clientsSuccess: "",
        clientsLoading: false,
      };
      break;

    // Get Client Data
    case GET_CLIENT_DATA:
      state = {
        ...state,
        clientsLoading: true,
        clientsSuccess: "",
        clientsError: "",
      };
      break;
    case GET_CLIENT_DATA_SUCCESS:
      state = {
        ...state,
        clientData: action.payload,
        clientsSuccess: "MsgOk[Client Data Received]",
        clientsError: "",
        clientsLoading: false,
      };
      break;
    case GET_CLIENT_DATA_ERROR:
      state = {
        ...state,
        clientData: {},
        clientsError: action.payload,
        clientsSuccess: "",
        clientsLoading: false,
      };
      break;

    // Add Client
    case ADD_CLIENT:
      state = {
        ...state,
        clientsLoading: true,
        clientsSuccess: "",
        clientsError: "",
      };
      break;
    case ADD_CLIENT_SUCCESS:
      state = {
        ...state,
        clientsSuccess: action.payload,
        clientsError: "",
        clientsLoading: false,
      };
      break;
    case ADD_CLIENT_ERROR:
      state = {
        ...state,
        clientsError: action.payload,
        clientsSuccess: "",
        clientsLoading: false,
      };
      break;

    // Modify Client Data
    case MODIFY_CLIENT_DATA:
      state = {
        ...state,
        clientsLoading: true,
        clientsSuccess: "",
        clientsError: "",
      };
      break;
    case MODIFY_CLIENT_DATA_SUCCESS:
      state = {
        ...state,
        clientsSuccess: action.payload,
        clientsError: "",
        clientsLoading: false,
      };
      break;
    case MODIFY_CLIENT_DATA_ERROR:
      state = {
        ...state,
        clientsError: action.payload,
        clientsSuccess: "",
        clientsLoading: false,
      };
      break;

    // Delete Client
    case DELETE_CLIENT:
      state = {
        ...state,
        clientsLoading: true,
        clientsSuccess: "",
        clientsError: "",
      };
      break;
    case DELETE_CLIENT_SUCCESS:
      state = {
        ...state,
        clientsSuccess: action.payload,
        clientsError: "",
        clientsLoading: false,
      };
      break;
    case DELETE_CLIENT_ERROR:
      state = {
        ...state,
        clientsError: action.payload,
        clientsSuccess: "",
        clientsLoading: false,
      };
      break;

    // Reset especifico
    case RESET_CLIENTS:
      state = initialState;
      break;

    // Reset general
    case RESET:
      state = initialState;
      break;

    default:
      state = { ...state };
      break;
  }

  return state;
};

export default clients;
