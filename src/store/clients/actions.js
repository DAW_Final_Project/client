// ActionTypes
import {
  // Get Clients List
  GET_CLIENTS_LIST,
  GET_CLIENTS_LIST_SUCCESS,
  GET_CLIENTS_LIST_ERROR,
  // Get Client Data
  GET_CLIENT_DATA,
  GET_CLIENT_DATA_SUCCESS,
  GET_CLIENT_DATA_ERROR,
  // Add Client
  ADD_CLIENT,
  ADD_CLIENT_SUCCESS,
  ADD_CLIENT_ERROR,
  // Modify Client Data
  MODIFY_CLIENT_DATA,
  MODIFY_CLIENT_DATA_SUCCESS,
  MODIFY_CLIENT_DATA_ERROR,
  // Delete Client
  DELETE_CLIENT,
  DELETE_CLIENT_SUCCESS,
  DELETE_CLIENT_ERROR,
} from "./actionTypes";

// Get Clients List
export const getClientsList = (sort, order, limit, skip, navigate) => ({
  type: GET_CLIENTS_LIST,
  params: { sort, order, limit, skip, navigate },
});

export const getClientsListSuccess = (obj) => ({
  type: GET_CLIENTS_LIST_SUCCESS,
  payload: obj,
});

export const getClientsListError = (error) => ({
  type: GET_CLIENTS_LIST_ERROR,
  payload: error,
});

// Get Client Data
export const getClientData = (client_id, navigate) => ({
  type: GET_CLIENT_DATA,
  params: { client_id, navigate },
});

export const getClientDataSuccess = (obj) => ({
  type: GET_CLIENT_DATA_SUCCESS,
  payload: obj,
});

export const getClientDataError = (error) => ({
  type: GET_CLIENT_DATA_ERROR,
  payload: error,
});

// Add Client
export const addClient = (data, navigate) => ({
  type: ADD_CLIENT,
  params: { data, navigate },
});

export const addClientSuccess = (obj) => ({
  type: ADD_CLIENT_SUCCESS,
  payload: obj,
});

export const addClientError = (error) => ({
  type: ADD_CLIENT_ERROR,
  payload: error,
});

// Modify Client Data
export const modifyClientData = (client_id, data, navigate) => ({
  type: MODIFY_CLIENT_DATA,
  params: { client_id, data, navigate },
});

export const modifyClientDataSuccess = (obj) => ({
  type: MODIFY_CLIENT_DATA_SUCCESS,
  payload: obj,
});

export const modifyClientDataError = (error) => ({
  type: MODIFY_CLIENT_DATA_ERROR,
  payload: error,
});

// Delete Client
export const deleteClient = (client_id, navigate) => ({
  type: DELETE_CLIENT,
  params: { client_id, navigate },
});

export const deleteClientSuccess = (obj) => ({
  type: DELETE_CLIENT_SUCCESS,
  payload: obj,
});

export const deleteClientError = (error) => ({
  type: DELETE_CLIENT_ERROR,
  payload: error,
});
