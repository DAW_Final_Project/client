import { takeEvery, put, call } from "redux-saga/effects";

// ActionTypes
import {
  // Get Clients List
  GET_CLIENTS_LIST,
  // Get Client Data
  GET_CLIENT_DATA,
  // Add Client
  ADD_CLIENT,
  // Modify Client Data
  MODIFY_CLIENT_DATA,
  // Delete Client
  DELETE_CLIENT,
} from "./actionTypes";

// Actions
import {
  // Get Clients List
  getClientsListSuccess,
  getClientsListError,
  // Get Client Data
  getClientDataSuccess,
  getClientDataError,
  // Add Client
  addClientSuccess,
  addClientError,
  // Modify Client Data
  modifyClientDataSuccess,
  modifyClientDataError,
  // Delete Client
  deleteClientSuccess,
  deleteClientError,
} from "./actions";

// Api functions
import * as api from "api/clients/clients.service";

/* --- FUNCTIONS --- */
// Get Clients List
function* getClientsList({ params: { sort, order, limit, skip, navigate } }) {
  try {
    const response = yield call(api.getClientsList, sort, order, limit, skip);
    if (response.status === 200) {
      yield put(getClientsListSuccess(response.data));
    } else {
      yield put(getClientsListError("MsgErr[Get Clients List Failed]"));
    }
  } catch (error) {
    yield put(getClientsListError(error));
  }
}

// Get Client Data
function* getClientData({ params: { client_id, navigate } }) {
  try {
    const response = yield call(api.getClientData, client_id);
    if (response.status === 200) {
      yield put(getClientDataSuccess(response.data[0]));
    } else {
      yield put(getClientDataError("MsgErr[Get Client Data Failed]"));
    }
  } catch (error) {
    yield put(getClientDataError(error));
  }
}

// Add Client
function* addClient({ params: { data, navigate } }) {
  try {
    const response = yield call(api.addClient, data);
    if (response.status === 200) {
      yield put(addClientSuccess("MsgOk[Client Added]"));
    } else {
      yield put(addClientError("MsgErr[Get Client Data Failed]"));
    }
  } catch (error) {
    yield put(addClientError(error));
  }
}
// Modify Client Data
function* modifyClientData({ params: { client_id, data, navigate } }) {
  try {
    const response = yield call(api.modifyClientData, client_id, data);
    if (response.status === 200) {
      yield put(modifyClientDataSuccess("MsgOk[Client Data Modified]"));
    } else {
      yield put(modifyClientDataError("MsgErr[Get Client Data Failed]"));
    }
  } catch (error) {
    yield put(modifyClientDataError(error));
  }
}
// Delete Client
function* deleteClient({ params: { client_id, navigate } }) {
  try {
    const response = yield call(api.deleteClient, client_id);
    if (response.status === 200) {
      yield put(deleteClientSuccess("MsgOk[Client Deleted]"));
    } else {
      yield put(deleteClientError("MsgErr[Delete Client Failed]"));
    }
  } catch (error) {
    yield put(deleteClientError(error));
  }
}

function* ClientsSaga() {
  yield takeEvery(GET_CLIENTS_LIST, getClientsList);
  yield takeEvery(GET_CLIENT_DATA, getClientData);
  yield takeEvery(ADD_CLIENT, addClient);
  yield takeEvery(MODIFY_CLIENT_DATA, modifyClientData);
  yield takeEvery(DELETE_CLIENT, deleteClient);
}

export default ClientsSaga;