// ActionTypes
import {
  // Get Materials List
  GET_MATERIALS_LIST,
  GET_MATERIALS_LIST_SUCCESS,
  GET_MATERIALS_LIST_ERROR,
  // Get Material Data
  GET_MATERIAL_DATA,
  GET_MATERIAL_DATA_SUCCESS,
  GET_MATERIAL_DATA_ERROR,
  // Add Material
  ADD_MATERIAL,
  ADD_MATERIAL_SUCCESS,
  ADD_MATERIAL_ERROR,
  // Modify Material Data
  MODIFY_MATERIAL_DATA,
  MODIFY_MATERIAL_DATA_SUCCESS,
  MODIFY_MATERIAL_DATA_ERROR,
  // Delete Material
  DELETE_MATERIAL,
  DELETE_MATERIAL_SUCCESS,
  DELETE_MATERIAL_ERROR,
} from "./actionTypes";

// Get Materials List
export const getMaterialsList = (sort, order, limit, skip, navigate) => ({
  type: GET_MATERIALS_LIST,
  params: { sort, order, limit, skip, navigate },
});

export const getMaterialsListSuccess = (obj) => ({
  type: GET_MATERIALS_LIST_SUCCESS,
  payload: obj,
});

export const getMaterialsListError = (error) => ({
  type: GET_MATERIALS_LIST_ERROR,
  payload: error,
});

// Get Material Data
export const getMaterialData = (material_id, navigate) => ({
  type: GET_MATERIAL_DATA,
  params: { material_id, navigate },
});

export const getMaterialDataSuccess = (obj) => ({
  type: GET_MATERIAL_DATA_SUCCESS,
  payload: obj,
});

export const getMaterialDataError = (error) => ({
  type: GET_MATERIAL_DATA_ERROR,
  payload: error,
});

// Add Material
export const addMaterial = (data, navigate) => ({
  type: ADD_MATERIAL,
  params: { data, navigate },
});

export const addMaterialSuccess = (obj) => ({
  type: ADD_MATERIAL_SUCCESS,
  payload: obj,
});

export const addMaterialError = (error) => ({
  type: ADD_MATERIAL_ERROR,
  payload: error,
});

// Modify Material Data
export const modifyMaterialData = (material_id, data, navigate) => ({
  type: MODIFY_MATERIAL_DATA,
  params: { material_id, data, navigate },
});

export const modifyMaterialDataSuccess = (obj) => ({
  type: MODIFY_MATERIAL_DATA_SUCCESS,
  payload: obj,
});

export const modifyMaterialDataError = (error) => ({
  type: MODIFY_MATERIAL_DATA_ERROR,
  payload: error,
});

// Delete Material
export const deleteMaterial = (material_id, navigate) => ({
  type: DELETE_MATERIAL,
  params: { material_id, navigate },
});

export const deleteMaterialSuccess = (obj) => ({
  type: DELETE_MATERIAL_SUCCESS,
  payload: obj,
});

export const deleteMaterialError = (error) => ({
  type: DELETE_MATERIAL_ERROR,
  payload: error,
});
