// ActionTypes
import {
  // Get Materials List
  GET_MATERIALS_LIST,
  GET_MATERIALS_LIST_SUCCESS,
  GET_MATERIALS_LIST_ERROR,
  // Get Material Data
  GET_MATERIAL_DATA,
  GET_MATERIAL_DATA_SUCCESS,
  GET_MATERIAL_DATA_ERROR,
  // Add Material
  ADD_MATERIAL,
  ADD_MATERIAL_SUCCESS,
  ADD_MATERIAL_ERROR,
  // Modify Material Data
  MODIFY_MATERIAL_DATA,
  MODIFY_MATERIAL_DATA_SUCCESS,
  MODIFY_MATERIAL_DATA_ERROR,
  // Delete Material
  DELETE_MATERIAL,
  DELETE_MATERIAL_SUCCESS,
  DELETE_MATERIAL_ERROR,
  // Specific Reset
  RESET_MATERIALS,
  // General Reset
  RESET,
} from "./actionTypes";

const initialState = {
  materialsError: "",
  materialsnSuccess: "",
  materialsLoading: false,
  materialsList: [],
  materialsListTotal: 0,
  materialData: {},
};

const materials = (state = initialState, action) => {
  switch (action.type) {
    // Get Materials List
    case GET_MATERIALS_LIST:
      state = {
        ...state,
        materialsLoading: true,
        materialsSuccess: "",
        materialsError: "",
      };
      break;
    case GET_MATERIALS_LIST_SUCCESS:
      state = {
        ...state,
        materialsList: action.payload.list,
        materialsListTotal: action.payload.total,
        materialsSuccess: "MsgOk[Materials List Received]",
        materialsError: "",
        materialsLoading: false,
      };
      break;
    case GET_MATERIALS_LIST_ERROR:
      state = {
        ...state,
        materialsList: [],
        materialsListTotal: 0,
        materialsError: action.payload,
        materialsSuccess: "",
        materialsLoading: false,
      };
      break;

    // Get Material Data
    case GET_MATERIAL_DATA:
      state = {
        ...state,
        materialsLoading: true,
        materialsSuccess: "",
        materialsError: "",
      };
      break;
    case GET_MATERIAL_DATA_SUCCESS:
      state = {
        ...state,
        materialData: action.payload,
        materialsSuccess: "MsgOk[Material Data Received]",
        materialsError: "",
        materialsLoading: false,
      };
      break;
    case GET_MATERIAL_DATA_ERROR:
      state = {
        ...state,
        materialData: {},
        materialsError: action.payload,
        materialsSuccess: "",
        materialsLoading: false,
      };
      break;

    // Add Material
    case ADD_MATERIAL:
      state = {
        ...state,
        materialsLoading: true,
        materialsSuccess: "",
        materialsError: "",
      };
      break;
    case ADD_MATERIAL_SUCCESS:
      state = {
        ...state,
        materialsSuccess: action.payload,
        materialsError: "",
        materialsLoading: false,
      };
      break;
    case ADD_MATERIAL_ERROR:
      state = {
        ...state,
        materialsError: action.payload,
        materialsSuccess: "",
        materialsLoading: false,
      };
      break;

    // Modify Material Data
    case MODIFY_MATERIAL_DATA:
      state = {
        ...state,
        materialsLoading: true,
        materialsSuccess: "",
        materialsError: "",
      };
      break;
    case MODIFY_MATERIAL_DATA_SUCCESS:
      state = {
        ...state,
        materialsSuccess: action.payload,
        materialsError: "",
        materialsLoading: false,
      };
      break;
    case MODIFY_MATERIAL_DATA_ERROR:
      state = {
        ...state,
        materialsError: action.payload,
        materialsSuccess: "",
        materialsLoading: false,
      };
      break;

    // Delete Material
    case DELETE_MATERIAL:
      state = {
        ...state,
        materialsLoading: true,
        materialsSuccess: "",
        materialsError: "",
      };
      break;
    case DELETE_MATERIAL_SUCCESS:
      state = {
        ...state,
        materialsSuccess: action.payload,
        materialsError: "",
        materialsLoading: false,
      };
      break;
    case DELETE_MATERIAL_ERROR:
      state = {
        ...state,
        materialsError: action.payload,
        materialsSuccess: "",
        materialsLoading: false,
      };
      break;

    // Reset especifico
    case RESET_MATERIALS:
      state = initialState;
      break;

    // Reset general
    case RESET:
      state = initialState;
      break;

    default:
      state = { ...state };
      break;
  }

  return state;
};

export default materials;
