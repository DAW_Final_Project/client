import { takeEvery, put, call } from "redux-saga/effects";

// ActionTypes
import {
  // Get Materials List
  GET_MATERIALS_LIST,
  // Get Material Data
  GET_MATERIAL_DATA,
  // Add Material
  ADD_MATERIAL,
  // Modify Material Data
  MODIFY_MATERIAL_DATA,
  // Delete Material
  DELETE_MATERIAL,
} from "./actionTypes";

// Actions
import {
  // Get Materials List
  getMaterialsListSuccess,
  getMaterialsListError,
  // Get Material Data
  getMaterialDataSuccess,
  getMaterialDataError,
  // Add Material
  addMaterialSuccess,
  addMaterialError,
  // Modify Material Data
  modifyMaterialDataSuccess,
  modifyMaterialDataError,
  // Delete Material
  deleteMaterialSuccess,
  deleteMaterialError,
} from "./actions";

// Api functions
import * as api from "api/materials/materials.service";

/* --- FUNCTIONS --- */
// Get Materials List
function* getMaterialsList({ params: { sort, order, limit, skip, navigate } }) {
  try {
    const response = yield call(api.getMaterialsList, sort, order, limit, skip);
    if (response.status === 200) {
      yield put(getMaterialsListSuccess(response.data));
    } else {
      yield put(getMaterialsListError("MsgErr[Get Materials List Failed]"));
    }
  } catch (error) {
    yield put(getMaterialsListError(error));
  }
}

// Get Material Data
function* getMaterialData({ params: { material_id, navigate } }) {
  try {
    const response = yield call(api.getMaterialData, material_id);
    if (response.status === 200) {
      yield put(getMaterialDataSuccess(response.data[0]));
    } else {
      yield put(getMaterialDataError("MsgErr[Get Material Data Failed]"));
    }
  } catch (error) {
    yield put(getMaterialDataError(error));
  }
}

// Add Material
function* addMaterial({ params: { data, navigate } }) {
  try {
    const response = yield call(api.addMaterial, data);
    if (response.status === 200) {
      yield put(addMaterialSuccess("MsgOk[Material Added]"));
    } else {
      yield put(addMaterialError("MsgErr[Get Material Data Failed]"));
    }
  } catch (error) {
    yield put(addMaterialError(error));
  }
}
// Modify Material Data
function* modifyMaterialData({ params: { material_id, data, navigate } }) {
  try {
    const response = yield call(api.modifyMaterialData, material_id, data);
    if (response.status === 200) {
      yield put(modifyMaterialDataSuccess("MsgOk[Material Data Modified]"));
    } else {
      yield put(modifyMaterialDataError("MsgErr[Get Material Data Failed]"));
    }
  } catch (error) {
    yield put(modifyMaterialDataError(error));
  }
}
// Delete Material
function* deleteMaterial({ params: { material_id, navigate } }) {
  try {
    const response = yield call(api.deleteMaterial, material_id);
    if (response.status === 200) {
      yield put(deleteMaterialSuccess("MsgOk[Material Data Modified]"));
    } else {
      yield put(deleteMaterialError("MsgErr[Get Material Data Failed]"));
    }
  } catch (error) {
    yield put(deleteMaterialError(error));
  }
}

function* MaterialsSaga() {
  yield takeEvery(GET_MATERIALS_LIST, getMaterialsList);
  yield takeEvery(GET_MATERIAL_DATA, getMaterialData);
  yield takeEvery(ADD_MATERIAL, addMaterial);
  yield takeEvery(MODIFY_MATERIAL_DATA, modifyMaterialData);
  yield takeEvery(DELETE_MATERIAL, deleteMaterial);
}

export default MaterialsSaga;