import React from "react";
import ReactDOM from 'react-dom/client';

// React Router Dom
import { BrowserRouter } from "react-router-dom";

// Redux
import { Provider } from "react-redux"
import store from "./store"

// Components
import App from "./App";

//Styles
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import "bootstrap/dist/css/bootstrap.min.css";
import 'boxicons/css/boxicons.min.css';
import "./assets/css/custom.css"

//RENDER
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>
);
