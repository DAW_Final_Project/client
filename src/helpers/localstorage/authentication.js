import { SS_USER, SS_TOKEN } from "./definitions";

//is user is logged in
const isUserAuthenticated = () => {
  return getLoggedInUser() !== null;
};

// Gets the logged in user data from local session
const getLoggedInUser = () => {
  const user = sessionStorage.getItem(SS_USER);
  if (user) return JSON.parse(user);
  return null;
};

// assign sessionStorage by user
const assignLoggedInUser = (user, token) => {
    sessionStorage.setItem(SS_USER, user)
    sessionStorage.setItem(SS_TOKEN, token)
  }

// remove sessionStorage by user
const removeLoggedInUser = () => {
  sessionStorage.removeItem(SS_USER);
  sessionStorage.removeItem(SS_TOKEN)
};

// get Authorization token from local session
const getAuthUserHeader = () => {
    /* const user = sessionStorage.getItem(SS_USER); */
    const token = sessionStorage.getItem(SS_TOKEN)
    if (token) return token
    return null
  }

export {
  isUserAuthenticated,
  getLoggedInUser,
  assignLoggedInUser,
  removeLoggedInUser,
  getAuthUserHeader,
};
