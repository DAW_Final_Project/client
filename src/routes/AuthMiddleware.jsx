// React Router Dom
import withRouter from "./js/withRouter";
import { Navigate } from "react-router-dom";

// authentication

import { isUserAuthenticated } from "../helpers/localstorage/authentication";

// Layouts
import AuthLayout from "../components/layouts/AuthLayout";
import NonAuthLayout from "../components/layouts/NonAuthLayout";

const AuthMiddleware = (props) => {
  let Layout;
  let Component = props.component;

  if (props.authRequired) {
   if (!isUserAuthenticated()) {
      return <Navigate to="/" />;
    } else {
      Layout = AuthLayout;
    }
  } else {
    Layout = NonAuthLayout;
  }
  return (
    <Layout>
      <Component {...props} />
    </Layout>
  );
};
export default withRouter(AuthMiddleware);
