// React Router Dom
import { Routes, Route } from "react-router-dom";
import AuthMiddleware from "./AuthMiddleware";

// Views
import Index from "pages/freeaccess/common/Login"
import AdminDashboard from "pages/authorized/admin/AdminDashboard"
import DeskDashboard from "pages/authorized/desk/DeskDashboard"
import TechDashboard from "pages/authorized/tech/TechDashboard"

// Clients
import Clients from "pages/authorized/clients/Clients-List"
import Client from "pages/authorized/clients/Client"
import AddClient from "pages/authorized/clients/AddClient"

// Users
import Users from "pages/authorized/users/Users-List"
import User from "pages/authorized/users/User"
import AddUser from "pages/authorized/users/AddUser"

// Services
import Services from "pages/authorized/services/Services-List"
import Service from "pages/authorized/services/Service"
import AddService from "pages/authorized/services/AddService"

// ERRORS
import NotFound from "components/common/NotFound"
import UnderConstruction from "components/common/UnderConstruction"


const AllRoutes = () => {
  return (
    <Routes>
      {/* FREE ACCESS ROUTES */}
      <Route path="/" element={<AuthMiddleware component={Index} authRequired={false}/>} />
      {/* AUTHORIZATION REQUIRED ROUTES */}
      <Route path="/admindashboard" element={<AuthMiddleware component={UnderConstruction} authRequired={true}/>} />
      <Route path="/deskdashboard" element={<AuthMiddleware component={DeskDashboard} authRequired={true}/>} />
      <Route path="/techdashboard" element={<AuthMiddleware component={UnderConstruction} authRequired={true}/>} />    
      <Route path="/clients" element={<AuthMiddleware component={Clients} authRequired={true}/>} />  
      <Route path="/client/:client_id" element={<AuthMiddleware component={Client} authRequired={true}/>} />                    
      <Route path="/addclient" element={<AuthMiddleware component={AddClient} authRequired={true}/>} />       
      <Route path="/users" element={<AuthMiddleware component={Users} authRequired={true}/>} />  
      <Route path="/user/:user_id" element={<AuthMiddleware component={User} authRequired={true}/>} />                    
      <Route path="/adduser" element={<AuthMiddleware component={AddUser} authRequired={true}/>} />       
      <Route path="/services/:client_id" element={<AuthMiddleware component={Services} authRequired={true}/>} />  
      <Route path="/client/:client_id/service/:service_id" element={<AuthMiddleware component={Service} authRequired={true}/>} />                    
      <Route path="/addservice" element={<AuthMiddleware component={AddService} authRequired={true}/>} />
      {/* ERRORS */}
      <Route path="*" element={<NotFound/>} />

    
      
    </Routes>
  );
};

export default AllRoutes;
