import i18n from "i18next"
import detector from "i18next-browser-languagedetector"
import { initReactI18next } from "react-i18next"

import { LS_I18N } from './helpers/localstorage/definitions'

import translationSP from "./locales/sp/translation.json"
import translationENG from "./locales/en/translation.json"
import translationVAL from "./locales/val/translation.json"

// the translations
const resources = {
  en: {
    translation: translationENG,
  },
  sp: {
    translation: translationSP,
  },
  val: {
    translation: translationVAL,
  },
}

const language = localStorage.getItem(LS_I18N)
if (!language) {
  localStorage.setItem(LS_I18N, "val")
}

i18n
  .use(detector)
  .use(initReactI18next)
  .init({
    resources,
    lng:  localStorage.getItem(LS_I18N)  || "val",
    fallbackLng: "val",

    keySeparator: false,

    interpolation: {
      escapeValue: false,
    },
  })

export default i18n
