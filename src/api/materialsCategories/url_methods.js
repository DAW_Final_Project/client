// Get MaterialsCategories List
export const GET_MATERIALSCATEGORIES_LIST = "/materialscategories/list"

// Get MaterialsCategory Data
export const GET_MATERIALSCATEGORY_DATA = "/materialscategories/data/{materialscategory_id}"

// Add MaterialsCategory
export const ADD_MATERIALSCATEGORY = "/materialscategories/add"

// Modify MaterialsCategory Data
export const MODIFY_MATERIALSCATEGORY_DATA = "/materialscategories/modify/{materialscategory_id}"

// Delete MaterialsCategory
export const DELETE_MATERIALSCATEGORY = "/materialscategories/delete/{materialscategory_id}"