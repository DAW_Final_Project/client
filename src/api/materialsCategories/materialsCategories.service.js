import { get, post, put, del } from "../axios.config";
import * as url from "./url_methods";
import { getAuthUserHeader } from "helpers/localstorage/authentication";

// Get MaterialsCategories List
const getMaterialsCategoriesList = (sort, order, limit, skip) =>
  get(url.GET_MATERIALSCATEGORIES_LIST, {
    headers: {
      Authorization: getAuthUserHeader(),
    },
    params: {
      sort: sort,
      order: order,
      limit: limit,
      skip: skip,
    },
  });

// Get MaterialsCategory Data
const getMaterialsCategoryData = (materialscategory_id) =>
  get(url.GET_MATERIALSCATEGORY_DATA.replace("{materialscategory_id}", materialscategory_id), {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

// Add MaterialsCategory
const addMaterialsCategory = (data) =>
  post(url.ADD_MATERIALSCATEGORY, data, {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

// Modify MaterialsCategory Data
const modifyMaterialsCategoryData = (materialscategory_id, data) =>
  put(url.MODIFY_MATERIALSCATEGORY_DATA.replace("{materialscategory_id}", materialscategory_id), data, {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

// Delete MaterialsCategory
const deleteMaterialsCategory = (materialscategory_id) =>
  del(url.DELETE_MATERIALSCATEGORY.replace("{materialscategory_id}", materialscategory_id), {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

export {
  getMaterialsCategoriesList,
  getMaterialsCategoryData,
  addMaterialsCategory,
  modifyMaterialsCategoryData,
  deleteMaterialsCategory,
};
