// Get Access List
export const GET_ACCESS_LIST = "/access/list"

// Get Access Data
export const GET_ACCESS_DATA = "/access/data/{access_id}"

// Add Access
export const ADD_ACCESS = "/access/add"

// Modify Access Data
export const MODIFY_ACCESS_DATA = "/access/modify/{access_id}"

// Delete Access
export const DELETE_ACCESS = "/access/delete/{access_id}"