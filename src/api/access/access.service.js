import { get, post, put, del } from "../axios.config";
import * as url from "./url_methods";
import { getAuthUserHeader } from "helpers/localstorage/authentication";

// Get Access List
const getAccessList = (sort, order, limit, skip) =>
  get(url.GET_ACCESS_LIST, {
    headers: {
      Authorization: getAuthUserHeader(),
    },
    params: {
      sort: sort,
      order: order,
      limit: limit,
      skip: skip,
    },
  });

// Get Access Data
const getAccessData = (access_id) =>
  get(url.GET_ACCESS_DATA.replace("{access_id}", access_id), {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

// Add Access
const addAccess = (data) =>
  post(url.ADD_ACCESS, data, {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

// Modify Access Data
const modifyAccessData = (access_id, data) =>
  put(url.MODIFY_ACCESS_DATA.replace("{access_id}", access_id), data, {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

// Delete Access
const deleteAccess = (access_id) =>
  del(url.DELETE_ACCESS.replace("{access_id}", access_id), {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

export {
  getAccessList,
  getAccessData,
  addAccess,
  modifyAccessData,
  deleteAccess,
};
