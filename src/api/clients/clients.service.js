import { get, post, put, del } from "../axios.config";
import * as url from "./url_methods";
import { getAuthUserHeader } from "helpers/localstorage/authentication";

// Get Clients List
const getClientsList = (sort, order, limit, skip) =>
  get(url.GET_CLIENTS_LIST, {
    headers: {
      Authorization: getAuthUserHeader(),
    },
    params: {
      sort: sort,
      order: order,
      limit: limit,
      skip: skip,
    },
  });

// Get Client Data
const getClientData = (client_id) =>
  get(url.GET_CLIENT_DATA.replace("{client_id}", client_id), {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

// Add Client
const addClient = (data) =>
  post(url.ADD_CLIENT, data, {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

// Modify Client Data
const modifyClientData = (client_id, data) =>
  put(url.MODIFY_CLIENT_DATA.replace("{client_id}", client_id), data, {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

// Delete Client
const deleteClient = (client_id) =>
  del(url.DELETE_CLIENT.replace("{client_id}", client_id), {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

export {
  getClientsList,
  getClientData,
  addClient,
  modifyClientData,
  deleteClient,
};
