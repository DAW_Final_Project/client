// Get Clients List
export const GET_CLIENTS_LIST = "/clients/list"

// Get Client Data
export const GET_CLIENT_DATA = "/clients/data/{client_id}"

// Add Client
export const ADD_CLIENT = "/clients/add"

// Modify Client Data
export const MODIFY_CLIENT_DATA = "/clients/modify/{client_id}"

// Delete Client
export const DELETE_CLIENT = "/clients/delete/{client_id}"