import axios from "axios"

//apply base url for axios
const API_URL = process.env.REACT_APP_API_URL

const axiosApi = axios.create({
  baseURL: API_URL,
})

// Interceptors
axiosApi.interceptors.request.use(
  config => {
    if (config.method == "post" || config.method == "put") {
      config.headers["Content-Type"] = "application/json"
    }
    // console.log("config",config);
    return config
  },
  function (error) {
    console.log(error)
    // Do something with request error
    return Promise.reject(error)
  }
)

axiosApi.interceptors.response.use(
  response => response,
  error => Promise.reject(error)
)

//GET
export async function get(url, config = {}) {
  return await axiosApi
    .get(url, { ...config })
    .then(response => response)
    .catch(error => error.response)
}

//POST
export async function post(url, data, config = {}) {  
  return await axiosApi
    .post(url, data, { ...config })
    .then(response => response)
    .catch(error => error.response)
}

//PATCH
export async function patch(url, data, config = {}) {
  return await axiosApi
    .patch(url, data, { ...config })
    .then(response => response)
    .catch(error => error.response)
}

//PUT
export async function put(url, data, config = {}) {
  return await axiosApi
    .put(url, data, { ...config })
    .then(response => response)
    .catch(error => error.response)
}

//DEL
export async function del(url, config = {}) {
  return await axiosApi
    .delete(url, { ...config })
    .then(response => response)
    .catch(error => error.response)
}

function validError(error) {
  if (error.isAxiosError) return { status: "500 (Axios)" }
  else return error.response
}

export default axiosApi
