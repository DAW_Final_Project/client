import { get, post, put, del } from "../axios.config";
import * as url from "./url_methods";
import { getAuthUserHeader } from "helpers/localstorage/authentication";

// Get Services List
const getServicesList = (client_id, sort, order, limit, skip) =>
  get(url.GET_SERVICES_LIST.replace("{client_id}", client_id), {
    headers: {
      Authorization: getAuthUserHeader(),
    },
    params: {
      sort: sort,
      order: order,
      limit: limit,
      skip: skip,
    },
  });

// Get Service Data
const getServiceData = (service_id) =>
  get(url.GET_SERVICE_DATA.replace("{service_id}", service_id), {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

// Add Service
const addService = (data) =>
  post(url.ADD_SERVICE, data, {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

// Modify Service Data
const modifyServiceData = (service_id, data) =>
  put(url.MODIFY_SERVICE_DATA.replace("{service_id}", service_id), data, {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

// Delete Service
const deleteService = (service_id) =>
  del(url.DELETE_SERVICE.replace("{service_id}", service_id), {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

export {
  getServicesList,
  getServiceData,
  addService,
  modifyServiceData,
  deleteService,
};
