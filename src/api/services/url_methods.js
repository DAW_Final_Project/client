// Get Services List
export const GET_SERVICES_LIST = "/services/list/{client_id}"

// Get Service Data
export const GET_SERVICE_DATA = "/services/data/{service_id}"

// Add Service
export const ADD_SERVICE = "/services/add"

// Modify Service Data
export const MODIFY_SERVICE_DATA = "/services/modify/{service_id}"

// Delete Service
export const DELETE_SERVICE = "/services/delete/{service_id}"