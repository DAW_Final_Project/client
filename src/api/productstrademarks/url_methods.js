// Get Trademarks List
export const GET_TRADEMARKS_LIST = "/productstrademarks/list"

// Get Trademark Data
export const GET_TRADEMARK_DATA = "/productstrademarks/data/{trademark_id}"

// Add Trademark
export const ADD_TRADEMARK = "/productstrademarks/add"

// Modify Trademark Data
export const MODIFY_TRADEMARK_DATA = "/productstrademarks/modify/{trademark_id}"

// Delete Trademark
export const DELETE_TRADEMARK = "/productstrademarks/delete/{trademark_id}"