import { get, post, put, del } from "../axios.config";
import * as url from "./url_methods";
import { getAuthUserHeader } from "helpers/localstorage/authentication";

// Get Trademarks List
const getTrademarksList = (sort, order, limit, skip) =>
  get(url.GET_TRADEMARKS_LIST, {
    headers: {
      Authorization: getAuthUserHeader(),
    },
    params: {
      sort: sort,
      order: order,
      limit: limit,
      skip: skip,
    },
  });

// Get Trademark Data
const getTrademarkData = (trademark_id) =>
  get(url.GET_TRADEMARK_DATA.replace("{trademark_id}", trademark_id), {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

// Add Trademark
const addTrademark = (data) =>
  post(url.ADD_TRADEMARK, data, {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

// Modify Trademark Data
const modifyTrademarkData = (trademark_id, data) =>
  put(url.MODIFY_TRADEMARK_DATA.replace("{trademark_id}", trademark_id), data, {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

// Delete Trademark
const deleteTrademark = (trademark_id) =>
  del(url.DELETE_TRADEMARK.replace("{trademark_id}", trademark_id), {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

export {
  getTrademarksList,
  getTrademarkData,
  addTrademark,
  modifyTrademarkData,
  deleteTrademark,
};
