import { get, post, put, del } from "../axios.config";
import * as url from "./url_methods";
import { getAuthUserHeader } from "helpers/localstorage/authentication";

// Get ProductsCategories List
const getProductsCategoriesList = (sort, order, limit, skip) =>
  get(url.GET_PRODUCTSCATEGORIES_LIST, {
    headers: {
      Authorization: getAuthUserHeader(),
    },
    params: {
      sort: sort,
      order: order,
      limit: limit,
      skip: skip,
    },
  });

// Get ProductsCategory Data
const getProductsCategoryData = (productscategory_id) =>
  get(url.GET_PRODUCTSCATEGORY_DATA.replace("{productscategory_id}", productscategory_id), {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

// Add ProductsCategory
const addProductsCategory = (data) =>
  post(url.ADD_PRODUCTSCATEGORY, data, {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

// Modify ProductsCategory Data
const modifyProductsCategoryData = (productscategory_id, data) =>
  put(url.MODIFY_PRODUCTSCATEGORY_DATA.replace("{productscategory_id}", productscategory_id), data, {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

// Delete ProductsCategory
const deleteProductsCategory = (productscategory_id) =>
  del(url.DELETE_PRODUCTSCATEGORY.replace("{productscategory_id}", productscategory_id), {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

export {
  getProductsCategoriesList,
  getProductsCategoryData,
  addProductsCategory,
  modifyProductsCategoryData,
  deleteProductsCategory,
};
