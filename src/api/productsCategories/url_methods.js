// Get ProductsCategories List
export const GET_PRODUCTSCATEGORIES_LIST = "/productscategories/list"

// Get ProductsCategory Data
export const GET_PRODUCTSCATEGORY_DATA = "/productscategories/data/{productscategory_id}"

// Add ProductsCategory
export const ADD_PRODUCTSCATEGORY = "/productscategories/add"

// Modify ProductsCategory Data
export const MODIFY_PRODUCTSCATEGORY_DATA = "/productscategories/modify/{productscategory_id}"

// Delete ProductsCategory
export const DELETE_PRODUCTSCATEGORY = "/productscategories/delete/{productscategory_id}"