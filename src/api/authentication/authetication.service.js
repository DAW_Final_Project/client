import { get, post } from "../axios.config";
import * as url from "./url_methods";

// Login
/* const login = (data) => post(url.LOGIN, data); */


const login = data => {
    return get(url.LOGIN, {
      auth: { store:data.store, username: data.username, password: data.password },
      params: {
        store: data.store
      },
    })
  }

export { login };
