import { get, post, put, del } from "../axios.config";
import * as url from "./url_methods";
import { getAuthUserHeader } from "helpers/localstorage/authentication";

// Get Roles List
const getRolesList = (sort, order, limit, skip) =>
  get(url.GET_ROLES_LIST, {
    headers: {
      Authorization: getAuthUserHeader(),
    },
    params: {
      sort: sort,
      order: order,
      limit: limit,
      skip: skip,
    },
  });

// Get Role Data
const getRoleData = (role_id) =>
  get(url.GET_ROLE_DATA.replace("{role_id}", role_id), {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

// Add Role
const addRole = (data) =>
  post(url.ADD_ROLE, data, {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

// Modify Role Data
const modifyRoleData = (role_id, data) =>
  put(url.MODIFY_ROLE_DATA.replace("{role_id}", role_id), data, {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

// Delete Role
const deleteRole = (role_id) =>
  del(url.DELETE_ROLE.replace("{role_id}", role_id), {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

export {
  getRolesList,
  getRoleData,
  addRole,
  modifyRoleData,
  deleteRole,
};
