// Get Roles List
export const GET_ROLES_LIST = "/roles/list"

// Get Role Data
export const GET_ROLE_DATA = "/roles/data/{role_id}"

// Add Role
export const ADD_ROLE = "/roles/add"

// Modify Role Data
export const MODIFY_ROLE_DATA = "/roles/modify/{role_id}"

// Delete Role
export const DELETE_ROLE = "/roles/delete/{role_id}"