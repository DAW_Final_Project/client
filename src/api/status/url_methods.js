// Get Status List
export const GET_STATUS_LIST = "/status/list"

// Get Status Data
export const GET_STATUS_DATA = "/status/data/{status_id}"

// Add Status
export const ADD_STATUS = "/status/add"

// Modify Status Data
export const MODIFY_STATUS_DATA = "/status/modify/{status_id}"

// Delete Status
export const DELETE_STATUS = "/status/delete/{status_id}"