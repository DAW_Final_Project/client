import { get, post, put, del } from "../axios.config";
import * as url from "./url_methods";
import { getAuthUserHeader } from "helpers/localstorage/authentication";

// Get Status List
const getStatusList = (sort, order, limit, skip) =>
  get(url.GET_STATUS_LIST, {
    headers: {
      Authorization: getAuthUserHeader(),
    },
    params: {
      sort: sort,
      order: order,
      limit: limit,
      skip: skip,
    },
  });

// Get Status Data
const getStatusData = (status_id) =>
  get(url.GET_STATUS_DATA.replace("{status_id}", status_id), {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

// Add Status
const addStatus = (data) =>
  post(url.ADD_STATUS, data, {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

// Modify Status Data
const modifyStatusData = (status_id, data) =>
  put(url.MODIFY_STATUS_DATA.replace("{status_id}", status_id), data, {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

// Delete Status
const deleteStatus = (status_id) =>
  del(url.DELETE_STATUS.replace("{status_id}", status_id), {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

export {
  getStatusList,
  getStatusData,
  addStatus,
  modifyStatusData,
  deleteStatus,
};
