import { get } from "../axios.config";
import * as url from "./url_methods";

// Users List
const getUsersList = (sort, order, limit, skip) => get(url.USER_LIST,{
  params: {
    sort: sort,
    order: order,
    limit: limit,
    skip: skip,
  }
});

// User Data
const getUserData = (user_id) =>get(url.USER_DATA.replace("{user_id}", user_id));
  

export { getUsersList, getUserData };
