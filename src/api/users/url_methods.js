// Users List
export const USER_LIST = "/users/list"

// User Data
export const USER_DATA = "/users/data/{user_id}"