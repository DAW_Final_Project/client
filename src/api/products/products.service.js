import { get, post, put, del } from "../axios.config";
import * as url from "./url_methods";
import { getAuthUserHeader } from "helpers/localstorage/authentication";

// Get Products List
const getProductsList = (category_id, trademark_id, sort, order, limit, skip) =>
  get(url.GET_PRODUCTS_LIST.replace("{category_id}", category_id).replace("{trademark_id}", trademark_id), {
    headers: {
      Authorization: getAuthUserHeader(),
    },
    params: {
      sort: sort,
      order: order,
      limit: limit,
      skip: skip,
    },
  });

// Get Product Data
const getProductData = (product_id) =>
  get(url.GET_PRODUCT_DATA.replace("{product_id}", product_id), {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

// Add Product
const addProduct = (data) =>
  post(url.ADD_PRODUCT, data, {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

// Modify Product Data
const modifyProductData = (product_id, data) =>
  put(url.MODIFY_PRODUCT_DATA.replace("{product_id}", product_id), data, {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

// Delete Product
const deleteProduct = (product_id) =>
  del(url.DELETE_PRODUCT.replace("{product_id}", product_id), {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

export {
  getProductsList,
  getProductData,
  addProduct,
  modifyProductData,
  deleteProduct,
};
