// Get Products List
export const GET_PRODUCTS_LIST = "/products/list/{category_id}/{trademark_id}"

// Get Product Data
export const GET_PRODUCT_DATA = "/products/data/{product_id}"

// Add Product
export const ADD_PRODUCT = "/products/add"

// Modify Product Data
export const MODIFY_PRODUCT_DATA = "/products/modify/{product_id}"

// Delete Product
export const DELETE_PRODUCT = "/products/delete/{product_id}"