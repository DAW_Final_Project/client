import { get, post, put, del } from "../axios.config";
import * as url from "./url_methods";
import { getAuthUserHeader } from "helpers/localstorage/authentication";

// Get Materials List
const getMaterialsList = (sort, order, limit, skip) =>
  get(url.GET_MATERIALS_LIST, {
    headers: {
      Authorization: getAuthUserHeader(),
    },
    params: {
      sort: sort,
      order: order,
      limit: limit,
      skip: skip,
    },
  });

// Get Material Data
const getMaterialData = (material_id) =>
  get(url.GET_MATERIAL_DATA.replace("{material_id}", material_id), {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

// Add Material
const addMaterial = (data) =>
  post(url.ADD_MATERIAL, data, {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

// Modify Material Data
const modifyMaterialData = (material_id, data) =>
  put(url.MODIFY_MATERIAL_DATA.replace("{material_id}", material_id), data, {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

// Delete Material
const deleteMaterial = (material_id) =>
  del(url.DELETE_MATERIAL.replace("{material_id}", material_id), {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

export {
  getMaterialsList,
  getMaterialData,
  addMaterial,
  modifyMaterialData,
  deleteMaterial,
};
