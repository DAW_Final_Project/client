// Get Materials List
export const GET_MATERIALS_LIST = "/materials/list"

// Get Material Data
export const GET_MATERIAL_DATA = "/materials/data/{material_id}"

// Add Material
export const ADD_MATERIAL = "/materials/add"

// Modify Material Data
export const MODIFY_MATERIAL_DATA = "/materials/modify/{material_id}"

// Delete Material
export const DELETE_MATERIAL = "/materials/delete/{material_id}"