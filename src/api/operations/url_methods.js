// Get Operations List
export const GET_OPERATIONS_LIST = "/operations/list/{product_id}"

// Get Operation Data
export const GET_OPERATION_DATA = "/operations/data/{operation_id}"

// Add Operation
export const ADD_OPERATION = "/operations/add"

// Modify Operation Data
export const MODIFY_OPERATION_DATA = "/operations/modify/{operation_id}"

// Delete Operation
export const DELETE_OPERATION = "/operations/delete/{operation_id}"