import { get, post, put, del } from "../axios.config";
import * as url from "./url_methods";
import { getAuthUserHeader } from "helpers/localstorage/authentication";

// Get Operations List
const getOperationsList = (product_id, sort, order, limit, skip) =>
  get(url.GET_OPERATIONS_LIST.replace("{product_id}", product_id), {
    headers: {
      Authorization: getAuthUserHeader(),
    },
    params: {
      sort: sort,
      order: order,
      limit: limit,
      skip: skip,
    },
  });

// Get Operation Data
const getOperationData = (operation_id) =>
  get(url.GET_OPERATION_DATA.replace("{operation_id}", operation_id), {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

// Add Operation
const addOperation = (data) =>
  post(url.ADD_OPERATION, data, {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

// Modify Operation Data
const modifyOperationData = (operation_id, data) =>
  put(url.MODIFY_OPERATION_DATA.replace("{operation_id}", operation_id), data, {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

// Delete Operation
const deleteOperation = (operation_id) =>
  del(url.DELETE_OPERATION.replace("{operation_id}", operation_id), {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

export {
  getOperationsList,
  getOperationData,
  addOperation,
  modifyOperationData,
  deleteOperation,
};
