// Get Stores List
export const GET_STORES_LIST_FOR_LOGIN = "/stores/listlogin"

// Get Stores List
export const GET_STORES_LIST = "/stores/list"

// Get Store Data
export const GET_STORE_DATA = "/stores/data/{store_id}"

// Add Store
export const ADD_STORE = "/stores/add"

// Modify Store Data
export const MODIFY_STORE_DATA = "/stores/modify/{store_id}"

// Delete Store
export const DELETE_STORE = "/stores/delete/{store_id}"