import { get, post, put, del } from "../axios.config";
import * as url from "./url_methods";
import { getAuthUserHeader } from "helpers/localstorage/authentication";

// Get Stores List For Login
const getStoresListForLogin = (sort, order, limit, skip) =>
  get(url.GET_STORES_LIST_FOR_LOGIN, {
    params: {
      sort: sort,
      order: order,
      limit: limit,
      skip: skip,
    },
  });

// Get Stores List
const getStoresList = (sort, order, limit, skip) =>
  get(url.GET_STORES_LIST, {
    headers: {
      Authorization: getAuthUserHeader(),
    },
    params: {
      sort: sort,
      order: order,
      limit: limit,
      skip: skip,
    },
  });

// Get Store Data
const getStoreData = (store_id) =>
  get(url.GET_STORE_DATA.replace("{store_id}", store_id), {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

// Add Store
const addStore = (data) =>
  post(url.ADD_STORE, data, {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

// Modify Store Data
const modifyStoreData = (store_id, data) =>
  put(url.MODIFY_STORE_DATA.replace("{store_id}", store_id), data, {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

// Delete Store
const deleteStore = (store_id) =>
  del(url.DELETE_STORE.replace("{store_id}", store_id), {
    headers: {
      Authorization: getAuthUserHeader(),
    },
  });

export {
  getStoresListForLogin,
  getStoresList,
  getStoreData,
  addStore,
  modifyStoreData,
  deleteStore,
};
