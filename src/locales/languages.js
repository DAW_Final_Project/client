import enFlag from "../assets/images/flags/en.png"
import spFlag from "../assets/images/flags/sp.png"
import valFlag from "../assets/images/flags/val.png"
const languages = {
  en: {
    label: "English",
    flag: enFlag,
  },
  sp: {
    label: "Spanish",
    flag: spFlag,
  },
  val: {
    label: "Valencian",
    flag: valFlag,
  },
}

export default languages
