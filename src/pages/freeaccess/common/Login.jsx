import React, { useState, useEffect } from "react";
import {
  Container,
  Row,
  Col,
  Label,
  Button,
  FormGroup,
  Input,
  FormFeedback,
  Alert,
} from "reactstrap";

// Redux
import { connect, useDispatch } from "react-redux";

// Routes
import withRouter from "routes/js/withRouter";

// Actions
import { login, getStoresListForLogin } from "store/actions";

// Authentication
import * as authentication from "helpers/localstorage/authentication";

// md5
import md5 from "md5";

//i18n
import { withTranslation } from "react-i18next";

//Images
import Logo from "assets/images/logo.png";

const Login = (props) => {
  // Functions from props
  const { onLogin, onGetStoresListForLogin } = props.R_Functions;

  // States from props
  const { authenticationError } = props.RC_Authentication;
  const { storesList } = props.RC_Stores;

  // Local States
  const [store, setStore] = useState("-1");
  const [storeInvalid, setStoreInvalid] = useState(false);
  const [username, setUsername] = useState("");
  const [usernameInvalid, setUsernameInvalid] = useState(false);
  const [password, setPassword] = useState("");
  const [passwordInvalid, setPasswordInvalid] = useState(false);
  const [accessDenied, setAccessDenied] = useState(false);
  const dispatch = useDispatch();

  // UserEffects
  useEffect(() => {
    dispatch({ type: "RESET" });
    authentication.removeLoggedInUser();
    onGetStoresListForLogin("name", "asc", 0, 0);
  }, []);

  useEffect(() => {
    if (authenticationError != "") {
      setAccessDenied(true);
    } else {
      setAccessDenied(false);
    }
  }, [authenticationError]);

  // Handlers
  const handleChangeStore = (event) => {
    if (storeInvalid) {
      setStoreInvalid(false);
    }
    setStore(event.target.value);
  };
  const handleChangeUsername = (event) => {
    if (usernameInvalid) {
      setUsernameInvalid(false);
    }
    setUsername(event.target.value);
  };

  const handleChangePassword = (event) => {
    if (passwordInvalid) {
      setPasswordInvalid(false);
    }
    setPassword(event.target.value);
  };

  const formValidator = () => {
    let valid = true;
    if (store == "-1") {
      valid = false;
      setStoreInvalid(true);
    }
    if (username == "") {
      valid = false;
      setUsernameInvalid(true);
    }
    if (password == "") {
      valid = false;
      setPasswordInvalid(true);
    }
    return valid;
  };

  const handleLogin = async () => {
    if (formValidator()) {
      let data = {
        store: store,
        username: username,
        password: md5(password),
      };
      onLogin(data, props.router.navigate);
    }
  };

  const handleReset = () => {
    setStore("-1");
    setStoreInvalid(false);
    setUsername("");
    setUsernameInvalid(false);
    setPassword("");
    setPasswordInvalid(false);
    setAccessDenied(false);
  };

  // RENDER
  return (
    <Container style={{maxWidth:"220px"}}>
      <Row>
        <Col className="d-flex justify-content-center">
          <img src={Logo} height="200" weight="200" alt=""></img>
        </Col>
      </Row>
      <Row>
        <Col className="d-flex justify-content-center">
          <h1>MODFIX</h1>
        </Col>
      </Row>
      {accessDenied ? (
        <>
          <Alert color="danger" className="p-2">
            <p className="text-center mb-3" style={{fontSize: "1.2rem"}}><u>{props.t("accessDenied")}</u></p>
            <p>{props.t("dataNotValid")}</p>
            <p>{props.t("reviewAndRetry")}</p>
            </Alert>
          <Row>
            <Col className="d-flex justify-content-center">
              <Button
                color="primary"
                className="btn btn-rounded mt-5"
                size="sm"
                onClick={handleReset}
              >
                {props.t("retry")}
              </Button>
            </Col>
          </Row>
        </>
      ) : (
        <>
          <Row>
            <Col className="d-flex justify-content-center">
              <FormGroup>
                <Label>{props.t("store")}</Label>
                <Input
                  id="store"
                  description="store"
                  type="select"
                  style={{ width: "210px" }}
                  onChange={handleChangeStore}
                  invalid={storeInvalid}
                  value={store}
                >
                  <option hidden value="-1" key="selectStore">
                    {props.t("p_store")}
                  </option>
                  {storesList &&
                    storesList.map((element, index) => (
                      <option value={element._id} key={index}>
                        {element.name}
                      </option>
                    ))}
                </Input>
                <FormFeedback>{props.t("requiredFieldMessage")}</FormFeedback>
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col className="d-flex justify-content-center">
              <FormGroup>
                <Label>{props.t("username")}: </Label>
                <Input
                  name="username"
                  type="text"
                  style={{ width: "210px" }}
                  placeholder={props.t("p_username")}
                  onChange={handleChangeUsername}
                  invalid={usernameInvalid}
                />
                <FormFeedback>{props.t("requiredFieldMessage")}</FormFeedback>
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col className="d-flex justify-content-center">
              <FormGroup>
                <Label>{props.t("password")}: </Label>
                <Input
                  name="password"
                  type="password"
                  style={{ width: "210px" }}
                  placeholder={props.t("p_password")}
                  onChange={handleChangePassword}
                  invalid={passwordInvalid}
                />
                <FormFeedback>{props.t("requiredFieldMessage")}</FormFeedback>
              </FormGroup>
            </Col>
          </Row>

          <Row>
            <Col className="d-flex justify-content-center">
              <Button
                color="primary"
                className="btn btn-rounded"
                size="sm"
                onClick={handleLogin}
              >
                {props.t("login")}
              </Button>
            </Col>
          </Row>
        </>
      )}
    </Container>
  );
};
const mapStateToProps = (state) => {
  return {
    RC_Authentication: state.Authentication,
    RC_Stores: state.Stores,
  };
};

const mapDispatchToProps = (dispatch) => ({
  R_Functions: {
    onLogin: (data, navigate) => dispatch(login(data, navigate)),
    onGetStoresListForLogin: (sort, order, limit, skip) =>
      dispatch(getStoresListForLogin(sort, order, limit, skip)),
  },
});

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(withTranslation()(Login))
);
