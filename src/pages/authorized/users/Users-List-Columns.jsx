import React from "react";
import { Label, Button } from "reactstrap";

const UsersListColumns = (props, toggleModal) => {
  // RENDER
  return [
    {
      dataField: "name",
      text: `${props.t("user")}`,
      sort: true,
      headerStyle: (colum, colIndex) => {
        return { width: "60%" }
      },
      formatter: (cellContent, row) => (
        <Label>{`${row.name} ${row.surnames}`}</Label>
      ),
    },
    {
      dataField: "document",
      text: `${props.t("document")}`,
      sort: true,
      headerStyle: (colum, colIndex) => {
        return { width: "30%" }
      },
    },
    {
      dataField: "_id",
      text: `${props.t("actions")}`,
      sort: true,
      headerStyle: (colum, colIndex) => {
        return { width: "10%" }
      },
      formatter: (cellContent, row) => (
        
        <Button
        type="button"
        color="primary"
        size="sm"
        onClick={() => {
          toggleModal(cellContent)
        }}
      >
        VER
      </Button>
        
      ),
    },
  ];
};

export default UsersListColumns;
