import React, { useState, useEffect } from "react";
import {
  Card,
  CardHeader,
  CardBody,
  Row,
  Col,
  Form,
  FormGroup,
  FormFeedback,
  Input,
  Label,
  Button,
} from "reactstrap";

// Redux
import { connect } from "react-redux";

// Actions
import { addClient } from "store/actions";

// Routes
import withRouter from "routes/js/withRouter";

// i18n
import { withTranslation } from "react-i18next";

const ClientRegistry = (props) => {
  // Functions from props
  const { onAddClient } = props.R_Functions;

  //States from props

  // Local States
  const [name, setName] = useState("");
  const [nameInvalid, setNameInvalid] = useState(false);
  const [surnames, setSurnames] = useState("");
  const [surnamesInvalid, setSurnamesInvalid] = useState(false);
  const [document, setDocument] = useState("");
  const [documentInvalid, setDocumentInvalid] = useState(false);
  const [email, setEmail] = useState("");
  const [emailInvalid, setEmailInvalid] = useState(false);
  const [phone, setPhone] = useState("");
  const [phoneInvalid, setPhoneInvalid] = useState(false);
  const [address, setAddress] = useState("");
  const [addressInvalid, setAddressInvalid] = useState(false);
  const [city, setCity] = useState("");
  const [cityInvalid, setCityInvalid] = useState(false);
  const [postalCode, setPostalCode] = useState("");
  const [postalCodeInvalid, setPostalCodeInvalid] = useState(false);

  // UseEffects
  useEffect(() => {}, []);

  // Handlers
  const handleNameChange = (event) => {
    if (nameInvalid) {
      setNameInvalid(false);
    }
    setName(event.target.value);
  };
  const handleSurnamesChange = (event) => {
    if (surnamesInvalid) {
      setSurnamesInvalid(false);
    }
    setSurnames(event.target.value);
  };
  const handleDocumentChange = (event) => {
    if (documentInvalid) {
      setDocumentInvalid(false);
    }
    setDocument(event.target.value);
  };
  const handleEmailChange = (event) => {
    if (emailInvalid) {
      setEmailInvalid(false);
    }
    setEmail(event.target.value);
  };
  const handlePhoneChange = (event) => {
    if (phoneInvalid) {
      setPhoneInvalid(false);
    }
    setPhone(event.target.value);
  };
  const handleAddressChange = (event) => {
    if (addressInvalid) {
      setAddressInvalid(false);
    }
    setAddress(event.target.value);
  };
  const handleCityChange = (event) => {
    if (cityInvalid) {
      setCityInvalid(false);
    }
    setCity(event.target.value);
  };
  const handlePostalCodeChange = (event) => {
    if (postalCodeInvalid) {
      setPostalCodeInvalid(false);
    }
    setPostalCode(event.target.value);
  };

  const formValidator = () => {
    let valid = true;
    if (name == "") {
      valid = false;
      setNameInvalid(true);
    }
    if (surnames == "") {
      valid = false;
      setSurnamesInvalid(true);
    }
    if (document == "") {
      valid = false;
      setDocumentInvalid(true);
    }
    if (email == "") {
      valid = false;
      setEmailInvalid(true);
    }
    if (phone == "") {
      valid = false;
      setPhoneInvalid(true);
    }
    if (address == "") {
      valid = false;
      setAddressInvalid(true);
    }
    if (city == "") {
      valid = false;
      setCityInvalid(true);
    }
    if (postalCode == "") {
      valid = false;
      setPostalCodeInvalid(true);
    }
    return valid;
  };
  const HandleSave = (event, values) => {
    if (formValidator()) {
      let data = {
        name: name,
        surnames: surnames,
        document: document,
        email: email,
        phone: phone,
        address: {
          address: address,
          city: city,
          postalCode: postalCode,
        },
        servicios_ids:[]
      };
      onAddClient(data, props.router.navigate);
    }
  };
  const handleClear = () => {
    setName("");
    setSurnames("");
    setDocument("");
    setEmail("");
    setPhone("");
    setAddress("");
    setCity("");
    setPostalCode("");
  };

  return (
    <React.Fragment>
      <Card>
        <CardHeader className="bg-primary text-white">
          <Row>
            <Col>Registro de Clientes</Col>
          </Row>
        </CardHeader>
        <CardBody>
          <Form>
            <Row>
              <Col lg="4">
                <FormGroup>
                  <Label>{props.t("name")}</Label>
                  <Input
                    id="name"
                    name="name"
                    type="text"
                    placeholder={props.t("p_name")}
                    onChange={handleNameChange}
                    value={name}
                    required
                    invalid={nameInvalid}
                  />
                  <FormFeedback>{props.t("requiredFieldMessage")}</FormFeedback>
                </FormGroup>
              </Col>
              <Col lg="8">
                <FormGroup>
                  <Label>{props.t("surnames")}</Label>
                  <Input
                    id="surnames"
                    name="surnames"
                    type="text"
                    placeholder={props.t("p_surnames")}
                    onChange={handleSurnamesChange}
                    value={surnames}
                    required
                    invalid={surnamesInvalid}
                  />
                  <FormFeedback>{props.t("requiredFieldMessage")}</FormFeedback>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col lg="3">
                <FormGroup>
                  <Label>{props.t("document")}</Label>
                  <Input
                    id="document"
                    name="document"
                    type="text"
                    placeholder={props.t("p_document")}
                    onChange={handleDocumentChange}
                    value={document}
                    required
                    invalid={documentInvalid}
                  />
                  <FormFeedback>{props.t("requiredFieldMessage")}</FormFeedback>
                </FormGroup>
              </Col>
              <Col lg="3">
                <FormGroup>
                  <Label>{props.t("phone")}</Label>
                  <Input
                    id="phone"
                    name="phone"
                    type="text"
                    placeholder={props.t("p_phone")}
                    onChange={handlePhoneChange}
                    value={phone}
                    required
                    invalid={phoneInvalid}
                  />
                  <FormFeedback>{props.t("requiredFieldMessage")}</FormFeedback>
                </FormGroup>
              </Col>
              <Col lg="6">
                <FormGroup>
                  <Label>{props.t("email")}</Label>
                  <Input
                    id="email"
                    name="email"
                    type="text"
                    placeholder={props.t("p_email")}
                    onChange={handleEmailChange}
                    value={email}
                    required
                    invalid={emailInvalid}
                  />
                  <FormFeedback>{props.t("requiredFieldMessage")}</FormFeedback>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col lg="12">
                <FormGroup>
                  <Label>{props.t("address")}</Label>
                  <Input
                    id="address"
                    name="address"
                    type="text"
                    placeholder={props.t("p_address")}
                    onChange={handleAddressChange}
                    value={address}
                    required
                    invalid={addressInvalid}
                  />
                  <FormFeedback>{props.t("requiredFieldMessage")}</FormFeedback>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col lg="8">
                <FormGroup>
                  <Label>{props.t("city")}</Label>
                  <Input
                    id="city"
                    name="city"
                    type="text"
                    placeholder={props.t("p_city")}
                    onChange={handleCityChange}
                    value={city}
                    required
                    invalid={cityInvalid}
                  />
                  <FormFeedback>{props.t("requiredFieldMessage")}</FormFeedback>
                </FormGroup>
              </Col>
              <Col lg="4">
                <FormGroup>
                  <Label>{props.t("postalCode")}</Label>
                  <Input
                    id="postalCode"
                    name="postalCode"
                    type="text"
                    placeholder={props.t("p_postalCode")}
                    onChange={handlePostalCodeChange}
                    value={postalCode}
                    required
                    invalid={postalCodeInvalid}
                  />
                  <FormFeedback>{props.t("requiredFieldMessage")}</FormFeedback>
                </FormGroup>
              </Col>
            </Row>
          </Form>
          <Row>
            <Col className="d-flex justify-content-end">
              <Button
                type="button"
                color="primary"
                size="md"
                className="btn-rounded waves-effect waves-light m-1"
                onClick={handleClear}
              >
                {props.t("Clear")}
              </Button>
              <Button
                type="button"
                color="primary"
                size="md"
                className="btn-rounded waves-effect waves-light m-1"
                onClick={HandleSave}
              >
                {props.t("Save")}
              </Button>
            </Col>
          </Row>
        </CardBody>
      </Card>
    </React.Fragment>
  );
};

const mapStateToProps = (state) => {
  return {
    RC_Clients: state.Clients,
  };
};

const mapDispatchToProps = (dispatch) => ({
  R_Functions: {
    onAddClient: (data, navigate) => dispatch(addClient(data, navigate)),
  },
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(withTranslation()(ClientRegistry))
);
