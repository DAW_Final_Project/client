import React, { useState, useEffect } from "react";
import { Card, CardHeader, CardBody, Row, Col } from "reactstrap";

// Redux
import { connect } from "react-redux";

// Actions
import { getUsersList } from "../../../store/actions";

// Routes
import withRouter from "../../../routes/js/withRouter";

// table paginator
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";

// Components
import UsersListColumns from "./Users-List-Columns";
import UsersListModal from "./Users-List-Modal";

// i18n
import { withTranslation } from "react-i18next";


const UsersList = (props) => {
  // Functions from props
  const { onGetUsersList } = props.R_Functions;

  //States from props
  const { usersList, usersListTotal } = props.RC_Users;

  // Local States
  const [showModal, setShowModal] = useState(false)  
  const [user_id, setUser_id] = useState("");
  const [skip, setSkip] = useState(0);
  const [limit, setLimit] = useState(5);
  const [sort, setSort] = useState("name");
  const [order, setOrder] = useState("asc");
  const [page, setPage] = useState(0);
  const [sizePerPage, setSizePerPage] = useState(5);
  const [totalElements, setTotalElements] = useState(
    parseInt(usersListTotal)
  );

  // UseEffects
  useEffect(() => {
    onGetUsersList(sort, order, limit, skip, props.router.navigate);
  }, []);

  useEffect(() => {
    setTotalElements(parseInt(usersListTotal));
  }, [usersListTotal]);

  useEffect(() => {
    onGetUsersList(sort, order, limit, skip, props.router.navigate);
  }, [skip, limit, sort, order]);

  // Handlers
  const handleTableChange = (type, newState) => {
    switch (type) {
      case "sort":
        setSort(newState.sortField);
        setOrder(newState.sortOrder);
        break;
    }
  };

  const handlePageChange = (page, sizePerPage) => {
    setPage(page);
    setSkip(page * sizePerPage);
    setLimit(sizePerPage);
  };

  const handleSizePerPageChange = (sizePerPage, page) => {
    setPage(1);
    setSkip(0);
    setSizePerPage(sizePerPage);
    setLimit(sizePerPage);
  };


  // Toggles
  const toggleModal = (user_id) => {
    if (!showModal) {
      setUser_id(user_id)
    }
    setShowModal(!showModal)
  }

  // Pagination Tools
  const customTotal = (from, to, size) => {
    return (
      <span className="react-bootstrap-table-pagination-total">
        &nbsp; &nbsp; {props.t("showingFrom")}{" "}{from}{" "}{props.t("to")}{" "}{to}{" "}{props.t("of")}{" "}{size}{" "}{props.t("results")}
      </span>
    );
  };

  const pageOptions = {
    totalSize: totalElements,
    page: page,
    sizePerPage: sizePerPage,
    pageStartIndex: 0,
    paginationSize: 3,
    withFirstAndLast: true,
    alwaysShowAllBtns: true,
    firstPageText: "|<<",
    prePageText: "<<",
    nextPageText: ">>",
    lastPageText: ">>|",
    hideSizePerPage: false,
    disablePageTitle: true,
    hidePageListOnlyOnePage: true,
    showTotal: true,
    paginationTotalRenderer: customTotal,
    onPageChange: (page, sizePerPage) => {
      handlePageChange(page, sizePerPage);
    },
    onSizePerPageChange: (sizePerPage, page) => {
      handleSizePerPageChange(sizePerPage, page);
    },
    sizePerPageList: [
      {
        text: "5",
        value: 5,
      },
      {
        text: "10",
        value: 10,
      },
      {
        text: "ALL",
        value: totalElements,
      },
    ],
  };

  return (
    <React.Fragment>
      <Card>
        <CardHeader className="bg-primary text-white">
          <Row>
            <Col>Listado de Usuarios</Col>
          </Row>
        </CardHeader>
        <CardBody>
          <BootstrapTable
            keyField="name"
            data={usersList || []}
            columns={UsersListColumns(props, toggleModal)}
            remote={{
              sort: true,
              pagination: true,
            }}
            onTableChange={handleTableChange}
            pagination={paginationFactory(pageOptions)}
            responsive
            bordered={false}
            striped={false}
            classes={"table table-centered table-nowrap"}
            headerWrapperClasses={"thead-light"}
          />
        </CardBody>
      </Card>
      {showModal && (
        <UsersListModal
          isOpen={showModal}
          toggle={toggleModal}
          user_id={user_id}
        />
      )}
    </React.Fragment>
  );
};

const mapStateToProps = (state) => {
  return {
    RC_Users: state.Users,
  };
};

const mapDispatchToProps = (dispatch) => ({
  R_Functions: {
    onGetUsersList: (sort, order, limit, skip, navigate) =>
      dispatch(getUsersList(sort, order, limit, skip, navigate)),
  },
});

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(withTranslation()(UsersList))
);
