import React, { useState, useEffect } from "react";
import {
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row,
  Col,
  Label,
} from "reactstrap";

// Redux
import { connect } from "react-redux";

// Routes
import withRouter from "../../../routes/js/withRouter";

// Actions
import { getUserData } from "../../../store/actions";

const UserListModal = (props) => {
  // Functions from props
  const { onGetUserData } = props.R_Functions;
  // States from props
  const { isOpen, toggle, user_id } = props;
  const { userData } = props.RC_Users;

  // UseEffects
  useEffect(() => {
    onGetUserData(user_id, props.router.navigate);
  }, []);

  // RENDER
  return (
    <Modal
      isOpen={isOpen}
      role="dialog"
      autoFocus={true}
      centered={true}
      className="exampleModal"
      backdrop="static"
      tabIndex="-1"
      toggle={toggle}
    >
      <div className="modal-content">
        <ModalHeader toggle={toggle}>Detalles del Usuario</ModalHeader>
        <ModalBody>
        <Row>
        <Col>
          <p>
              <Label><b>Nombre</b></Label>
              {": "}
              <Label>{userData.name}</Label>
          </p>
          <p>
              <Label><b>Apellidos</b></Label>
              {": "}
              <Label>{userData.surnames}</Label>
          </p>
          <p>
              <Label><b>Documento</b></Label>
              {": "}
              <Label>{userData.document}</Label>
          </p>
         <p>
              <Label><b>Direccion</b></Label>
              {": "}
              {userData.address && 
              <Label>{`${userData.address.address} (${userData.address.postalCode} ${userData.address.city})`}</Label>
              }              
          </p>
         <p>
              <Label><b>Email</b></Label>
              {": "}
              <Label>{userData.email}</Label>
          </p>
         <p>
              <Label><b>Telefono</b></Label>
              {": "}
              <Label>{userData.phone}</Label>              
          </p>
        </Col>
      </Row>
        </ModalBody>
        <ModalFooter>
          <Button
            type="button"
            color="primary"
            size="sm"
            onClick={toggle}
          >
            Close
          </Button>
        </ModalFooter>
      </div>
    </Modal>
  );
};

const mapStateToProps = (state) => {
  return {
    RC_Users: state.Users,
  };
};

const mapDispatchToProps = (dispatch) => ({
  R_Functions: {
    onGetUserData: (navigate) => dispatch(getUserData(navigate)),
  },
});

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(UserListModal)
);
