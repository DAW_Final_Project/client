import React from "react";
import { Label, Button, Badge } from "reactstrap";

// React Router Dom
import { Link } from "react-router-dom";

// moment
import moment from "moment"
import "moment/locale/es"

const ServicesListColumns = (props, client_id, toggleModal, handleDelete) => {
  // RENDER
  return [
    {
      dataField: "created_at",
      text: `${props.t("registeredAt")}`,
      sort: true,
      headerStyle: (colum, colIndex) => {
        return { width: "20%" };
      },
      formatter: (cellContent, row) => (
        <Label>{moment(cellContent).format("DD/MM/YYYY, HH:mm")}</Label>
      ),
    },
    {
      dataField: "description",
      text: `${props.t("service")}`,
      sort: true,
      headerStyle: (colum, colIndex) => {
        return { width: "40%" };
      },
      formatter: (cellContent, row) => <Label>{`${cellContent}`}</Label>,
    },
    {
      dataField: "operations",
      text: `${props.t("operations")}`,
      sort: true,
      headerClasses:"text-center",
      headerStyle: (colum, colIndex) => {
        return { width: "15%" };
      },
      classes:"text-center",
      formatter: (cellContent, row) => {
      return (<Label>{`${cellContent}`}</Label>)
      },
    },
    {
      dataField: "status",
      text: `${props.t("status")}`,
      sort: true,
      headerClasses:"text-center",
      headerStyle: (colum, colIndex) => {
        return { width: "15%" };
      },
      classes:"text-center",
      formatter: (cellContent, row) => {
        let color = "danger";
        let text = "error";
        switch (cellContent) {
          case 1:
            color = "secondary";
            text = "received";
            break;
          case 2:
            color = "warning";
            text = "in progres";
            break;
          case 3:
            color = "success";
            text = "finished";
            break;
          default:
            color = "danger";
            text = "error";
            break;
        }

        return <Badge color={color} pill>{`${props.t(text)}`}</Badge>;
      },
    },
    {
      dataField: "_id",
      text: `${props.t("actions")}`,
      sort: true,
      headerStyle: (colum, colIndex) => {
        return { width: "15%" };
      },
      headerClasses: "text-center",
      classes: "text-center",
      formatter: (cellContent, row) => (
        <>
          {/* <a href="#">
            <i
              className="bx bx-show bx-sm m-1"
              onClick={() => {
                toggleModal(cellContent);
              }}
            />
          </a> */}
          <Link to={`/client/${client_id}/service/${cellContent}`}>
            <i className="bx bx-log-in bx-sm m-1" />
          </Link>
          {/* <a href="#">
            <i
              className="bx bx-trash bx-sm m-1"
              onClick={() => {
                handleDelete(cellContent);
              }}
            />
          </a> */}
        </>
      ),
    },
  ];
};

export default ServicesListColumns;
