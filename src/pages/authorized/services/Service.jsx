import React, { useState, useEffect } from "react";
import {
  Card,
  CardHeader,
  CardBody,
  Row,
  Col,
  Label,
  FormGroup,
  Input,
  Button,
} from "reactstrap";

// Redux
import { connect } from "react-redux";

// Routes
import withRouter from "routes/js/withRouter";
import { Link } from "react-router-dom";

// Actions
import {
  getServiceData,
  getClientData,
  getStoreData,
  getProductData,
  getTrademarkData,
} from "store/actions";

// Components
import ServiceOperationsList from "./Service-Operations_List";
import PdfViewer from "components/pdf/pdf_viewer";

// i18n
import { withTranslation } from "react-i18next";
import operations from "store/operations/reducer";

const ServiceData = (props) => {
  // Functions from props
  const {
    onGetServiceData,
    onGetClientData,
    onGetStoreData,
    onGetProductData,
    onGetTrademarkData,
  } = props.R_Functions;

  // States from props
  const { client_id, service_id } = props.router.params;
  const { serviceData } = props.RC_Services;
  const { clientData } = props.RC_Clients;
  const { storeData } = props.RC_Stores;
  const { productData } = props.RC_Products;
  const { trademarkData } = props.RC_Trademarks;

  // Local States
  const [showStore, setShowStore] = useState(false);
  const [showClient, setShowClient] = useState(false);
  const [showProduct, setShowProduct] = useState(false);
  const [showButton, setShowButton] = useState(false);
  const [showPdf, setShowPdf] = useState(false);

  // UseEffects
  useEffect(() => {
    onGetServiceData(service_id, props.router.navigate);
    onGetClientData(client_id, props.router.navigate);
  }, []);

  useEffect(() => {
    if (serviceData && serviceData.store_id) {
      onGetStoreData(serviceData.store_id, props.router.navigate);
    }
    if (serviceData && serviceData.product_id) {
      onGetProductData(serviceData.product_id, props.router.navigate);
    }
    if (
      serviceData &&
      serviceData.operations &&
      serviceData.operations.length > 0
    ) {
      let temp = true;
      for (let i = 0; i < serviceData.operations.length; i++) {
        if (serviceData.operations[i].status != 3) {
          temp = false;
        }
        setShowButton(temp);
      }
    }
  }, [serviceData]);

  useEffect(() => {
    if (productData && productData.trademark_id) {
      onGetTrademarkData(productData.trademark_id, props.router.navigate);
    }
  }, [productData]);

  // Toggles
  const toggleStore = () => {
    setShowStore(!showStore);
  };
  const toggleClient = () => {
    setShowClient(!showClient);
  };
  const toggleProduct = () => {
    setShowProduct(!showProduct);
  };
  const togglePdf = () => {
    setShowPdf(!showPdf);
  };

  // Render
  return (
    <React.Fragment>
      <Card>
        <CardHeader className="bg-primary text-white">
          <Row>
            <Col lg="10">
              <h4>
                {clientData.name} {clientData.surnames} {"-"}{" "}
                {serviceData.description}
              </h4>
            </Col>
            <Col lg="1" className="text-end">
              {showButton && (
                <Button onClick={togglePdf} className="p-0" style={{border:"none",backgroundColor:"transparent"}}>
                  {showPdf ? <span>DATA</span> : <span>PDF</span>}
                </Button>
              )}
            </Col>
            <Col lg="1" className="text-end">
              <Link to={`/client/${clientData._id}`} className="text-white">
                <i className="bx bx-log-out bx-sm m-1" />
              </Link>
            </Col>
          </Row>
        </CardHeader>
        {showPdf ? (
          <CardBody>
            <Row>
              <Col className="d-flex justify-content-center">
                <PdfViewer
                  serviceData={serviceData}
                  clientData={clientData}
                  storeData={storeData}
                  productData={productData}
                  trademarkData={trademarkData}
                />
              </Col>
            </Row>
          </CardBody>
        ) : (
          <CardBody>
            <Row>
              <Col>
                <Card>
                  {showStore ? (
                    <>
                      <CardHeader>
                        <Row>
                          <Col lg="11">{props.t("store")}</Col>
                          <Col lg="1" className="d-flex justify-content-end">
                            <a href="#">
                              <i
                                className="bx bxs-up-arrow bx-sm m-1"
                                onClick={() => {
                                  toggleStore();
                                }}
                              />
                            </a>
                          </Col>
                        </Row>
                      </CardHeader>
                      <CardBody>
                        <Row>
                          <Col lg="4">
                            <FormGroup>
                              <Label>{props.t("store")}</Label>
                              <Input
                                type="text"
                                value={
                                  storeData && storeData.name
                                    ? storeData.name
                                    : ""
                                }
                                disabled={true}
                              />
                            </FormGroup>
                          </Col>
                          <Col lg="6">
                            <FormGroup>
                              <Label>{props.t("email")}</Label>
                              <Input
                                type="text"
                                value={
                                  storeData && storeData.email
                                    ? storeData.email
                                    : ""
                                }
                                disabled={true}
                              />
                            </FormGroup>
                          </Col>
                          <Col lg="2">
                            <FormGroup>
                              <Label>{props.t("phone")}</Label>
                              <Input
                                type="text"
                                value={
                                  storeData && storeData.phone
                                    ? storeData.phone
                                    : ""
                                }
                                disabled={true}
                              />
                            </FormGroup>
                          </Col>
                        </Row>
                        <Row>
                          <Col lg="7">
                            <FormGroup>
                              <Label>{props.t("address")}</Label>
                              <Input
                                type="text"
                                value={
                                  storeData &&
                                  storeData.address &&
                                  storeData.address.address
                                    ? storeData.address.address
                                    : ""
                                }
                                disabled={true}
                              />
                            </FormGroup>
                          </Col>
                          <Col lg="3">
                            <FormGroup>
                              <Label>{props.t("city")}</Label>
                              <Input
                                type="text"
                                value={
                                  storeData &&
                                  storeData.address &&
                                  storeData.address.city
                                    ? storeData.address.city
                                    : ""
                                }
                                disabled={true}
                              />
                            </FormGroup>
                          </Col>
                          <Col lg="2">
                            <FormGroup>
                              <Label>{props.t("postalCode")}</Label>
                              <Input
                                type="text"
                                value={
                                  storeData &&
                                  storeData.address &&
                                  storeData.address.postalCode
                                    ? storeData.address.postalCode
                                    : ""
                                }
                                disabled={true}
                              />
                            </FormGroup>
                          </Col>
                        </Row>
                      </CardBody>
                    </>
                  ) : (
                    <CardHeader>
                      <Row>
                        <Col lg="2">{props.t("store")}:</Col>
                        <Col lg="9" className="text-center">
                          {storeData && storeData.name ? (
                            <b>{storeData.name}</b>
                          ) : (
                            ""
                          )}
                        </Col>
                        <Col lg="1" className="d-flex justify-content-end">
                          <a href="#">
                            <i
                              className="bx bxs-down-arrow bx-sm m-1"
                              onClick={() => {
                                toggleStore();
                              }}
                            />
                          </a>
                        </Col>
                      </Row>
                    </CardHeader>
                  )}
                </Card>
              </Col>
            </Row>
            <br />
            <Row>
              <Col>
                <Card>
                  {showClient ? (
                    <>
                      <CardHeader>
                        <Row>
                          <Col lg="11">{props.t("client")}</Col>
                          <Col lg="1" className="d-flex justify-content-end">
                            <a href="#">
                              <i
                                className="bx bxs-up-arrow bx-sm m-1"
                                onClick={() => {
                                  toggleClient();
                                }}
                              />
                            </a>
                          </Col>
                        </Row>
                      </CardHeader>
                      <CardBody>
                        <Row>
                          <Col lg="4">
                            <FormGroup>
                              <Label>{props.t("name")}</Label>
                              <Input
                                id="name"
                                name="name"
                                type="text"
                                value={clientData.name ? clientData.name : ""}
                                disabled={true}
                              />
                            </FormGroup>
                          </Col>

                          <Col lg="8">
                            <FormGroup>
                              <Label>{props.t("surnames")}</Label>
                              <Input
                                id="surnames"
                                name="surnames"
                                type="text"
                                value={
                                  clientData.surnames ? clientData.surnames : ""
                                }
                                disabled={true}
                              />
                            </FormGroup>
                          </Col>
                        </Row>
                        <Row>
                          <Col lg="3">
                            <FormGroup>
                              <Label>{props.t("document")}</Label>
                              <Input
                                id="document"
                                name="document"
                                type="text"
                                value={
                                  clientData.document ? clientData.document : ""
                                }
                                disabled={true}
                              />
                            </FormGroup>
                          </Col>
                          <Col lg="3">
                            <FormGroup>
                              <Label>{props.t("phone")}</Label>
                              <Input
                                id="phone"
                                name="phone"
                                type="text"
                                value={clientData.phone ? clientData.phone : ""}
                                disabled={true}
                              />
                            </FormGroup>
                          </Col>
                          <Col lg="6">
                            <FormGroup>
                              <Label>{props.t("email")}</Label>
                              <Input
                                id="email"
                                name="email"
                                type="text"
                                value={clientData.email ? clientData.email : ""}
                                disabled={true}
                              />
                            </FormGroup>
                          </Col>
                        </Row>
                        <Row>
                          <Col lg="12">
                            <FormGroup>
                              <Label>{props.t("address")}</Label>
                              <Input
                                id="address"
                                name="address"
                                type="text"
                                value={
                                  clientData.address &&
                                  clientData.address.address
                                    ? clientData.address.address
                                    : ""
                                }
                                disabled={true}
                              />
                            </FormGroup>
                          </Col>
                        </Row>
                        <Row>
                          <Col lg="8">
                            <FormGroup>
                              <Label>{props.t("city")}</Label>
                              <Input
                                id="city"
                                name="city"
                                type="text"
                                value={
                                  clientData.address && clientData.address.city
                                    ? clientData.address.city
                                    : ""
                                }
                                disabled={true}
                              />
                            </FormGroup>
                          </Col>
                          <Col lg="4">
                            <FormGroup>
                              <Label>{props.t("postalCode")}</Label>
                              <Input
                                id="postalCode"
                                name="postalCode"
                                type="text"
                                value={
                                  clientData.address &&
                                  clientData.address.postalCode
                                    ? clientData.address.postalCode
                                    : ""
                                }
                                disabled={true}
                              />
                            </FormGroup>
                          </Col>
                        </Row>
                      </CardBody>
                    </>
                  ) : (
                    <CardHeader>
                      <Row>
                        <Col lg="2">{props.t("client")}:</Col>
                        <Col lg="9" className="text-center">
                          {clientData.name ? (
                            <b>{`${clientData.name} ${clientData.surnames}`}</b>
                          ) : (
                            ""
                          )}
                        </Col>
                        <Col lg="1" className="d-flex justify-content-end">
                          <a href="#">
                            <i
                              className="bx bxs-down-arrow bx-sm m-1"
                              onClick={() => {
                                toggleClient();
                              }}
                            />
                          </a>
                        </Col>
                      </Row>
                    </CardHeader>
                  )}
                </Card>
              </Col>
            </Row>
            <br />
            <Row>
              <Col>
                <Card>
                  {showProduct ? (
                    <>
                      <CardHeader>
                        <Row>
                          <Col lg="11">{props.t("product")}</Col>
                          <Col lg="1" className="d-flex justify-content-end">
                            <a href="#">
                              <i
                                className="bx bxs-up-arrow bx-sm m-1"
                                onClick={() => {
                                  toggleProduct();
                                }}
                              />
                            </a>
                          </Col>
                        </Row>
                      </CardHeader>
                      <CardBody>
                        <Row>
                          <Col lg="4">
                            <FormGroup>
                              <Label>{props.t("product")}</Label>
                              <Input
                                type="text"
                                value={
                                  productData && productData.alias
                                    ? productData.alias
                                    : ""
                                }
                                disabled={true}
                              />
                            </FormGroup>
                          </Col>
                          <Col lg="4">
                            <FormGroup>
                              <Label>{props.t("trademark")}</Label>
                              <Input
                                type="text"
                                value={
                                  trademarkData && trademarkData.trademark
                                    ? trademarkData.trademark
                                    : ""
                                }
                                disabled={true}
                              />
                            </FormGroup>
                          </Col>
                          <Col lg="4">
                            <FormGroup>
                              <Label>{props.t("model")}</Label>
                              <Input
                                type="text"
                                value={
                                  productData && productData.model
                                    ? productData.model
                                    : ""
                                }
                                disabled={true}
                              />
                            </FormGroup>
                          </Col>
                        </Row>
                      </CardBody>
                    </>
                  ) : (
                    <CardHeader>
                      <Row>
                        <Col lg="2">{props.t("product")}:</Col>
                        <Col lg="9" className="text-center">
                          {productData && productData.alias ? (
                            <b>{productData.alias}</b>
                          ) : (
                            ""
                          )}
                        </Col>
                        <Col lg="1" className="d-flex justify-content-end">
                          <a href="#">
                            <i
                              className="bx bxs-down-arrow bx-sm m-1"
                              onClick={() => {
                                toggleProduct();
                              }}
                            />
                          </a>
                        </Col>
                      </Row>
                    </CardHeader>
                  )}
                </Card>
              </Col>
            </Row>
            <br />
            <Row>
              <Col>
                {serviceData && serviceData.operations && (
                  <ServiceOperationsList
                    operationsList={serviceData.operations}
                  />
                )}
              </Col>
            </Row>
          </CardBody>
        )}
      </Card>
    </React.Fragment>
  );
};
const mapStateToProps = (state) => {
  return {
    RC_Services: state.Services,
    RC_Clients: state.Clients,
    RC_Stores: state.Stores,
    RC_Products: state.Products,
    RC_Trademarks: state.Trademarks,
  };
};

const mapDispatchToProps = (dispatch) => ({
  R_Functions: {
    onGetServiceData: (service_id, navigate) =>
      dispatch(getServiceData(service_id, navigate)),
    onGetClientData: (client_id, navigate) =>
      dispatch(getClientData(client_id, navigate)),
    onGetStoreData: (store_id, navigate) =>
      dispatch(getStoreData(store_id, navigate)),
    onGetProductData: (product_id, navigate) =>
      dispatch(getProductData(product_id, navigate)),
    onGetTrademarkData: (trademark_id, navigate) =>
      dispatch(getTrademarkData(trademark_id, navigate)),
  },
});

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(withTranslation()(ServiceData))
);
