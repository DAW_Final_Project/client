import React, { useState, useEffect } from "react";
import { Card, CardHeader, CardBody, Row, Col } from "reactstrap";

// Redux
import { connect } from "react-redux";

// Actions
import { getServicesList, deleteService } from "store/actions";

// Routes
import withRouter from "routes/js/withRouter";

// table paginator
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";

// Components
import OperationsListColumns from "./Service-Operations-List-Columns";

// i18n
import { withTranslation } from "react-i18next";

const ServicesList = (props) => {
  //States from props
  const { operationsList } = props;

  // Pagination Tools
  const customTotal = (from, to, size) => {
    return (
      <span className="react-bootstrap-table-pagination-total">
        &nbsp; &nbsp; {props.t("showingFrom")} {from} {props.t("to")} {to}{" "}
        {props.t("of")} {size} {props.t("results")}
      </span>
    );
  };

  const pageOptions = {
    sizePerPage: 5,
    pageStartIndex: 1,
    paginationSize: 5,
    withFirstAndLast: true,
    alwaysShowAllBtns: true,
    firstPageText: "|<<",
    prePageText: "<<",
    nextPageText: ">>",
    lastPageText: ">>|",
    hideSizePerPage: false,
    disablePageTitle: true,
    hidePageListOnlyOnePage: true,
    showTotal: true,
    sizePerPageList: [
      {
        text: "5",
        value: 5,
      },
      {
        text: "10",
        value: 10,
      },
      {
        text: "All",
        value: operationsList.length,
      },
    ],
  };

  return (
    <React.Fragment>
      <Card>
        <CardHeader>
          <Row>
            <Col lg="12">{props.t("serviceOperationsList")}</Col>
          </Row>
        </CardHeader>
        <CardBody>
          <BootstrapTable
            keyField="_id"
            data={operationsList || []}
            columns={OperationsListColumns(props)}
            pagination={operationsList.length>5 ? paginationFactory(pageOptions):null}
            responsive
            bordered={false}
            striped={false}
            classes={"table table-centered table-nowrap"}
            headerWrapperClasses={"thead-light"}
          />
        </CardBody>
      </Card>
    </React.Fragment>
  );
};

export default withRouter(withTranslation()(ServicesList));
