import React, { useState, useEffect } from "react";
import {
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row,
  Col,
  Form,
  FormGroup,
  FormFeedback,
  Input,
  Label,
} from "reactstrap";

// Redux
import { connect } from "react-redux";

// Routes
import withRouter from "routes/js/withRouter";

// Actions
import { getServiceData, modifyServiceData } from "store/actions";

// i18n
import { withTranslation } from "react-i18next";

// Helpers
import sleep from "helpers/functions/sleep"

const ServiceOperationsListModal = (props) => {
  // Functions from props
  const { onGetServiceData, onModifyServiceData } = props.R_Functions;
  // States from props
  const { isOpen, toggle, product_id, product } = props;
  const { serviceData } = props.RC_Services;

  // Local States

  // UseEffects
  useEffect(() => {
    
  }, []);

  // Handlers
  const handleNameChange = (event) => {
    if (nameInvalid) {
      setNameInvalid(false);
    }
    setName(event.target.value);
  };

  const formValidator = () => {
    let valid = true;
    
    if (surnames == "") {
      valid = false;
      setSurnamesInvalid(true);
    }

    return valid;
  };
  const handleSave = async (event, values) => {
    if (formValidator()) {
      await sleep(500)
      onGetServiceData(service_id, props.router.navigate);
      toggleEdit();
    }
  };
  const handleClear = () => {
  };

  // Toggles
  
  // RENDER
  return (
    <Modal
      isOpen={isOpen}
      role="dialog"
      autoFocus={true}
      centered={true}
      className="exampleModal"
      backdrop="static"
      tabIndex="-1"
      toggle={toggle}
      size="lg"
    >
      <div className="modal-content">
        <ModalHeader toggle={toggle}>Añadir operacion</ModalHeader>
        <ModalBody>
         
        </ModalBody>
        <ModalFooter>
              <Button type="button" color="primary" size="sm" onClick={handleSave}>
                {props.t("Save")}
              </Button>
              <Button
                type="button"
                color="primary"
                size="sm"
                onClick={toggleEdit}
              >
                {props.t("Cancel")}
              </Button>
        </ModalFooter>
      </div>
    </Modal>
  );
};

const mapStateToProps = (state) => {
  return {
    RC_Services: state.Services,
  };
};

const mapDispatchToProps = (dispatch) => ({
  R_Functions: {
    onGetServiceData: (navigate) => dispatch(getServiceData(navigate)),
    onModifyServiceData: (service_id, data, navigate) =>
      dispatch(modifyServiceData(service_id, data, navigate)),
  },
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(withTranslation()(ServiceOperationsListModal))
);
