import React, { useState, useEffect } from "react";
import { Card, CardHeader, CardBody, Row, Col } from "reactstrap";

// Redux
import { connect } from "react-redux";

// Actions
import { getServicesList, deleteService } from "store/actions";

// Routes
import withRouter from "routes/js/withRouter";

// table paginator
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";

// Components
import ServicesListColumns from "./Services-List-Columns";
import ServicesListModal from "./Services-List-Modal";

// i18n
import { withTranslation } from "react-i18next";

const ServicesList = (props) => {
  // Functions from props
  const { onGetServicesList, onDeleteService } = props.R_Functions;

  //States from props
  const { servicesList, servicesListTotal } = props.RC_Services;
  const { client_id } = props.router.params;

  // Local States
  const [showModal, setShowModal] = useState(false);
  const [service_id, setService_id] = useState("");
  const [skip, setSkip] = useState(0);
  const [limit, setLimit] = useState(5);
  const [sort, setSort] = useState("description");
  const [order, setOrder] = useState("asc");
  const [page, setPage] = useState(0);
  const [sizePerPage, setSizePerPage] = useState(5);
  const [totalElements, setTotalElements] = useState(
    parseInt(servicesListTotal)
  );

  // UseEffects
  useEffect(() => {
    onGetServicesList(
      client_id,
      sort,
      order,
      limit,
      skip,
      props.router.navigate
    );
  }, []);

  useEffect(() => {
    setTotalElements(parseInt(servicesListTotal));
  }, [servicesListTotal]);

  useEffect(() => {
    onGetServicesList(
      client_id,
      sort,
      order,
      limit,
      skip,
      props.router.navigate
    );
  }, [skip, limit, sort, order]);

  // Handlers
  const handleTableChange = (type, newState) => {
    switch (type) {
      case "sort":
        setSort(newState.sortField);
        setOrder(newState.sortOrder);
        break;
    }
  };

  const handlePageChange = (page, sizePerPage) => {
    setPage(page);
    setSkip(page * sizePerPage);
    setLimit(sizePerPage);
  };

  const handleSizePerPageChange = (sizePerPage, page) => {
    setPage(1);
    setSkip(0);
    setSizePerPage(sizePerPage);
    setLimit(sizePerPage);
  };

  const handleDelete = (service_id) => {
    if (window.confirm(props.t("registryDeleteAlert"))) {
      onDeleteService(service_id, props.router.navigate);
    }
  };

  // Toggles
  const toggleModal = (service_id) => {
    if (!showModal) {
      setService_id(service_id);
    } else {
      onGetServicesList(
        client_id,
        sort,
        order,
        limit,
        skip,
        props.router.navigate
      );
    }
    setShowModal(!showModal);
  };

  // Pagination Tools
  const customTotal = (from, to, size) => {
    return (
      <span className="react-bootstrap-table-pagination-total">
        &nbsp; &nbsp; {props.t("showingFrom")} {from} {props.t("to")} {to}{" "}
        {props.t("of")} {size} {props.t("results")}
      </span>
    );
  };

  const pageOptions = {
    totalSize: totalElements,
    page: page,
    sizePerPage: sizePerPage,
    pageStartIndex: 0,
    paginationSize: 3,
    withFirstAndLast: true,
    alwaysShowAllBtns: true,
    firstPageText: "|<<",
    prePageText: "<<",
    nextPageText: ">>",
    lastPageText: ">>|",
    hideSizePerPage: false,
    disablePageTitle: true,
    hidePageListOnlyOnePage: true,
    showTotal: true,
    paginationTotalRenderer: customTotal,
    onPageChange: (page, sizePerPage) => {
      handlePageChange(page, sizePerPage);
    },
    onSizePerPageChange: (sizePerPage, page) => {
      handleSizePerPageChange(sizePerPage, page);
    },
    sizePerPageList: [
      {
        text: "5",
        value: 5,
      },
      {
        text: "10",
        value: 10,
      },
      {
        text: "ALL",
        value: totalElements,
      },
    ],
  };

  return (
    <React.Fragment>
      <Card>
        <CardHeader className="bg-primary text-white">
          <Row>
            <Col>{props.t("servicesList")}</Col>
          </Row>
        </CardHeader>
        <CardBody>
          <BootstrapTable
            keyField="_id"
            data={servicesList || []}
            columns={ServicesListColumns(
              props,
              client_id,
              toggleModal,
              handleDelete
            )}
            remote={{
              sort: true,
              pagination: true,
            }}
            onTableChange={handleTableChange}
            pagination={
              totalElements > 5 ? paginationFactory(pageOptions) : null
            }
            responsive
            bordered={false}
            striped={false}
            classes={"table table-centered table-nowrap"}
            headerWrapperClasses={"thead-light"}
          />
        </CardBody>
      </Card>
      {showModal && (
        <ServicesListModal
          isOpen={showModal}
          toggle={toggleModal}
          service_id={service_id}
        />
      )}
    </React.Fragment>
  );
};

const mapStateToProps = (state) => {
  return {
    RC_Services: state.Services,
  };
};

const mapDispatchToProps = (dispatch) => ({
  R_Functions: {
    onGetServicesList: (sort, order, limit, skip, navigate) =>
      dispatch(getServicesList(sort, order, limit, skip, navigate)),
    onDeleteService: (service_id, navigate) =>
      dispatch(deleteService(service_id, navigate)),
  },
});

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(withTranslation()(ServicesList))
);
