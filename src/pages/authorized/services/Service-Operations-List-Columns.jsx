import React from "react";
import { Label, Badge } from "reactstrap";

// moment
import moment from "moment";
import "moment/locale/es";

const ServicesListColumns = (props) => {
  // RENDER
  return [
    {
      dataField: "alias",
      text: `${props.t("operation")}`,
      sort: true,
      headerStyle: (colum, colIndex) => {
        return { width: "45%" };
      },
      formatter: (cellContent, row) => <Label>{`${cellContent}`}</Label>,
    },
    {
      dataField: "status",
      text: `${props.t("status")}`,
      sort: true,
      headerClasses: "text-center",
      headerStyle: (colum, colIndex) => {
        return { width: "15%" };
      },
      classes: "text-center",
      formatter: (cellContent, row) => {
        let color = "danger";
        let text = "error";
        switch (cellContent) {
          case 1:
            color = "secondary";
            text = "received";
            break;
          case 2:
            color = "warning";
            text = "in progres";
            break;
          case 3:
            color = "success";
            text = "finished";
            break;
          default:
            color = "danger";
            text = "error";
            break;
        }

        return <Badge color={color} pill>{`${props.t(text)}`}</Badge>;
      },
    },
    {
      dataField: "started_at",
      text: `${props.t("asigned")}`,
      sort: true,
      headerStyle: (colum, colIndex) => {
        return { width: "20%" };
      },
      formatter: (cellContent, row) => (
        <>
          {row.status == 2 || row.status == 3 ? (
            <Label>{moment(cellContent).format("DD/MM/YYYY, HH:mm")}</Label>
          ) : (
            <></>
          )}
        </>
      ),
    },
    {
      dataField: "finished_at",
      text: `${props.t("finished")}`,
      sort: true,
      headerStyle: (colum, colIndex) => {
        return { width: "20%" };
      },
      formatter: (cellContent, row) => (
        <>
          {row.status == 3 ? (
            <Label>{moment(cellContent).format("DD/MM/YYYY, HH:mm")}</Label>
          ) : (
            <></>
          )}
        </>
      ),
    },
  ];
};

export default ServicesListColumns;
