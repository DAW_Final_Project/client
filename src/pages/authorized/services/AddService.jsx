import React, { useState, useEffect } from "react";
import {
  Card,
  CardHeader,
  CardBody,
  Row,
  Col,
  Form,
  FormGroup,
  FormFeedback,
  Input,
  Label,
  Button,
  ListGroup,
  ListGroupItem,
} from "reactstrap";

// Redux
import { connect } from "react-redux";

// Actions
import {
  addService,
  getProductsCategoriesList,
  getTrademarksList,
  getProductsList,
  getOperationsList,
} from "store/actions";

// Routes
import withRouter from "routes/js/withRouter";

// Helpers
import sleep from "helpers/functions/sleep";

// Authentication
import { getLoggedInUser } from "helpers/localstorage/authentication";

// i18n
import { withTranslation } from "react-i18next";

const ServiceRegistry = (props) => {
  // Functions from props
  const {
    onAddService,
    onGetProductsCategoriesList,
    onGetTrademarksList,
    onGetProductsList,
    onGetOperationsList,
  } = props.R_Functions;

  const refreshList = props.refreshList;

  //States from props
  const { operationsList } = props.RC_Operations;
  const { productscategoriesList } = props.RC_ProductsCategories;
  const { trademarksList } = props.RC_Trademarks;
  const { productsList } = props.RC_Products;

  // Local States
  const [description, setDescription] = useState("");
  const [descriptionInvalid, setDescriptionInvalid] = useState(false);
  const [category, setCategory] = useState("-1");
  const [categoryInvalid, setCategoryInvalid] = useState(false);
  const [trademark, setTrademark] = useState("-1");
  const [trademarkInvalid, setTrademarkInvalid] = useState(false);
  const [product, setProduct] = useState("-1");
  const [productInvalid, setProductInvalid] = useState(false);
  const [operation, setOperation] = useState("-1");
  const [operationInvalid, setOperationInvalid] = useState(false);
  const [operations, setOperations] = useState([]);
  const [operationsInvalid, setOperationsInvalid] = useState(false);
  const user = getLoggedInUser();

  // UseEffects
  useEffect(() => {
    onGetProductsCategoriesList("category", "asc", 0, 0, props.router.navigate);
    onGetTrademarksList("trademark", "asc", 0, 0, props.router.navigate);
  }, []);

  useEffect(() => {
    if (category != "-1" && trademark != "-1") {
      onGetProductsList(
        category,
        trademark,
        "alias",
        "asc",
        0,
        0,
        props.router.navigate
      );
    }
  }, [category, trademark]);

  useEffect(() => {
    if (product != "-1") {
      onGetOperationsList(product, "alias", "asc", 0, 0, props.router.navigate);
    }
  }, [product]);

  // Handlers
  const handleDescriptionChange = (event) => {
    if (descriptionInvalid) {
      setDescriptionInvalid(false);
    }
    setDescription(event.target.value);
  };
  const handleCategoryChange = (event) => {
    if (categoryInvalid) {
      setCategoryInvalid(false);
    }
    setCategory(event.target.value);
    setProduct("-1");
  };

  const handleTrademarkChange = (event) => {
    if (trademarkInvalid) {
      setTrademarkInvalid(false);
    }
    setTrademark(event.target.value);
    setProduct("-1");
  };

  const handleProductChange = (event) => {
    if (productInvalid) {
      setProductInvalid(false);
    }
    setProduct(event.target.value);
  };
  const handleOperationChange = (event) => {
    if (operationInvalid) {
      setOperationInvalid(false);
    }
    setOperation(event.target.value);
  };
  const handleAddOperation = () => {
    if (operation != "-1") {
      let temp = operations;
      temp.push(JSON.parse(operation));
      setOperations(temp);
      setOperation("-1");
    }
  };

  const formValidator = () => {
    let valid = true;
    if (description == "") {
      valid = false;
      setDescriptionInvalid(true);
    }
    if (category == "-1") {
      valid = false;
      setCategoryInvalid(true);
    }
    if (trademark == "-1") {
      valid = false;
      setTrademarkInvalid(true);
    }
    if (product == "-1") {
      valid = false;
      setProductInvalid(true);
    }
    if (operations.length < 1) {
      valid = false;
      setOperationInvalid(true);
    }
    return valid;
  };

  const HandleSave = async (event, values) => {
    if (formValidator()) {
      let operaciones = [];
      for (let i = 0; i < operations.length; i++) {
        let operacion = {
          created_at:Date.now(),
          operation_id: operations[i]._id,
          started_at:null,
          ended_at:null,
          user_id:null
        };
        operaciones.push(operacion);
      }
      let data = {
        description: description,
        client_id: props.router.params.client_id,
        product_id: product,
        store_id: user.store,
        operations: operaciones,
      };
      onAddService(data, props.router.navigate);
      handleClear();
      await sleep(500);
      refreshList();
    }
  };
  const handleClear = () => {
    setDescription("");
    setDescriptionInvalid(false);
    setCategory("-1");
    setCategoryInvalid(false);
    setTrademark("-1");
    setTrademarkInvalid(false);
    setProduct("-1");
    setProductInvalid(false);
    setOperation("-1");
    setOperationInvalid(false);
    setOperations([]);
    setOperationsInvalid(false);
  };

  return (
    <React.Fragment>
      <Card>
        <CardHeader className="bg-primary text-white">
          <Row>
            <Col>{props.t("newService")}</Col>
          </Row>
        </CardHeader>
        <CardBody>
          <Form>
            <Row>
              <Col lg="12">
                <FormGroup>
                  <Label>{props.t("description")}</Label>
                  <Input
                    id="description"
                    description="description"
                    type="text"
                    placeholder={props.t("p_description")}
                    onChange={handleDescriptionChange}
                    value={description}
                    required
                    invalid={descriptionInvalid}
                  />
                  <FormFeedback>{props.t("requiredFieldMessage")}</FormFeedback>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col lg="4">
                <FormGroup>
                  <Label>{props.t("category")}</Label>
                  <Input
                    id="category"
                    description="category"
                    type="select"
                    onChange={handleCategoryChange}
                    invalid={categoryInvalid}
                    value={category}
                  >
                    <option value="-1" key="selectCategory">
                      {props.t("p_category")}
                    </option>
                    {productscategoriesList &&
                      productscategoriesList.map((element, index) => (
                        <option value={element._id} key={index}>
                          {element.category}
                        </option>
                      ))}
                  </Input>
                  <FormFeedback>{props.t("requiredFieldMessage")}</FormFeedback>
                </FormGroup>
              </Col>
              <Col lg="4">
                <FormGroup>
                  <Label>{props.t("trademark")}</Label>
                  <Input
                    id="trademark"
                    description="trademark"
                    type="select"
                    onChange={handleTrademarkChange}
                    invalid={trademarkInvalid}
                    value={trademark}
                  >
                    <option value="-1" key="selectTrademark">
                      {props.t("p_trademark")}
                    </option>
                    {trademarksList &&
                      trademarksList.map((element, index) => (
                        <option value={element._id} key={index}>
                          {element.trademark}
                        </option>
                      ))}
                  </Input>
                  <FormFeedback>{props.t("requiredFieldMessage")}</FormFeedback>
                </FormGroup>
              </Col>
              <Col lg="4">
                <FormGroup>
                  <Label>{props.t("product")}</Label>
                  <Input
                    id="product"
                    description="product"
                    type="select"
                    onChange={handleProductChange}
                    invalid={productInvalid}
                    value={product}
                    disabled={category == "-1" || trademark == "-1"}
                  >
                    <option value="-1" key="selectProduct">
                      {props.t("p_product")}
                    </option>
                    {productsList &&
                      productsList.map((element, index) => (
                        <option value={element._id} key={index}>
                          {element.alias} ({element.model})
                        </option>
                      ))}
                  </Input>
                  <FormFeedback>{props.t("requiredFieldMessage")}</FormFeedback>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col lg="10">
                <FormGroup>
                  <Label>{props.t("operation")}</Label>
                  <Input
                    id="operation"
                    description="operation"
                    type="select"
                    onChange={handleOperationChange}
                    invalid={operationInvalid}
                    value={operation}
                  >
                    <option value="-1" key="selectOperation">
                      {props.t("p_operation")}
                    </option>
                    {operationsList &&
                      operationsList.map((element, index) => {
                        let list = true;
                        for (let i = 0; i < operations.length; i++) {
                          if (element._id == operations[i]._id) {
                            list = false;
                          }
                        }
                        if (list) {
                          return (
                            <option
                              value={JSON.stringify({
                                _id: element._id,
                                alias: element.alias,
                              })}
                              key={index}
                            >
                              {element.alias}
                            </option>
                          );
                        } else {
                          return null;
                        }
                      })}
                  </Input>
                  <FormFeedback>{props.t("requiredFieldMessage")}</FormFeedback>
                </FormGroup>
              </Col>
              <Col lg="2" className="d-flex justify-content-center align-items-end mb-3">
                <Button
                  type="button"
                  color="primary"
                  size="md"
                  className="btn-rounded waves-effect waves-light"
                  onClick={handleAddOperation}
                >
                  {props.t("addOperation")}
                </Button>
              </Col>
            </Row>
            <Row>
              <Col lg="10">
                <ListGroup>
                  {operations &&
                    operations.map((element, index) => (
                      <ListGroupItem key={index}>{element.alias}</ListGroupItem>
                    ))}
                </ListGroup>
              </Col>
            </Row>
          </Form>
          <Row>
            <Col lg="10"></Col>
            <Col lg="2" className="d-flex justify-content-center">
              <Button
                type="button"
                color="primary"
                size="md"
                className="btn-rounded waves-effect waves-light me-1"
                onClick={handleClear}
              >
                {props.t("clear")}
              </Button>
              <Button
                type="button"
                color="primary"
                size="md"
                className="btn-rounded waves-effect waves-light ms-1"
                onClick={HandleSave}
              >
                {props.t("save")}
              </Button>
            </Col>
          </Row>
        </CardBody>
      </Card>
    </React.Fragment>
  );
};

const mapStateToProps = (state) => {
  return {
    RC_Services: state.Services,
    RC_ProductsCategories: state.ProductsCategories,
    RC_Trademarks: state.Trademarks,
    RC_Operations: state.Operations,
    RC_Products: state.Products,
  };
};

const mapDispatchToProps = (dispatch) => ({
  R_Functions: {
    onAddService: (data, navigate) => dispatch(addService(data, navigate)),
    onGetProductsCategoriesList: (sort, order, limit, skip, navigate) =>
      dispatch(getProductsCategoriesList(sort, order, limit, skip, navigate)),
    onGetTrademarksList: (sort, order, limit, skip, navigate) =>
      dispatch(getTrademarksList(sort, order, limit, skip, navigate)),
    onGetProductsList: (
      category_id,
      trademark_id,
      sort,
      order,
      limit,
      skip,
      navigate
    ) =>
      dispatch(
        getProductsList(
          category_id,
          trademark_id,
          sort,
          order,
          limit,
          skip,
          navigate
        )
      ),
    onGetOperationsList: (sort, order, limit, skip, navigate) =>
      dispatch(getOperationsList(sort, order, limit, skip, navigate)),
  },
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(withTranslation()(ServiceRegistry))
);
