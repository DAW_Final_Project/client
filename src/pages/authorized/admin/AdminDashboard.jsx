import React from "react";
import {
  Container,
  Row,
  Col,
  Label,
} from "reactstrap";

// Redux
import { connect } from "react-redux";

// Routes
import withRouter from "../../../routes/js/withRouter";

// Components
import ClientList from "../clients/Clients-List";
import UserList from "../users/Users-List";

const AdminDashboard = (props) => {
  // Render
  return (
    <Container>
      <Row>
        <Col className="d-flex justify-content-center">
          <Label>ADMINISTRADOR</Label>
        </Col>
      </Row>
      <Row>
        <Col>
          <ClientList />
        </Col>
      </Row>
      <Row>
        <Col>
          <UserList />
        </Col>
      </Row>
    </Container>
  );
};
const mapStateToProps = (state) => {
  return {
   
  };
};

const mapDispatchToProps = (dispatch) => ({
  R_Functions: {
    
  },
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AdminDashboard));