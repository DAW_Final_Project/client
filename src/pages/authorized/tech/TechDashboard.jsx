import React from "react";
import {
  Container,
  Row,
  Col,
  Label,
} from "reactstrap";

// Redux
import { connect } from "react-redux";

// Routes
import withRouter from "../../../routes/js/withRouter";

// Components
import UnderConstruction from "../../../components/common/UnderConstruction";

const TechDashboard = (props) => {
  // Render
  return (
    <Container>
      <Row>
        <Col className="d-flex justify-content-center">
          <Label>TALLER</Label>
        </Col>
      </Row>
      <Row>
        <Col>
          <UnderConstruction />
        </Col>
      </Row>
    </Container>
  );
};
const mapStateToProps = (state) => {
  return {
   
  };
};

const mapDispatchToProps = (dispatch) => ({
  R_Functions: {
    
  },
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(TechDashboard));