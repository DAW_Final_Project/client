import React from "react";
import { Container, Row, Col, Label } from "reactstrap";

// Redux
import { connect } from "react-redux";

// Routes
import withRouter from "routes/js/withRouter";

// Components
import UnderConstruction from "components/common/UnderConstruction";
import ClientRegistration from "pages/authorized/clients/AddClient"

const DeskDashboard = (props) => {
  // Render
  return (
    <Container className="container-fluid">
      <Row>
        <Col className="d-flex justify-content-center">
          <Label>MOSTRADOR</Label>
        </Col>
      </Row>
      <Row>
        <Col>
          <ClientRegistration />
        </Col>
      </Row>
    </Container>
  );
};
const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => ({
  R_Functions: {},
});

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(DeskDashboard)
);
