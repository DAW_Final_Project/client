import React, { useState, useEffect } from "react";
import { Card, CardHeader, CardBody, Row, Col, Label } from "reactstrap";

// Redux
import { connect } from "react-redux";

// Routes
import withRouter from "../../../routes/js/withRouter";
import { Link } from "react-router-dom";

// Actions
import { getClientData } from "../../../store/actions";

// Components
import AddService from "../services/AddService"
import ServicesList from "../services/Services-List"

// i18n
import { withTranslation } from "react-i18next";

const ClientData = (props) => {
  // Functions from props
  const { onGetClientData } = props.R_Functions;
  // States from props
  const { clientData } = props.RC_Clients;
  const { client_id } = props.router.params;
  // Local States
const [refresh, setRefresh]= useState(true)
  // UseEffects
  useEffect(() => {
    onGetClientData(client_id, props.router.navigate);
  }, []);
  useEffect(() => {
    if(!refresh){
      setRefresh(true)
    }
  }, [refresh]);

  const refreshList=()=>{
    setRefresh(!refresh)
  }
  
  // Render
  return (
    <React.Fragment>
      <Card>
        <CardHeader className="bg-primary text-white">
          <Row>
            <Col lg="11">
              <h4>{clientData.name} {clientData.surnames}</h4>
            </Col>
            <Col lg="1" className="text-end">
              <Link to={`/clients`} className="text-white">
                <i className="bx bx-log-out bx-sm m-1" />
              </Link>
            </Col>
          </Row>
        </CardHeader>
        <CardBody>
          <Row>
            <Col>
            <AddService refreshList={refreshList}/>
            </Col>
          </Row>
          <br/>
          <Row>
            <Col>
            {refresh && <ServicesList/>}
            </Col>
          </Row>
        </CardBody>
      </Card>
    </React.Fragment>
  );
};
const mapStateToProps = (state) => {
  return {
    RC_Clients: state.Clients,
  };
};

const mapDispatchToProps = (dispatch) => ({
  R_Functions: {
    onGetClientData: (navigate) => dispatch(getClientData(navigate)),
  },
});

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(withTranslation()(ClientData))
);
