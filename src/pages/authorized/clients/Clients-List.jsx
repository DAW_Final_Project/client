import React, { useState, useEffect } from "react";
import { Card, CardHeader, CardBody, Row, Col } from "reactstrap";

// Redux
import { connect } from "react-redux";

// Actions
import { getClientsList, deleteClient } from "store/actions";

// Routes
import withRouter from "routes/js/withRouter";

// table paginator
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";

// Components
import ClientsListColumns from "./Clients-List-Columns";
import ClientsListEditorModal from "./Clients-List-Editor_Modal";
import ClientsListAdderModal from "./Clients-List-Adder_Modal";

// i18n
import { withTranslation } from "react-i18next";

const ClientsList = (props) => {
  // Functions from props
  const { onGetClientsList, onDeleteClient } = props.R_Functions;

  //States from props
  const { clientsList, clientsListTotal, clientsSuccess } = props.RC_Clients;

  // Local States
  const [showEditorModal, setShowEditorModal] = useState(false);
  const [showAdderModal, setShowAdderModal] = useState(false);
  const [client_id, setClient_id] = useState("");
  const [skip, setSkip] = useState(0);
  const [limit, setLimit] = useState(10);
  const [sort, setSort] = useState("surnames");
  const [order, setOrder] = useState("asc");
  const [page, setPage] = useState(0);
  const [sizePerPage, setSizePerPage] = useState(10);
  const [totalElements, setTotalElements] = useState(
    parseInt(clientsListTotal)
  );

  // UseEffects
  useEffect(() => {
    onGetClientsList(sort, order, limit, skip, props.router.navigate);
  }, []);

  useEffect(() => {
    setTotalElements(parseInt(clientsListTotal));
  }, [clientsListTotal]);

  useEffect(() => {
    onGetClientsList(sort, order, limit, skip, props.router.navigate);
  }, [skip, limit, sort, order]);

  useEffect(() => {
    if (clientsSuccess === "MsgOk[Client Deleted]") {
      onGetClientsList(sort, order, limit, skip, props.router.navigate);
    }
  }, [clientsSuccess]);

  // Handlers
  const handleTableChange = (type, newState) => {
    switch (type) {
      case "sort":
        setSort(newState.sortField);
        setOrder(newState.sortOrder);
        break;
    }
  };

  const handlePageChange = (page, sizePerPage) => {
    setPage(page);
    setSkip(page * sizePerPage);
    setLimit(sizePerPage);
  };

  const handleSizePerPageChange = (sizePerPage, page) => {
    setPage(1);
    setSkip(0);
    setSizePerPage(sizePerPage);
    setLimit(sizePerPage);
  };

  const handleDelete = (client_id) => {
    if (window.confirm(props.t("registryDeleteAlert"))) {
      onDeleteClient(client_id, props.router.navigate);
    }
  };

  // Toggles
  const toggleEditorModal = (client_id) => {
    if (!showEditorModal) {
      setClient_id(client_id);
    } else {
      onGetClientsList(sort, order, limit, skip, props.router.navigate);
    }
    setShowEditorModal(!showEditorModal);
  };

  const toggleAdderModal = (client_id) => {
    if (showAdderModal) {
      onGetClientsList(sort, order, limit, skip, props.router.navigate);
    }
    setShowAdderModal(!showAdderModal);
  };

  // Pagination Tools
  const customTotal = (from, to, size) => {
    return (
      <span className="react-bootstrap-table-pagination-total">
        &nbsp; &nbsp; {props.t("showingFrom")} {from} {props.t("to")} {to}{" "}
        {props.t("of")} {size} {props.t("results")}
      </span>
    );
  };

  const pageOptions = {
    totalSize: totalElements,
    page: page,
    sizePerPage: sizePerPage,
    pageStartIndex: 0,
    paginationSize: 3,
    withFirstAndLast: true,
    alwaysShowAllBtns: true,
    firstPageText: "|<<",
    prePageText: "<<",
    nextPageText: ">>",
    lastPageText: ">>|",
    hideSizePerPage: false,
    disablePageTitle: true,
    hidePageListOnlyOnePage: true,
    showTotal: true,
    paginationTotalRenderer: customTotal,
    onPageChange: (page, sizePerPage) => {
      handlePageChange(page, sizePerPage);
    },
    onSizePerPageChange: (sizePerPage, page) => {
      handleSizePerPageChange(sizePerPage, page);
    },
    sizePerPageList: [
      {
        text: "5",
        value: 5,
      },
      {
        text: "10",
        value: 10,
      },
      {
        text: "ALL",
        value: totalElements,
      },
    ],
  };

  return (
    <React.Fragment>
      <Card>
        <CardHeader className="bg-primary text-white">
          <Row>
            <Col lg="11">
              <h4>{props.t("clientsList")}</h4>
            </Col>
            <Col lg="1" className="d-flex justify-content-end">
              <a href="#">
                <i
                  className="bx bx-plus-circle bx-sm m-1 text-white"
                  onClick={toggleAdderModal}
                />
              </a>
            </Col>
          </Row>
        </CardHeader>
        <CardBody>
          <BootstrapTable
            keyField="_id"
            data={clientsList || []}
            columns={ClientsListColumns(props, toggleEditorModal, handleDelete)}
            remote={{
              sort: true,
              pagination: true,
            }}
            onTableChange={handleTableChange}
            pagination={paginationFactory(pageOptions)}
            responsive
            bordered={false}
            striped={false}
            classes={"table table-centered table-nowrap"}
            headerWrapperClasses={"thead-light"}
          />
        </CardBody>
      </Card>
      {showEditorModal && (
        <ClientsListEditorModal
          isOpen={showEditorModal}
          toggle={toggleEditorModal}
          client_id={client_id}
        />
      )}
      {showAdderModal && (
        <ClientsListAdderModal
          isOpen={showAdderModal}
          toggle={toggleAdderModal}
        />
      )}
    </React.Fragment>
  );
};

const mapStateToProps = (state) => {
  return {
    RC_Clients: state.Clients,
  };
};

const mapDispatchToProps = (dispatch) => ({
  R_Functions: {
    onGetClientsList: (sort, order, limit, skip, navigate) =>
      dispatch(getClientsList(sort, order, limit, skip, navigate)),
    onDeleteClient: (client_id, navigate) =>
      dispatch(deleteClient(client_id, navigate)),
  },
});

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(withTranslation()(ClientsList))
);
