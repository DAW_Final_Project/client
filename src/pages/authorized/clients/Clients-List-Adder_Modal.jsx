import React, { useState } from "react";
import {
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row,
  Col,
  Form,
  FormGroup,
  FormFeedback,
  Input,
  Label,
} from "reactstrap";

// Redux
import { connect } from "react-redux";

// Routes
import withRouter from "routes/js/withRouter";

// Actions
import { addClient } from "store/actions";

// i18n
import { withTranslation } from "react-i18next";

// Helpers
import sleep from "helpers/functions/sleep";

const ClientListModal = (props) => {
  // Functions from props
  const { onAddClient } = props.R_Functions;
  // States from props
  const { isOpen, toggle } = props;

  // Local States
  const [name, setName] = useState("");
  const [nameInvalid, setNameInvalid] = useState(false);
  const [surnames, setSurnames] = useState("");
  const [surnamesInvalid, setSurnamesInvalid] = useState(false);
  const [document, setDocument] = useState("");
  const [documentInvalid, setDocumentInvalid] = useState(false);
  const [email, setEmail] = useState("");
  const [emailInvalid, setEmailInvalid] = useState(false);
  const [phone, setPhone] = useState("");
  const [phoneInvalid, setPhoneInvalid] = useState(false);
  const [address, setAddress] = useState("");
  const [addressInvalid, setAddressInvalid] = useState(false);
  const [city, setCity] = useState("");
  const [cityInvalid, setCityInvalid] = useState(false);
  const [postalCode, setPostalCode] = useState("");
  const [postalCodeInvalid, setPostalCodeInvalid] = useState(false);

  // Handlers
  const handleNameChange = (event) => {
    if (nameInvalid) {
      setNameInvalid(false);
    }
    setName(event.target.value);
  };
  const handleSurnamesChange = (event) => {
    if (surnamesInvalid) {
      setSurnamesInvalid(false);
    }
    setSurnames(event.target.value);
  };
  const handleDocumentChange = (event) => {
    if (documentInvalid) {
      setDocumentInvalid(false);
    }
    setDocument(event.target.value);
  };
  const handleEmailChange = (event) => {
    if (emailInvalid) {
      setEmailInvalid(false);
    }
    setEmail(event.target.value);
  };
  const handlePhoneChange = (event) => {
    if (phoneInvalid) {
      setPhoneInvalid(false);
    }
    setPhone(event.target.value);
  };
  const handleAddressChange = (event) => {
    if (addressInvalid) {
      setAddressInvalid(false);
    }
    setAddress(event.target.value);
  };
  const handleCityChange = (event) => {
    if (cityInvalid) {
      setCityInvalid(false);
    }
    setCity(event.target.value);
  };
  const handlePostalCodeChange = (event) => {
    if (postalCodeInvalid) {
      setPostalCodeInvalid(false);
    }
    setPostalCode(event.target.value);
  };

  const formValidator = () => {
    let valid = true;
    if (name == "") {
      valid = false;
      setNameInvalid(true);
    }
    if (surnames == "") {
      valid = false;
      setSurnamesInvalid(true);
    }
    if (document == "") {
      valid = false;
      setDocumentInvalid(true);
    }
    if (email == "") {
      valid = false;
      setEmailInvalid(true);
    }
    if (phone == "") {
      valid = false;
      setPhoneInvalid(true);
    }
    if (address == "") {
      valid = false;
      setAddressInvalid(true);
    }
    if (city == "") {
      valid = false;
      setCityInvalid(true);
    }
    if (postalCode == "") {
      valid = false;
      setPostalCodeInvalid(true);
    }
    return valid;
  };
  const handleSave = async (event, values) => {
    if (formValidator()) {
      let data = {};
      data.name = name;
      data.surnames = surnames;
      data.document = document;
      data.email = email;
      data.phone = phone;
      data.address = {};
      data.address.address = address;
      data.address.city = city;
      data.address.postalCode = postalCode;
      onAddClient(data, props.router.navigate);
      await sleep(500)
      toggle();
    }
  };

  // RENDER
  return (
    <Modal
      isOpen={isOpen}
      role="dialog"
      autoFocus={true}
      centered={true}
      className="exampleModal"
      backdrop="static"
      tabIndex="-1"
      toggle={toggle}
      size="lg"
    >
      <div className="modal-content">
        <ModalHeader toggle={toggle}>
          <Row>
            <Col>{props.t("newClient")}</Col>
          </Row>
        </ModalHeader>
        <ModalBody>
          <Form>
            <Row>
              <Col lg="4">
                <FormGroup>
                  <Label>{props.t("name")}</Label>
                  <Input
                    id="name"
                    name="name"
                    type="text"
                    placeholder={props.t("p_name")}
                    onChange={handleNameChange}
                    value={name}
                    required
                    invalid={nameInvalid}
                  />
                  <FormFeedback>{props.t("requiredFieldMessage")}</FormFeedback>
                </FormGroup>
              </Col>
              <Col lg="8">
                <FormGroup>
                  <Label>{props.t("surnames")}</Label>
                  <Input
                    id="surnames"
                    name="surnames"
                    type="text"
                    placeholder={props.t("p_surnames")}
                    onChange={handleSurnamesChange}
                    value={surnames}
                    required
                    invalid={surnamesInvalid}
                  />
                  <FormFeedback>{props.t("requiredFieldMessage")}</FormFeedback>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col lg="3">
                <FormGroup>
                  <Label>{props.t("document")}</Label>
                  <Input
                    id="document"
                    name="document"
                    type="text"
                    placeholder={props.t("p_document")}
                    onChange={handleDocumentChange}
                    value={document}
                    required
                    invalid={documentInvalid}
                  />
                  <FormFeedback>{props.t("requiredFieldMessage")}</FormFeedback>
                </FormGroup>
              </Col>
              <Col lg="3">
                <FormGroup>
                  <Label>{props.t("phone")}</Label>
                  <Input
                    id="phone"
                    name="phone"
                    type="text"
                    placeholder={props.t("p_phone")}
                    onChange={handlePhoneChange}
                    value={phone}
                    required
                    invalid={phoneInvalid}
                  />
                  <FormFeedback>{props.t("requiredFieldMessage")}</FormFeedback>
                </FormGroup>
              </Col>
              <Col lg="6">
                <FormGroup>
                  <Label>{props.t("email")}</Label>
                  <Input
                    id="email"
                    name="email"
                    type="text"
                    placeholder={props.t("p_email")}
                    onChange={handleEmailChange}
                    value={email}
                    required
                    invalid={emailInvalid}
                  />
                  <FormFeedback>{props.t("requiredFieldMessage")}</FormFeedback>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col lg="12">
                <FormGroup>
                  <Label>{props.t("address")}</Label>
                  <Input
                    id="address"
                    name="address"
                    type="text"
                    placeholder={props.t("p_address")}
                    onChange={handleAddressChange}
                    value={address}
                    required
                    invalid={addressInvalid}
                  />
                  <FormFeedback>{props.t("requiredFieldMessage")}</FormFeedback>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col lg="8">
                <FormGroup>
                  <Label>{props.t("city")}</Label>
                  <Input
                    id="city"
                    name="city"
                    type="text"
                    placeholder={props.t("p_city")}
                    onChange={handleCityChange}
                    value={city}
                    required
                    invalid={cityInvalid}
                  />
                  <FormFeedback>{props.t("requiredFieldMessage")}</FormFeedback>
                </FormGroup>
              </Col>
              <Col lg="4">
                <FormGroup>
                  <Label>{props.t("postalCode")}</Label>
                  <Input
                    id="postalCode"
                    name="postalCode"
                    type="text"
                    placeholder={props.t("p_postalCode")}
                    onChange={handlePostalCodeChange}
                    value={postalCode}
                    required
                    invalid={postalCodeInvalid}
                  />
                  <FormFeedback>{props.t("requiredFieldMessage")}</FormFeedback>
                </FormGroup>
              </Col>
            </Row>
          </Form>
        </ModalBody>
        <ModalFooter>
          <Button type="button" color="primary" size="sm" onClick={handleSave}>
            {props.t("Save")}
          </Button>
          <Button type="button" color="primary" size="sm" onClick={toggle}>
            {props.t("Cancel")}
          </Button>
        </ModalFooter>
      </div>
    </Modal>
  );
};

const mapStateToProps = (state) => {
  return {
    RC_Clients: state.Clients,
  };
};

const mapDispatchToProps = (dispatch) => ({
  R_Functions: {
    onAddClient: (data, navigate) => dispatch(addClient(data, navigate)),
  },
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(withTranslation()(ClientListModal))
);
