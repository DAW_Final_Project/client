import React from "react";
import { Label, Button } from "reactstrap";

// React Router Dom
import { Link } from "react-router-dom";

const ClientsListColumns = (props, toggleModal, handleDelete) => {
  // RENDER
  return [
    {
      dataField: "surnames",
      text: `${props.t("client")}`,
      sort: true,
      headerStyle: (colum, colIndex) => {
        return { width: "60%" };
      },
      formatter: (cellContent, row) => (
        <Label>{`${row.surnames}, ${row.name}`}</Label>
      ),
    },
    {
      dataField: "document",
      text: `${props.t("document")}`,
      sort: true,
      headerStyle: (colum, colIndex) => {
        return { width: "25%" };
      },
    },
    {
      dataField: "_id",
      text: `${props.t("actions")}`,
      sort: true,
      headerStyle: (colum, colIndex) => {
        return { width: "15%" };
      },
      headerClasses:"text-center",
      classes:"text-center",
      formatter: (cellContent, row) => (
        <>
        <a href="#">
        <i
          className="bx bx-show bx-sm m-1"
          onClick={() => {
            toggleModal(cellContent);
          }}
        />
        </a>
        <Link to={`/client/${cellContent}`}>
        <i
          className="bx bx-log-in bx-sm m-1"
        />
        </Link>
        {/* <a href="#">
        <i
          className="bx bx-trash bx-sm m-1"
          onClick={() => {
            handleDelete(cellContent);
          }}
        />
        </a> */}
        </>
      ),
    },
  ];
};

export default ClientsListColumns;
