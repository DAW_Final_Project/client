import React from "react";
import { View, Text, StyleSheet } from "@react-pdf/renderer";

// i18n
import { withTranslation } from "react-i18next";

const styles = StyleSheet.create({
  card: {
    borderColor: "black",
    borderWidth: 1,
    padding: 15,
    marginTop: 20,
  },
  cardHeader: {
    backgroundColor: "lightgrey",
    textTransform: "uppercase",
    paddingTop: 4,
    paddingLeft: 5,
  },
  cardBody: {
    padding: 10,
  },
  rowLabels: {
    flexDirection: "row",
    marginTop: 10,
  },
  rowData: {
    flexDirection: "row",
    marginTop: 5,
  },
  labelText: {
    textAlign: "left",
    paddingLeft: 2,
    fontSize: 10,
  },
  dataText: {
    backgroundColor: "cornsilk",
    textAlign: "left",
    paddingLeft: 2,
    fontSize: 10,
    border: 1,
    borderColor: "black",
    margin: 2,
    textIndent: 5,
  },
});

const InvoiceItemsTable = (props) => {
  // States from props
  const serviceData = props.serviceData;
  const clientData = props.clientData;
  const storeData = props.storeData;
  const productData = props.productData;
  const trademarkData = props.trademarkData;
  console.log("storeData", storeData);

  /*  const truncateString = (str) => {
        const long = 66;
        let str2 = str.slice(0, long - 1)
        for (var i = long; i < str.length; i = i + long) {
            str2 = str2 + "\n" + str.slice(i, i + long - 1)
        }
        return str2;
    }
    const data = truncateString(TransactionData.data) */

  return (
    <View>
      <View style={styles.card}>
        <View style={styles.cardHeader}>
          <Text style={styles.header}>{props.t("store")}</Text>
        </View>

        <View style={styles.cardBody}>
          <View style={styles.rowLabels}>
            <View style={{ width: "33.3%" }}>
              <Text style={styles.labelText}>{props.t("store")}</Text>
            </View>
            <View style={{ width: "50%" }}>
              <Text style={styles.labelText}>{props.t("email")}</Text>
            </View>
            <View style={{ width: "16.6%" }}>
              <Text style={styles.labelText}>{props.t("phone")}</Text>
            </View>
          </View>
          <View style={styles.rowData}>
            <View style={{ width: "33.3%" }}>
              <Text style={styles.dataText}>{storeData.name}</Text>
            </View>
            <View style={{ width: "50%" }}>
              <Text style={styles.dataText}>{storeData.email}</Text>
            </View>
            <View style={{ width: "16.6%" }}>
              <Text style={styles.dataText}>{storeData.phone}</Text>
            </View>
          </View>

          <View style={styles.rowLabels}>
            <View style={{ width: "58.3%" }}>
              <Text style={styles.labelText}>{props.t("address")}</Text>
            </View>
            <View style={{ width: "25%" }}>
              <Text style={styles.labelText}>{props.t("city")}</Text>
            </View>
            <View style={{ width: "16.6%" }}>
              <Text style={styles.labelText}>{props.t("postalCode")}</Text>
            </View>
          </View>
          <View style={styles.rowData}>
            <View style={{ width: "58.3%" }}>
              <Text style={styles.dataText}>{storeData.address.address}</Text>
            </View>
            <View style={{ width: "25%" }}>
              <Text style={styles.dataText}>{storeData.address.city}</Text>
            </View>
            <View style={{ width: "16.6%" }}>
              <Text style={styles.dataText}>
                {storeData.address.postalCode}
              </Text>
            </View>
          </View>
        </View>
      </View>

      <View style={styles.card}>
        <View style={styles.cardHeader}>
          <Text style={styles.header}>{props.t("client")}</Text>
        </View>

        <View style={styles.cardBody}>
          <View style={styles.rowLabels}>
            <View style={{ width: "33.3%" }}>
              <Text style={styles.labelText}>{props.t("name")}</Text>
            </View>
            <View style={{ width: "66.6%" }}>
              <Text style={styles.labelText}>{props.t("surnames")}</Text>
            </View>
          </View>
          <View style={styles.rowData}>
            <View style={{ width: "33.3%" }}>
              <Text style={styles.dataText}>{clientData.name}</Text>
            </View>
            <View style={{ width: "66.6%" }}>
              <Text style={styles.dataText}>{clientData.surnames}</Text>
            </View>
          </View>

          <View style={styles.rowLabels}>
            <View style={{ width: "25%" }}>
              <Text style={styles.labelText}>{props.t("document")}</Text>
            </View>
            <View style={{ width: "25%" }}>
              <Text style={styles.labelText}>{props.t("phone")}</Text>
            </View>
            <View style={{ width: "50%" }}>
              <Text style={styles.labelText}>{props.t("email")}</Text>
            </View>
          </View>
          <View style={styles.rowData}>
            <View style={{ width: "25%" }}>
              <Text style={styles.dataText}>{clientData.document}</Text>
            </View>
            <View style={{ width: "25%" }}>
              <Text style={styles.dataText}>{clientData.phone}</Text>
            </View>
            <View style={{ width: "50%" }}>
              <Text style={styles.dataText}>{clientData.email}</Text>
            </View>
          </View>

          <View style={styles.rowLabels}>
            <View style={{ width: "100%" }}>
              <Text style={styles.labelText}>{props.t("address")}</Text>
            </View>
          </View>
          <View style={styles.rowData}>
            <View style={{ width: "100%" }}>
              <Text style={styles.dataText}>{clientData.address.address}</Text>
            </View>
          </View>

          <View style={styles.rowLabels}>
            <View style={{ width: "66.6%" }}>
              <Text style={styles.labelText}>{props.t("city")}</Text>
            </View>
            <View style={{ width: "33.3%" }}>
              <Text style={styles.labelText}>{props.t("postalCode")}</Text>
            </View>
          </View>
          <View style={styles.rowData}>
            <View style={{ width: "66.6%" }}>
              <Text style={styles.dataText}>{clientData.address.city}</Text>
            </View>
            <View style={{ width: "33.3%" }}>
              <Text style={styles.dataText}>
                {clientData.address.postalCode}
              </Text>
            </View>
          </View>
        </View>
      </View>

      <View style={styles.card}>
        <View style={styles.cardHeader}>
          <Text style={styles.header}>{props.t("product")}</Text>
        </View>

        <View style={styles.cardBody}>
          <View style={styles.rowLabels}>
            <View style={{ width: "33.3%" }}>
              <Text style={styles.labelText}>{props.t("product")}</Text>
            </View>
            <View style={{ width: "33.3%" }}>
              <Text style={styles.labelText}>{props.t("trademark")}</Text>
            </View>
            <View style={{ width: "33.3%" }}>
              <Text style={styles.labelText}>{props.t("model")}</Text>
            </View>
          </View>
          <View style={styles.rowData}>
            <View style={{ width: "33.3%" }}>
              <Text style={styles.dataText}>{productData.alias}</Text>
            </View>
            <View style={{ width: "33.3%" }}>
              <Text style={styles.dataText}>{trademarkData.trademark}</Text>
            </View>
            <View style={{ width: "33.3%" }}>
              <Text style={styles.dataText}>{productData.model}</Text>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

export default withTranslation()(InvoiceItemsTable);
