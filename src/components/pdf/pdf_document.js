import React from "react";
import { Page, Document, StyleSheet } from "@react-pdf/renderer";

//store
import store from "../../store";

//Pdf components
import PdfHeader from "./pdf_header";
import PdfTableItems from "./pdf_table_items";
import PdfFooter from "./pdf_footer";

// Create styles
const styles = StyleSheet.create({
  page: {
    fontFamily: "Helvetica",
    fontSize: 11,
    paddingTop: 30,
    paddingLeft: 60,
    paddingRight: 60,
    lineHeight: 1.5,
    flexDirection: "column",
  },
});

// Create Document Component
const PDFDocument = (props) => {
  const serviceData = props.serviceData;
  const clientData = props.clientData;
  const storeData = props.storeData;
  const productData = props.productData;
  const trademarkData = props.trademarkData;
  return (
    <Document title={serviceData._id} author="ModFix S.L.">
      <Page size="A4" style={styles.page}>
        <PdfHeader storeData={storeData} />
        <PdfTableItems
          serviceData={serviceData}
          clientData={clientData}
          storeData={storeData}
          productData={productData}
          trademarkData={trademarkData}
        />
        <PdfFooter />
      </Page>
    </Document>
  );
};

export default PDFDocument;
