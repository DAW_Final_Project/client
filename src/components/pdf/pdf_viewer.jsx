// React
import React from "react";

// React Router Dom
import withRouter from "routes/js/withRouter";

// Pdf
import { PDFViewer } from "@react-pdf/renderer";
import PdfDocument from "./pdf_document";

const PdfViewer = (props) => {
  // States from props
  const serviceData = props.serviceData;
  const clientData = props.clientData;
  const storeData = props.storeData;
  const productData = props.productData;
  const trademarkData = props.trademarkData;

  // RENDER
  return (
    <React.Fragment>
      <PDFViewer className="pdfviewer">
        <PdfDocument
          serviceData={serviceData}
          clientData={clientData}
          storeData={storeData}
          productData={productData}
          trademarkData={trademarkData}
        />
      </PDFViewer>
    </React.Fragment>
  );
};

export default withRouter(PdfViewer);
