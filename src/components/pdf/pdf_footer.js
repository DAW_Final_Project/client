import React from "react";
import { Text, View, Image, StyleSheet } from "@react-pdf/renderer";

//images

const styles = StyleSheet.create({
  titleContainer: {
    flexDirection: "row",
    marginTop: 3,
    padding: 10,
  },
  text: {
    width: "100%",
    fontSize: 10,
    textAlign: "center",
  },
});

const PdfFooter = (props) => (
  <>
    <View style={styles.titleContainer}>
      <Text style={styles.text}>
      © {new Date().getFullYear()} Modfix S.L.
      </Text>
    </View>
  </>
);

export default PdfFooter;
