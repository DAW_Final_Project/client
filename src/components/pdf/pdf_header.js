import React, { useEffect, useState } from "react";
import { Text, View, Image, StyleSheet } from "@react-pdf/renderer";

//store
import store from "../../store";

//images
import logo from "assets/images/logo.png";

const styles = StyleSheet.create({
  row: {
    flexDirection: "row",
  },
  titleContainer: {
    width: "50%",
    letterSpacing: 4,
    fontSize: 20,
    textAlign: "center",
    paddingTop: 12,
  },
  logoContainer: {
    width: "25%",
  },
  dataContainer: {
    flexDirection: "row",
    marginTop: 12,
  },
  storeContainer: {
    borderColor: "black",
    borderWidth: 1,
    width: "50%",
    padding: 15,
    fontSize: 10,
  },
  logo01: {
    width: 50,
    height: 50,
    marginLeft: 10,
    marginRight: "auto",
  },
  logo02: {
    width: 50,
    height: 50,
    marginRight: 10,
    marginLeft: "auto",
    textAlign: "left",
  },
});

const PdfHeader = (props) => {
  const storeData = props.storeData;

  return (
    <>
      <View style={styles.row}>
        <View style={styles.logoContainer}>
          <Image style={styles.logo01} src={logo} />
        </View>
        <View style={styles.titleContainer}>
          <Text>ModFix S.L.</Text>
        </View>
        <View style={styles.logoContainer}>
          <Image style={styles.logo02} src={logo} />
        </View>
      </View>
      {/* <View style={styles.dataContainer}>
        <View style={styles.storeContainer}>
          <Text >Empresa</Text>
          <Text >Modfix S.L.</Text>
          <Text >B12345678X</Text>
          <Text >Calle 01</Text>
          <Text >Ciudad 01 46000</Text>
        </View>
        <View style={styles.storeContainer}>
          <Text >Tienda</Text>
          <Text >{storeData.name}</Text>
          <Text >{storeData.address.address}</Text>
          <Text >{storeData.address.city} {storeData.address.postalCode}</Text>
          <Text >{storeData.phone}</Text>
          <Text >{storeData.email}</Text>
        </View> 
      </View> */}
    </>
  );
};

export default PdfHeader;
