import { Row, Col } from "reactstrap";
import withRouter from "../../routes/js/withRouter";

//Images
import Logo from "../../assets/images/logo.png";

const Header = (props) => {
  // Render
  return (
    <header className="container-fluid">
      <Row>
        <Col lg="1">
          <img src={Logo} height="75" weight="75" alt=""></img>
        </Col>
        <Col lg="10" className="d-flex justify-content-center align-items-center">
          <h1>ModFix</h1>
        </Col>
        <Col lg="1">          
        <img src={Logo} height="75" weight="75" alt=""></img>
        </Col>
      </Row>
    </header>
  );
};

export default withRouter(Header);
