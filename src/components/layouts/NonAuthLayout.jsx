import { Container, Card, CardBody, CardFooter, Row, Col } from "reactstrap";
import withRouter from "../../routes/js/withRouter";

// Components
import Header from "./Header";
import Footer from "./Footer";
import LanguageSelector from "components/common/LanguajeSelector";
import Loader from "components/common/Loader";

const NonAuthLayout = (props) => {
  // Render
  return (
    <Container
      id="nonauthlayout"
      className="container-fluid d-flex justify-content-center align-items-center mt-5"
    >
      <Card>
        <CardBody>
          <Loader />
          {props.children}
        </CardBody>
        <CardFooter className="bg-white border-0">
          <Row>
            <Col>
              <Footer />
            </Col>
          </Row>
          <Row>
            <Col className="d-flex justify-content-center">
              <LanguageSelector />
            </Col>
          </Row>
        </CardFooter>
      </Card>
    </Container>
  );
};

export default withRouter(NonAuthLayout);
