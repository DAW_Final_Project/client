import { Container, Card, CardHeader, CardBody, CardFooter } from "reactstrap";
import withRouter from "../../routes/js/withRouter";

// i18n
import { withTranslation } from "react-i18next";

// Components
import Header from "./Header";
import NavBar from "../navbar/NavBar";
import Footer from "./Footer";
import Loader from "components/common/Loader";

const AuthLayout = (props) => {
  // Render
  return (
    <Container id="nonauthlayout" className="container-fluid mt-2">
      <Card>
        <CardHeader>
          <Header />
        </CardHeader>
        <CardBody style={{ minHeight: "700px" }}>
    <Loader />
          <NavBar />
          <br />
          {props.children}
        </CardBody>
        <CardFooter className="bg-white border-0">
          <Footer />
        </CardFooter>
      </Card>
    </Container>
  );
};

export default withRouter(withTranslation()(AuthLayout));
