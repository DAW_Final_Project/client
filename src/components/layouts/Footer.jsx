import React from "react";
import { Container, Row, Col, Label } from "reactstrap";
import withRouter from "../../routes/js/withRouter";

// Components
import LanguageSelector from "components/common/LanguajeSelector"

const Footer = (props) => {
  // Render
  return (
      <footer className="footer">
        <Container  className="container-fluid d-sm-block">
              <Row>
                <Col className="d-flex justify-content-center">
                  <Label>
                    © {new Date().getFullYear()} Modfix S.L.
                  </Label>
                </Col>
              </Row>
        </Container>
      </footer>
  );
};

export default withRouter(Footer);
