// Reactstrap
import { Container, Row, Col} from "reactstrap";

// Routes
import withRouter from "routes/js/withRouter";

// Images
import notFound from "assets/images/404NotFound.gif";

const NotFound = (props) => {
  // Render
  return (
    <Container className="container-fluid">
      <Row className="mt-5">
        <Col className="d-flex justify-content-center">
          <img src={notFound}/>
        </Col>
      </Row>
    </Container>
  );
};

export default withRouter(NotFound);
