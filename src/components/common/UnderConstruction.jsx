// Reactstrap
import { Container, Row, Col} from "reactstrap";

// Routes
import withRouter from "routes/js/withRouter";

// Images
import underConstruction from "assets/images/under_construction.gif";

const UnderConstruction = (props) => {
  // Render
  return (
    <Container className="container-fluid">
      <Row>
        <Col className="d-flex justify-content-center">
          <img src={underConstruction}/>
        </Col>
      </Row>
    </Container>
  );
};

export default withRouter(UnderConstruction);
