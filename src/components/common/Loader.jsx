import React from "react";

// Redux
import { connect } from "react-redux";

const Loader = (props) => {
  // States from props
  //Authentication
  const { authenticationLoading } = props.RC_Authentication;
  //Clients
  const { clientsLoading } = props.RC_Clients;
  //Services
  const { servicesLoading } = props.RC_Services;
  //Username
  const { usersLoading } = props.RC_Users;
  return (
      <>
      {authenticationLoading || clientsLoading || servicesLoading || usersLoading ? (
    <div
      className=""
      style={{
          display:"block",
        position: "absolute",
        top: "0",
        left: "0",
        right: "0",
        bottom: "0",
        zIndex: "9999",
        backgroundColor: "rgba(240,240,240,0.3)",
      }}
    >
      <div
      className=""
        style={{
          position: "absolute",
          width: "10%",
          height: "10%",
          top: "50%",
          left: "45%",
          margin: "auto",
        }}
      >
        <div
          className=""
          style={{
            margin: "auto",
            width: "10%",
            height: "10%",
            position: "relative",
          }}
        >
          <div className="spinner-border text-primary" role="status">
            <span className="sr-only"></span>
          </div>
        </div>
      </div>
    </div>
    ):null}
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    RC_Authentication: state.Authentication,
    RC_Clients: state.Clients,
    RC_Services: state.Services,
    RC_Users: state.Users,
  };
};

export default connect(mapStateToProps)(Loader);
