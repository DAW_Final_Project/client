import React, { useEffect, useState } from "react";
import { get, map } from "lodash";
import { withTranslation } from "react-i18next";

//i18n
import i18n from "../../i18n";
import languages from "locales/languages";
import { LS_I18N } from "helpers/localstorage/definitions";

const LanguageSelector = (props) => {
  // Declare a new state variable, which we'll call "menu"
  const [selectedLang, setSelectedLang] = useState("");
  const [isOpen, setIsOpen] = useState(false);

  useEffect(() => {
    setIsOpen(false);
    const currentLanguage = localStorage.getItem(LS_I18N);
    setSelectedLang(currentLanguage);
  }, []);

  const changeLanguageAction = (event) => {
    //set language as i18n
    i18n.changeLanguage(event.target.getAttribute("data-lang"));
    //localStorage.setItem(LS_I18N, event.target.value)
    setSelectedLang(event.target.getAttribute("data-lang"));
    setIsOpen(!isOpen);
  };

  const TogleMenu = () => {
    setIsOpen(!isOpen);
  };

  const Flags = () => {
    return (
      <>
        {map(Object.keys(languages), (key) => (
          <>
            <br />
            <button
              data-lang={key}
              className="btn border-none"
              onClick={changeLanguageAction}
              key={`${key}button`}
            >
              <img
                key={`${key}img`}
                alt={key}
                data-lang={key} 
                src={get(languages, `${key}.flag`)}
                className="flag"
                style={{ height: "20px" }}
              />
            </button>
          </>
        ))}
      </>
    );
  };

  return (
    <>
      <span className="languageSelector">
        <button
          className="btn header-item waves-effect flag-btn"
          onClick={TogleMenu}
          key={`${selectedLang}buttonSelected`}
        >
          <img
            src={get(languages, `${selectedLang}.flag`)}
            alt="flag"
            className="flag"
            style={{ height: "25px" }}
            key={`${selectedLang}imgSelected`}
          />
        </button>

        {isOpen && <Flags />}
      </span>
    </>
  );
};

export default withTranslation()(LanguageSelector);
