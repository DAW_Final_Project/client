import React from "react";
import { Navbar, Nav, NavItem } from "reactstrap";

// Routes
import withRouter from "routes/js/withRouter";
import { Link } from "react-router-dom";

// Components
import LanguageSelector from "components/common/LanguajeSelector";

//i18n
import { withTranslation } from "react-i18next";

const NavBar = (props) => {
  // States from props
  const { user } = props;

  // Render
  return (
    <Navbar id="navbar" color="light" light expand="md">
      <Nav className="ml-auto" navbar>
        <h5 className="ms-2">{user}</h5>
      </Nav>
      <Nav className="ml-auto me-3" navbar>
        <NavItem>
          <LanguageSelector />
        </NavItem>
        <NavItem className="d-flex align-items-center">
          <Link to="/">{props.t("logout")}</Link>
        </NavItem>
      </Nav>
    </Navbar>
  );
};

export default withRouter(withTranslation()(NavBar));
