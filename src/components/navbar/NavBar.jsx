import React, { useEffect, useState } from "react";
import withRouter from "routes/js/withRouter";

// Authentication
import { getLoggedInUser } from "helpers/localstorage/authentication";

// Components
import NavBar_Admin from "./NavBar_Admin";
import NavBar_Desk from "./NavBar_Desk";
import NavBar_Tech from "./NavBar_Tech";

const AuthLayout = (props) => {
  const [role, setRole] = useState(0);
  const [name, setName] = useState("");

  useEffect(() => {
    let user = getLoggedInUser();
    if(user){
      setRole(user.role);
      setName(user.user);
    }
  }, []);

  const DefaultNavBar = () => <React.Fragment>Cargando</React.Fragment>;
  // Render
  return (
    <React.Fragment>
      {role == 0 && <DefaultNavBar />}
      {role == 1 && <NavBar_Admin user={name} />}
      {role == 2 && <NavBar_Desk user={name}/>}
      {role == 3 && <NavBar_Tech user={name} />}
    </React.Fragment>
  );
};

export default withRouter(AuthLayout);
