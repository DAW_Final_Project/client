/* import React from "react";
import {
  Container,
  Row,
  Col,
  Label,
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Button,
} from "reactstrap";

// Redux
import { connect } from "react-redux";

// Routes
import withRouter from "../../routes/js/withRouter";
import { Link } from "react-router-dom";

// Components
import LanguageSelector from "components/common/LanguajeSelector";

//i18n
import { withTranslation } from "react-i18next";

const NavBar = (props) => {
  // Render
  return (
    <Navbar id="navbar" color="light" light expand="md">
      <Nav className="ml-auto" navbar>
      <UncontrolledDropdown nav inNavbar className="navitem">
          <DropdownToggle nav caret className="p-0">
            {props.t("systemAdministration")}
          </DropdownToggle>
          <DropdownMenu>            
          <DropdownItem>
              <Link to="/clients">{props.t("clientsList")}</Link>
            </DropdownItem>
            <DropdownItem>
              <Link to="/addclient">{props.t("addClient")}</Link>
            </DropdownItem>
            <DropdownItem>
              <Link to="/modifyclientdata">{props.t("modifyClientData")}</Link>
            </DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
        <UncontrolledDropdown nav inNavbar className="navitem">
          <DropdownToggle nav caret className="p-0">
            {props.t("customerAttendance")}
          </DropdownToggle>
          <DropdownMenu>            
          <DropdownItem>
              <Link to="/clients">{props.t("clientsList")}</Link>
            </DropdownItem>
            <DropdownItem>
              <Link to="/addclient">{props.t("addClient")}</Link>
            </DropdownItem>
            <DropdownItem>
              <Link to="/modifyclientdata">{props.t("modifyClientData")}</Link>
            </DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
        <UncontrolledDropdown nav inNavbar className="navitem">
          <DropdownToggle nav caret className="p-0">
            {props.t("technicalService")}
          </DropdownToggle>
          <DropdownMenu>            
          <DropdownItem>
              <Link to="/clients">{props.t("clientsList")}</Link>
            </DropdownItem>
            <DropdownItem>
              <Link to="/addclient">{props.t("addClient")}</Link>
            </DropdownItem>
            <DropdownItem>
              <Link to="/modifyclientdata">{props.t("modifyClientData")}</Link>
            </DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
      </Nav>
      <Nav className="ml-auto" navbar>
        <NavItem>
          <LanguageSelector />
        </NavItem>
        <NavItem className="d-flex align-items-center">
          <Link to="/">{props.t("logout")}</Link>
        </NavItem>
      </Nav>
    </Navbar>
  );
};
const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => ({
  R_Functions: {},
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withTranslation()(NavBar)));
 */

import React from "react";
import { Navbar, Nav, NavItem } from "reactstrap";

// Routes
import withRouter from "routes/js/withRouter";
import { Link } from "react-router-dom";

// Components
import LanguageSelector from "components/common/LanguajeSelector";

//i18n
import { withTranslation } from "react-i18next";

const NavBar = (props) => {
  // States from props
  const { user } = props;

  // Render
  return (
    <Navbar id="navbar" color="light" light expand="md">
      <Nav className="ml-auto" navbar>
        <h5 className="ms-2">{user}</h5>
      </Nav>
      <Nav className="ml-auto me-3" navbar>
        <NavItem>
          <LanguageSelector />
        </NavItem>
        <NavItem className="d-flex align-items-center">
          <Link to="/">{props.t("logout")}</Link>
        </NavItem>
      </Nav>
    </Navbar>
  );
};

export default withRouter(withTranslation()(NavBar));